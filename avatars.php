<?php
require_once 'include/common.php';
load_kvs_env();
load_class('Avatars');
load_class('FFProbe');

require_once KVS_ROOT . 'admin/include/functions_screenshots.php';

$kvs = new KVSDB();
$req = $args->required([
    'which', 'limit', 'all',
]);
extract($req);

$limit or $limit = 500;

// load the wanted size constraints
$sizes   = $kvs->fetch("SELECT variable, value FROM ktvs_options WHERE variable LIKE '%_SIZE' ORDER BY variable", R_ASSOC_K);
$formats = [
    'models'  => [
        1 => $sizes['MODELS_SCREENSHOT_1_SIZE']['value'],
        2 => $sizes['MODELS_SCREENSHOT_2_SIZE']['value'],
    ],
    'sources' => [
        1 => $sizes['CS_SCREENSHOT_1_SIZE']['value'],
        2 => $sizes['CS_SCREENSHOT_2_SIZE']['value'],
    ],
];
msg($formats);

$t = new Timer;

if (!$which || $which == 'models') {
    $models = $kvs->fetch("SELECT * FROM ktvs_models WHERE '' IN(screenshot1,screenshot2) AND total_videos > 0 ORDER BY total_videos DESC LIMIT $limit");
    foreach ($models as $model) {
        $msg   = "model ";
        $needs = [];
        $done  = [];
        if ($model->screenshot1 == '') {
            $needs['s1_' . slug($model->title, '_') . '.jpg'] = $formats['models'][1];
            $msg .= " needs ss1.. ";
        }
        if ($model->screenshot2 == '') {
            $needs['s2_' . slug($model->title, '_') . '.jpg'] = $formats['models'][2];
            $msg .= " needs ss2.. ";
        }

        msg($model->title, $msg);

        $best_video = $kvs->fetch("SELECT v.* FROM ktvs_videos v LEFT JOIN ktvs_models_videos mv ON mv.video_id = v.video_id LEFT JOIN ktvs_models m ON mv.model_id = m.model_id WHERE m.model_id = ? ORDER BY v.video_viewed DESC LIMIT 3", $model->model_id);
        if ($best_video) {
            foreach ($needs as $fn => $fs) {
                $bv          = array_shift($best_video);
                $output_dir  = $config['content_path_models'] . '/' . $model->model_id . '/';
                $output_file = $output_dir . $fn;
                $video_file  = KVS_ROOT . 'cdn_dir/kvs/videos/' . (floor($bv->video_id / 1000) * 1000) . '/' . $bv->video_id . '/' . $bv->video_id . '.mp4';


                $test = new FFProbe($video_file);

                var_dump($test);
                die;


                if (file_exists($video_file)) {
                    $frame     = floor($bv->duration / 2);
                    $work_file = make_thumb_from_movie($video_file, $fs, $frame);
                    if ($work_file) {
                        if (!is_dir($output_dir)) {
                            mkdir($output_dir, 0777, true);
                        }

                        if (copy($work_file, $output_file)) {
                            msg($model->title, sprintf("copied %s to %s using video: %s", basename($work_file), str_replace($config['content_path_models'] . '/', '', $output_file), basename($video_file)) );
                            unlink($work_file);
                            $done['screenshot' . $fn[1]] = $fn;
                        }
                    }
                }
            }

            if (count($done) > 0) {
                $upd = $kvs->execute("UPDATE ktvs_models SET ?% WHERE model_id = ?", $done, $model->model_id);
                msg($model->title, "Updated $upd rows with new screenshots ...");
            }
        }
    }
}

if (!$which || $which == 'sources') {
    $sources = $kvs->fetch("SELECT * FROM ktvs_content_sources WHERE '' IN (screenshot1,screenshot2) AND total_videos > 0");
    msg("sources", "Found " . count($sources) . " sources without screenshots.");
    if ( $limit ) {
        $sources = array_slice($sources, 0, $limit);
    }
    foreach ($sources as $source) {
        $msg   = "source ";
        $needs = [];
        $done  = [];
        if ($source->screenshot1 == '') {
            $needs['s1_' . slug($source->dir, '_') . '.jpg'] = $formats['sources'][1];
            $msg .= " needs ss1.. ";
        }
        if ($source->screenshot2 == '') {
            $needs['s2_' . slug($source->dir, '_') . '.jpg'] = $formats['sources'][2];
            $msg .= " needs ss2.. ";
        }

        msg($source->title, $msg);
        $best_video = $kvs->fetch("SELECT * FROM ktvs_videos WHERE content_source_id = ? ORDER BY ktvs_videos.video_viewed DESC  LIMIT 3", $source->content_source_id);
        if ($best_video) {
            foreach ($needs as $fn => $fs) {
                if ( count($best_video) > 0 )
                    $bv          = array_shift($best_video);
                $output_dir  = $config['content_path_content_sources'] . '/' . $source->content_source_id . '/';
                $output_file = $output_dir . $fn;
                $video_file  = KVS_ROOT . 'cdn_dir/kvs/videos/' . (floor($bv->video_id / 1000) * 1000) . '/' . $bv->video_id . '/' . $bv->video_id . '.mp4';
                if (file_exists($video_file)) {
                    $frame     = floor($bv->duration / 2);
                    $work_file = make_thumb_from_movie($video_file, $fs, $frame);
                    if ($work_file) {
                        if (!is_dir($output_dir)) {
                            mkdir($output_dir, 0777, true);
                        }

                        if (copy($work_file, $output_file)) {
                            msg($source->dir, sprintf("copied %s to %s using vid: %s", basename($work_file), str_replace($config['content_path_content_sources'] . '/', '', $output_file), basename($video_file)) );
                            unlink($work_file);
                            $done['screenshot' . $fn[1]] = $fn;
                        }
                    }
                }
            }
            if (count($done) > 0) {
                $upd = $kvs->execute("UPDATE ktvs_content_sources SET ?% WHERE content_source_id = ?", $done, $source->content_source_id);
                msg($source->dir, "Updated $upd rows with new screenshots ...");
            }
        }
    }
}

/**
 * @param $movie_file
 * @param $output_dir
 * @param $output_file
 * @param $size
 */
function make_thumb_from_movie($movie_file, $output_size, $frame = 15)
{
    global $config, $t;
    $work_dir = $config['project_path'] . '/tmp/avatars/';
    if (!is_dir($work_dir)) {
        mkdir($work_dir, 0777, true);
    }

    $format_base = [
        'format_screenshot_id' => 100000,
        'size'                 => $output_size,
        'aspect_ratio_id'      => 2,
        'im_options'           => "-enhance -strip -unsharp 1.0x1.0+0.5 -unsharp 1.0x1.0+0.5 -modulate 110,102,100 -unsharp 1.0x1.0+0.5 -contrast -gamma 1.2 -resize %SIZE% %INPUT_FILE% -filter Lanczos -filter Blackman -quality 80 %OUTPUT_FILE%",
    ];

    $file_id = strtoupper(crc32("$movie_file"));

    $work_file   = "$file_id-%s.jpg";
    $work_image  = $work_dir . sprintf($work_file, 'full');
    $output_file = $work_dir . sprintf($work_file, $output_size);
    if (file_exists($work_image)) {
        unlink($work_image);
    }

    if (file_exists($output_file)) {
        unlink($output_file);
    }
    $tmp = Avatars::video_frame($movie_file, $work_image, $frame);
    if ($tmp && is_file($work_image)) {
        $tmp = make_screen_from_source($work_image, $output_file, $format_base, '');
        unlink($work_image);
        if ($tmp === false) {
            return $output_file;
        }
    }
    @unlink($output_file);

    return false;
}

function load_kvs_env()
{
        $regexp_include_tpl="|{{include\ file\ *=['\"\ ]*([^}]*?\.tpl)['\"]|is";
        $regexp_insert_block="|{{insert\ +name\ *=\ *['\"]getBlock['\"]\ +block_id\ *=\ *['\"](.*?)['\"]\ +block_name\ *=\ *['\"](.*?)['\"]|is";
        $regexp_insert_global="|{{insert\ +name\ *=\ *['\"]getGlobal['\"]\ +global_id\ *=\ *['\"](.*?)['\"]|is";
        $regexp_insert_adv="|{{insert\ +name\ *=\ *['\"]getAdv['\"]\ +place_id\ *=\ *['\"](.*?)['\"]|is";
        $regexp_valid_external_id="|^[A-Za-z0-9_]+$|is";
        $regexp_valid_block_name="|^[A-Za-z0-9\ ]+$|is";
        $regexp_valid_page_component_id="|^[^/^\\\\]+$|is";
        $regexp_valid_text_id="|^[A-Za-z0-9_\.]+$|is";
        $regexp_check_email = "/^([^@])+@([^@])+\.([^@])+$/is";
        $regexp_check_alpha_numeric = "|^[a-zA-Z0-9_@\.\-]+$|is";
        umask(0);

        $tmp =  function() {
            include_once 'setup.php';
            return $config;
        }

        $conn = function() {
            include_once 'setup_db.php';
            return $conn;
        }



    /**
     * stuff that gets loaded:
     * $config
     * $conn
     *
     * from funcs base:
     * $regexp_include_tpl="|{{include\ file\ *=['\"\ ]*([^}]*?\.tpl)['\"]|is";
        $regexp_insert_block="|{{insert\ +name\ *=\ *['\"]getBlock['\"]\ +block_id\ *=\ *['\"](.*?)['\"]\ +block_name\ *=\ *['\"](.*?)['\"]|is";
        $regexp_insert_global="|{{insert\ +name\ *=\ *['\"]getGlobal['\"]\ +global_id\ *=\ *['\"](.*?)['\"]|is";
        $regexp_insert_adv="|{{insert\ +name\ *=\ *['\"]getAdv['\"]\ +place_id\ *=\ *['\"](.*?)['\"]|is";
        $regexp_valid_external_id="|^[A-Za-z0-9_]+$|is";
        $regexp_valid_block_name="|^[A-Za-z0-9\ ]+$|is";
        $regexp_valid_page_component_id="|^[^/^\\\\]+$|is";
        $regexp_valid_text_id="|^[A-Za-z0-9_\.]+$|is";
        $regexp_check_email = "/^([^@])+@([^@])+\.([^@])+$/is";
        $regexp_check_alpha_numeric = "|^[a-zA-Z0-9_@\.\-]+$|is";
        umask(0);
    *
    * From servers
        $GLOBAL_FTP_SERVERS=array();
    * funcs screenshots changes nothing
    * funcs con conversion cahnges nothing
    * funcs admin doesn't change anything, but it includes additional files

        include_once('functions_base.php');
        include_once('functions.php');
        include_once("functions_servers.php");
        include_once('placeholder.php');





    global $config;
    $cwd = getcwd();
    chdir(KVS_ROOT . 'admin/include');
    include_once 'setup.php';
    include_once 'setup_db.php';
    include_once 'functions_base.php';
    include_once 'functions_servers.php';
    include_once 'functions_screenshots.php';
    include_once 'functions_cron_conversion.php';
    include_once 'functions_admin.php';
    include_once 'functions.php';
    include_once 'placeholder.php';
    include_once 'pclzip.lib.php';
    ini_set('display_errors', 1);
    chdir($cwd);
}
