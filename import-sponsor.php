<?php
require 'include/common.php';

$new = dbh_get('kvs', 'mysqli');
$old = dbh_get('old_mt', 'mysqli');
$programs = do_query("SELECT * FROM programs", $old);
foreach($programs as $p) {

    $check = do_query("SELECT * FROM ktvs_content_sources_groups WHERE title = "._escpq($p->name,$new), $new);
    if ( !$check ) {
        $group_id = do_query("INSERT INTO ktvs_content_sources_groups (title, custom1, added_date, dir) VALUES ("._escpq($p->name,$new).","._escpq($p->url,$new).", NOW(),"._escpq(slug($p->name),$new).")", $new);
        if ( $group_id ) print "[GROUP] Added $p->name" . NL;
        else exit("Couldnt' add $p->name");
    } else {
        $check = $check[0];
        $group_id = $check->content_source_group_id;
        if ( $check->dir == '' ) {
            $dir = slug($check->title);
            $upd = do_query("UPDATE ktvs_content_sources_groups SET dir = '".$dir."' WHERE content_source_group_id = $group_id", $new);
            if ( $upd )
                print "[GROUP] Updated dir: $dir".NL;
        }

    }

    $sponsors = do_query("SELECT * FROM sponsors WHERE program_id = $p->program_id", $old);
    if ( !$sponsors ) continue;

    foreach($sponsors as $sponsor) {
        $ktvs_sponsor = grab_row("SELECT * FROM ktvs_content_sources WHERE title = "._escpq($sponsor->url,$new),$new);
        if ( !$ktvs_sponsor ) {
            list($dir,$ext) = explode('.',$sponsor->title);
            $dir = slug($dir);
            $sponsor_id = do_query(sprintf("INSERT INTO ktvs_content_sources (content_source_group_id, title, url, added_date, dir) VALUES (%s, %s,%s,%s,%s)", $group_id, _escpq($sponsor->url,$new), _escpq($sponsor->link, $new), 'NOW()', _escpq($dir)), $new);
            if ($sponsor_id) print "[SPONSOR][$sponsor_id] Added $sponsor->url" . NL;
            else exit("Couldnt added $sponsor->url");
        } else {
            if ( $ktvs_sponsor->content_source_group_id === '0' ) {
                print "[SPONSOR][$ktvs_sponsor->content_source_id] Missing group id! .. ";
                $upd = do_query("UPDATE ktvs_content_sources SET content_source_group_id = $group_id WHERE content_source_id = $ktvs_sponsor->content_source_id", $new);
                if ( $upd )
                    print "updated: $group_id!".NL;
                else
                    print "no update :(".NL;
            }
        }
    }
}

// $rand = ['visit','checkout','goto'];

// // forgot to make a slug for the above

// $sources = do_query("SELECT * FROM ktvs_content_sources", $new);
// $added = [];
// foreach($sources as $source) {
//     list($dir,$ext) = explode('.',$source->title);
//     $dir = slug($dir);
//     if ( in_array($dir,$added))
//     {
//         $dir = $dir .'-'.$ext;
//         if ( in_array($dir,$added) )
//         {
//             shuffle($rand);
//             $dir = $rand[0].'-'.$dir;
//         }
//     }
//     $added[] = $dir;
//     $upd = "UPDATE ktvs_content_sources SET dir = "._escpq($dir)." WHERE content_source_id = $source->content_source_id";
//     do_query($upd);
// }
