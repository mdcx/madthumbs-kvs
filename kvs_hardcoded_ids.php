<?php
require "../include/list_countries.php";

$id_list = [
    'gender_list' => array(
        0 => 'female',
        1 => 'male',
        2 => 'other',
    ),
    'hair_list'   => array(
        0 => ' ',
        1 => 'black',
        2 => 'dark',
        3 => 'red',
        4 => 'brown',
        5 => 'blond',
        6 => 'grey',
        7 => 'bald',
        8 => 'wig',
    ),

    'eye_color_list' => array(
        0 => ' ',
        1 => 'blue',
        2 => 'gray',
        3 => 'green',
        4 => 'amber',
        5 => 'brown',
        6 => 'hazel',
        7 => 'black',
    ),
    'country_list'   => $list_countries,
];

unset($list_countries);

foreach($id_list['country_list']['name'] as $k=>&$v) $v = strtolower($v);

return $id_list;