<?php

include 'include/common.php';

define('SAVE_QUERIES', true);


$req  = $args->required(['limit']);
$cmd  = $args->method();

$import = new accountsImport;
$import->$cmd();

class accountsImport
{
    /**
     * @var MadDB
     */
    public $kvs;

    /**
     * @var MadDB
     */
    public $mt;

    /**
     * @var array
     */
    private $defaults = [
        'username'        => false,
        'email'           => false,
        'custom10'        => false,
        'last_login_date' => '0000-00-00',
        'country_id'      => 2270,
        'gender_id'       => 0,
        'status_id'       => 6,
        'birth_date'      => '0000-00-00',
        'last_login_date' => '0000-00-00 00:00:00',
        'added_date'      => '',
        'is_trusted'      => 1,

    ];

    /**
     * @var array
     */
    private $map = [
        'username'   => 'username',
        'name'       => 'display_name',
        'url'        => 'website',
        'email'      => 'email',
        'id'         => 'custom10',
        'last_login' => 'last_login_date',
        'paypal'     => 'account_paypal',

    ];

    public function __construct()
    {
        $this->mt  = new MTDB();
        $this->kvs = new KVSDB();
        load_file('text');
    }

    public function sponsor()
    {
        $accounts = $this->mt->fetch("SELECT p.name,p.link,p.url,ca.paypal, u.* FROM programs p"
            . " JOIN programs_users pu ON pu.program_id = p.program_id "
            . " JOIN users u ON pu.user_id = u.id "
            . " LEFT OUTER JOIN cash_accounts ca ON u.id = ca.user_id"
            . " ORDER BY rand()"
            . " LIMIT 0, 10");

        $all_users = [];

        foreach ($accounts as $mt_acc) {

            $new_user = $this->defaults;
            foreach ($this->map as $mt_val => $kvs_val) {
                $new_user[$kvs_val] = $mt_acc->$mt_val;
            }

            $nu          = $new_user['username'];
            $video_count = 0;

            if (in_array(false, array_values($new_user), true) === false) {
                // verify we haven't imported them into kvs before, and try to get their old + new program id
                $new_group_id  = $this->kvs->fetch_var("SELECT content_source_group_id FROM ktvs_content_sources_groups WHERE title = ?", $new_user['display_name']);
                $been_imported = $this->kvs->fetch("SELECT * FROM ktvs_users WHERE (username = ? OR custom10 = ?)", $mt_acc->username, $mt_acc->id);
                if ($new_group_id) {
                    $new_user['content_source_group_id'] = $new_group_id;

                    if (count($been_imported) === 0) {
                        if (($ktvs_user = $this->kvs->insert('ktvs_users', $new_user))) {
                            msg($nu, "added user to new db, id: $ktvs_user");
                        } else {
                            msg($nu, "failed adding user to database!");
                            msg($nu, "err: " . $this->kvs->last_error());
                            vdd($new_user);
                            die;
                        }
                    } else {
                        // msg($nu, "user previously imported ...");
                        $ktvs_user = $been_imported[0]->user_id;
                    }

                    // make sure this source has some videos in the DB, if not, skip it
                    $content_sources     = $this->kvs->get_col("SELECT content_source_id FROM ktvs_content_sources WHERE content_source_group_id = $new_group_id");
                    $videos_not_imported = count($content_sources) == 0 ? [] : $this->kvs->get_col($this->kvs->prepare("select video_id from ktvs_videos WHERE user_id != ? AND content_source_id IN ( ?@ )", [$ktvs_user, $content_sources]));
                    $all_videos          = count($content_sources) == 0 ? [] : $this->kvs->get_col($this->kvs->prepare("select video_id from ktvs_videos WHERE content_source_id IN ( ?@ )", [$content_sources]));

                    $video_count = count($all_videos);
                    if (($videos_not_count = count($videos_not_imported)) > 0 && $ktvs_user) {
                        $upd = $this->kvs->query($this->kvs->prepare("UPDATE ktvs_videos SET user_id = ? WHERE video_id IN (?@)", $ktvs_user, $videos_not_imported));
                        if ($upd) {
                            msg($nu, "associated $upd videos with user ...");
                        }
                    }

                } else {
                    msg($nu, "no content_source_group found for '{$new_user['display_name']}'");
                }
            } else {
                msg($mt_acc->username, "something is wrong with this user:");
                msg($mt_acc->username, $new_user);
            }

            $all_users[$new_user['username']] = [
                'username'     => $new_user['username'],
                'display_name' => $new_user['display_name'],
                'email'        => $new_user['email'],
                'vid_count'    => $video_count,
            ];
        }
        ksort($all_users);
        print NL . text_table($all_users) . NL;
    }
}

if (SAVE_QUERIES === true) {
    $import->kvs->query_report();
}
