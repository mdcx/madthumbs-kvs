<?php

chdir('../include');

include_once 'setup.php';
include_once 'setup_db.php';
include_once 'functions_base.php';
include_once 'functions_servers.php';
include_once 'functions_screenshots.php';
include_once 'functions_cron_conversion.php';
include_once 'functions_admin.php';
include_once 'functions.php';
include_once 'placeholder.php';
include_once 'pclzip.lib.php';
include_once '../import/include/common.php';

$sql = "update $config[tables_prefix]users set
                        public_videos_count=(select count(*) from $config[tables_prefix]videos where status_id=1 and user_id=$config[tables_prefix]users.user_id and is_private=0),
                        private_videos_count=(select count(*) from $config[tables_prefix]videos where status_id=1 and user_id=$config[tables_prefix]users.user_id and is_private=1),
                        premium_videos_count=(select count(*) from $config[tables_prefix]videos where status_id=1 and user_id=$config[tables_prefix]users.user_id and is_private=2),
                        total_videos_count=public_videos_count+private_videos_count+premium_videos_count
                    where user_id = ?";

$start  = microtime(true);
$result = '';
$diff   = microtime(true) - $start;

if (((PHP_SAPI === 'cli' || defined('STDIN')) && strpos(implode(' ', $argv), 'debug') !== false) || (isset($_GET['debug']))) {
    $diff   = number_format($diff, 4);
    $format = "[%s] [%s] DURATION: %s\n                      %s\n";

    $sql  = preg_replace("/[ \t]+/", " ", $sql);
    $find = ['select', 'update', 'delete', 'create', 'set', 'order by', 'group by', 'limit', 'into', 'where', 'alter'];
    $sql  = preg_replace("/(" . implode('|', $find) . ")/ie", "strtoupper('\\1')", $sql);
    $sql  = str_replace(",", ", ", $sql);

    print "\n\n$sql\n\n";

    $sql  = wordwrap($sql, 100);
    $sql  = str_replace("\n", "\n                      ", $sql);
    $line = sprintf($format, date('Y-m-d H:i:s'), get_caller(['sql', 'sql_pr', 'sql_debug']), $diff, $sql);
    //file_put_contents("/tmp/slow.queries", $line, FILE_APPEND);
    echo $line;
}

$str1 = "If you turn on notices without the quotes, you'll see that PHP is issuing notices for every time it runs across a match: Notice: Use of undefined constant _t - assumed '_t' ";
$str2 = 'I believe David is correct. I was getting the same "undefined constant" message. The replacement part of your answer should be edited to "strtoupper(\'\\1\')"';

var_dump(similarity($str1, $str2));

var_dump(similiar_text($str1, $str2));

/**
 * @param $str1
 * @param $str2
 */
function similarity($str1, $str2)
{
    $len1 = strlen($str1);
    $len2 = strlen($str2);

    $max        = max($len1, $len2);
    $similarity = $i = $j = 0;

    while (($i < $len1) && isset($str2[$j])) {
        if ($str1[$i] == $str2[$j]) {
            $similarity++;
            $i++;
            $j++;
        } elseif ($len1 < $len2) {
            $len1++;
            $j++;
        } elseif ($len1 > $len2) {
            $i++;
            $len1--;
        } else {
            $i++;
            $j++;
        }
    }

    return round($similarity / $max, 2);
}
