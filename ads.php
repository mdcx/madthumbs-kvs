<?php
require 'include/common.php';

$baseq = "select ads.*, ap.program_id, adsp.sponsor_id FROM ads LEFT JOIN ads_programs ap ON ap.ad_id = ads.id LEFT JOIN ads_sponsors adsp ON adsp.ad_id = ads.id"
            . " WHERE adsp.sponsor_id IS NOT NULL"
            . " ORDER BY %s"
            . " LIMIT %s,%s";

$mt = new MTDB();
$kvs = new KVSDB();

$sql = sprintf($baseq, 'ads.id', 0,50000);
$res = $mt->get_results($sql);
foreach($res as $ad) {
    $check = $kvs->fetch("SELECT * FROM ktvs_content_sources WHERE title = ? OR url = ?", $ad->title, $ad->linkurl);
    if ( $check ) {
        $check = $check[0];

        $map = [
            'vidhtml'       =>'custom10',
            'imgurl'        =>'custom_file3',
            'vidimgurl'     =>'custom_file2'
        ];

        $changed = false;
        foreach($map as $src=>$dst) {
            if ( isset($ad->$src) && $ad->$src != '' )
            {
                $check->$dst = $ad->$src;
                $changed = true;
            } else {
                $check->$dst = '';
            }
        }

        if ( $changed ) {
            $upd = $kvs->update('ktvs_content_sources','content_source_id', $check);
           print "updated $check->title".NL;
        }

    }
}