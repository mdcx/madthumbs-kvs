<?php
require_once 'include/common.php';
include_once(KVS_ROOT.'admin/include/setup.php');
include_once(KVS_ROOT.'admin/include/functions_base.php');
include_once(KVS_ROOT.'admin/include/functions_servers.php');
$kvs = new KVSDB();

$req = $args->required(['task_id', 'cmd']);
extract($req);

if ($cmd === 'check') {
    $task_id = (int) $task_id;
    $check   = $kvs->fetch_row("SELECT task_id, status_id FROM ktvs_background_tasks_history WHERE task_id = ?", $task_id);
    if ($check && (int) $check->status_id === 3) {
        exit("done");
    } else {
        exit("processing");
    }
} elseif ($cmd === 'update_remote') {
    // get initial data
    $conversion_servers = $kvs->fetch("select *, 1 as is_conversion_server from $config[tables_prefix]admin_conversion_servers where status_id=1 order by rand()", 'R_ASSOC_K');
    foreach($conversion_servers as $do) {
        $put = put_file('remote_cron.php',KVS_ROOT.'admin/tools/', './', $do);
        if ( $put === true ) {
            msg("update_remote", "Updated remote_cron.php on $do[title]");
        } else {
            msg("update_remote", "Failed to update remote_cron.php on $do[title]");
        }
    }

}
