<?php
require_once "../include/setup_db.php";
require_once 'common.php';

//define('QUERY_PRINT', true);
define('SS_DIR', realpath(dirname(__FILE__) . '/../../contents/models'));

$field_map = [
    'alias'        => 'title',
    'description'  => 'description',
    'sexuality'    => 'custom2',
    'country'      => 'country_id',
    'measurements' => 'measurements',
    'height'       => 'height',
    'weight'       => 'weight',
    'haircolor'    => 'hair_id',
    'eyecolor'     => 'eye_color_id',
    'model_image'  => 'screenshot1',
    'rating'       => 'rating',
];

$kvs_model_keys = array_values($field_map);

$q = "SELECT * FROM old_models ORDER BY id LIMIT %d, %d";

$num_rows  = grab_value("SELECT count(id) FROM old_models", $conn);
$per_cycle = 250;
$cycles    = ceil($num_rows / $per_cycle);
$c         = 0;
print "Found $num_rows total models in the MT DB.\n";
print "Running $cycles cycles @ $per_cycle items each.\n";
while ($c < $cycles) {
    $offset = $c * $per_cycle;
    $sql    = sprintf($q, $offset, $per_cycle);

    $models = do_query($sql, $conn);
    print "Found " . count($models) . " in this cycle.\n";
    foreach ($models as $model) {
        $new_model = array_combine($kvs_model_keys, array_fill(0, count($kvs_model_keys), ''));
        foreach ($field_map as $mad_key => $kvs_key) {
            if ($kvs_key === 'custom' || strpos($kvs_key, '_id') !== false) {
                if ($kvs_key === 'gender_id') {
                    $new_model[$kvs_key] = 0;
                } else {
                    if ($kvs_key === 'hair_id' && $model->$mad_key === 'blonde') {
                        $model->$mad_key = 'blond';
                    }
                    $new_model[$kvs_key] = convert_mad2kvs($kvs_key, $model->$mad_key);
                }
            } elseif (isset($model->$mad_key)) {
                $new_model[$kvs_key] = $model->$mad_key;
            }
        }
        // validate $new_model. if half the fields have values, let's import it
        // if not, oh well.
        if (isset($new_model['title']) && strlen($new_model['title']) > 2) {
            add_new_model($new_model);
        }
    }
    $c++;
}

/**
 * @param $model
 * @return mixed
 */
function add_new_model($model)
{
    global $conn;
    $model['dir']           = slug($model['title']);
    $model['rating_amount'] = rand(50, 150);
    $model['added_date']    = date('Y-m-d H:i:s');

    $check = grab_value("SELECT model_id FROM ktvs_models WHERE dir = '" . do_query("ESCAPE " . $model['dir'], $conn) . "'", $conn);
    if ($check) {
        return $check;
    }

    // add model to kvs DB
    $screenshot = $model['screenshot1'];
    unset($model['screenshot1']);
    $sql = "INSERT INTO ktvs_models (%s) VALUES (%s)";
    foreach ($model as $k => $v) {
        if ($v != '') {
            $fields[] = $k;
            $values[] = is_numeric($v) ? $v : "'" . do_query("ESCAPE $v", $conn) . "'";
        }
    }

    $fields = implode(', ', $fields);
    $values = implode(', ', $values);

    $sql          = sprintf($sql, $fields, $values);
    $new_model_id = do_query($sql, $conn);
    if ($new_model_id) {
        print "[added] {$model['title']} -> $new_model_id ... \n";
        // if a ss existed, let's use th new id we jsut got and d'/l the old one
        if ($screenshot != '') {
            $new_ss = dl_old_screenshot($screenshot, $new_model_id, $model['dir']);
            if ($new_ss) {
                $sql = "UPDATE ktvs_models SET screenshot1 = '" . $new_ss . "' WHERE model_id = $new_model_id";
                $upd = do_query($sql, $conn);
            }
        }
    }
}

/**
 * @param $img_name
 * @param $model_id
 * @param $slug
 */
function dl_old_screenshot($img_name, $model_id, $slug)
{
    $old_url = "http://cache.tgpsitecentral.com/madthumbs/models/{$img_name}";
    $grab    = file_get_contents($old_url);
    if ($grab) {
        $parts         = explode('.', $img_name);
        $ext           = strtolower(array_pop($parts));
        $model_ss_dir  = SS_DIR . '/' . $model_id;
        $model_ss_name = 's1_' . str_replace('-', '_', $slug) . '.' . $ext;

        $model_ss_full = $model_ss_dir . '/' . $model_ss_name;
        if (!is_dir($model_ss_dir)) {
            mkdir($model_ss_dir, 0777, true);
        }
        $write = file_put_contents($model_ss_full, $grab);
        if ($write) {
            print "[screenshot] Downloaded $old_url to $model_ss_name ...\n";

            return $model_ss_name;
        }
    }

    return false;
}

/**
 * @param $kvs_fn
 * @param $mt_value
 * @return mixed
 */
function convert_mad2kvs($kvs_fn, $mt_value)
{
    static $id_map;
    if (empty($id_map)) {
        $id_map = require_once "kvs_hardcoded_ids.php";
    }

    $list = str_replace('_id', '_list', $kvs_fn);
    if ($kvs_fn != 'country_id') {
        $id_check = array_search($mt_value, $id_map[$list], false);
        if ($id_check) {
            return $id_check;
        }
    } elseif ($mt_value != '') {
        // first check for code
        if ($id_check = array_search(strtolower($mt_value), $id_map['country_list']['code'], false)
            || $id_check = array_search(strtolower($mt_value), $id_map['country_list']['name'], false)
        ) {
            return $id_check;
        }
    }

    return '';
}
