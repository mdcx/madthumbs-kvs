<?php
header('Content-Type: text/plain');
require 'include/common.php';

$new_req = [
    // 'sponsor' => '0|false',
    'models'    => [true, false],
    // 'force_id' => false,
    'do'        => [false, 10],
    'start'     => '0|0',
    'nogroup'   => [true, true],
    // 'min_id' => false,
    'allcheck'  => true,
    'per_slice' => [true, 50000],
    'per_cycle' => [true, 20000],
    'filep'     => [true, 'part'],
    'filen'     => [true, 1],
    'gay'       => [true, 'NOT IN'],
    'sponsor', 'force_id', 'min_id',
];

$args     = new Args;
$required = $args->required(
    $new_req, true
);

extract($required);

$new = dbh_get('kvs', 'mysqli');
$old = dbh_get('old_mt', 'mysqli');

$where = [
    "released_at != 0 AND featured_status != 'never' AND banned = 0 AND have_mp4 = 'y' AND embed IS NULL",
    sprintf("videos.category_id %s (346,383,2267,14661)", $gay),
];

$limit  = 10;
$offset = 0;
$join   = '';

if ($min_id) {
    if ($min_id === true) {
        $min_id = grab_value("SELECT custom1 from ktvs_videos WHERE custom1 != '' ORDER BY custom1 ASC LIMIT 1", $new);
    }

    $where[] = "videos.id <= $min_id";
}

if ($allcheck) {
    $all_custom = do_query("SELECT custom1 FROM ktvs_videos WHERE custom1 != ''", $new, 'custom1');
    $all_custom = implode(',', array_keys($all_custom));
    $where[]    = "videos.id NOT IN(" . $all_custom . ")";
}

if ($sponsor) {
    $where[] = "sponsor != 0";
}

if ($models) {
    $where[] = "videos.id IS NOT NULL";
    $join    = "RIGHT OUTER JOIN models_videos mv ON mv.video_id = videos.id LEFT JOIN models m ON m.id = mv.model_id";
}

if ($force_id) {
    $force_id = (int) $force_id;
    $where    = ["videos.id = $force_id"];
    $do       = 1;
}

if (count($where) > 0) {
    $where = implode(' AND ', $where);
}

$vid_q = "SELECT videos.*, tags.name as tag_name
            FROM videos
            LEFT JOIN tags ON videos.category_id = tags.id $join
            WHERE $where
            ORDER BY videos.id DESC";

$do_videos    = $do !== false ? $do : 5000;
$total_done   = 0;
$cur_cycle    = 0;
$begin_offset = (int) $start !== false ? $start : 0;

$per_slice       = min($do_videos, $per_slice);
$do_videos_start = $do_videos;
$slice           = [];
while ($do_videos > 0) {
    $offset = $begin_offset + $cur_cycle * $per_cycle;
    $limit  = $per_cycle;

    $query = $vid_q . " LIMIT $offset, $limit";
    print NL . "[q] ";
    $videos = do_query($query, $old);
    print " found " . count($videos) . ":" . NL;
    if (!$videos) {
        break;
    }

    print "(slice: $per_slice): ";
    foreach ($videos as $video) {
        $video_id = $video->id;
        if (!$force_id && !$allcheck && do_query("SELECT video_id FROM ktvs_videos WHERE custom1 = $video_id", $new)) {
            continue;
        }

        $vid = [
            'id'           => $video->id,
            'category'     => $video->tag_name,
            'title'        => $video->title,
            'description'  => $video->description,
            'url'          => 'http://www.madthumbs.com/cdn_dir/videos/' . floor($video->id / 1000) . "/$video->id.mp4",
            'rating'       => $video->rating,
            'views'        => max($video->views, 1),
            'tags'         => [],
            'models'       => [],
            'source_group' => '',
            'source'       => '',
        ];

        $models = do_query("SELECT m.alias as model_alias FROM models_videos mv LEFT JOIN models m ON m.id = mv.model_id WHERE mv.video_id = $video_id", $old);
        if ($models) {
            foreach ($models as $model) {
                $vid['models'][] = $model->model_alias;
            }
        }

        $tag_pre = do_query("SELECT t.name FROM tags t JOIN tags_videos tv ON t.id = tv.tag_id WHERE video_id = " . $video_id, $old);
        if ($tag_pre) {
            foreach ($tag_pre as $tag) {
                $vid['tags'][] = $tag->name;
            }
        }

        $tag_pre = do_query("SELECT dt.name FROM deep_tags dt JOIN deep_tags_videos dtv ON dtv.deep_tag_id = dt.id WHERE dtv.video_id =$video_id", $old);
        if ($tag_pre) {
            foreach ($tag_pre as $tag) {
                $vid['tags'][] = $tag->name;
            }
        }

        $vid['tags'] = array_unique($vid['tags']);

        array_walk($vid['tags'], function (&$v, $k) {
            $v = str_replace('_', ' ', $v);
            $v = str_replace('teen 18', 'teen 18+', $v);
            $v = trim($v);
        });

        $vid['tags']   = implode(',', $vid['tags']);
        $vid['models'] = implode(',', array_map('trim', array_unique($vid['models'])));

        $video->sponsor = (int) $video->sponsor;
        $program_id     = false;

        if ($video->sponsor === 0) {
            $tmp = grab_row("SELECT s.url,s.program_id FROM sponsors s LEFT JOIN ads_sponsors adsp ON adsp.sponsor_id = s.sponsor_id LEFT JOIN ads_videos av ON adsp.ad_id = av.ad_id WHERE av.video_id = $video_id", $old);
            if ($tmp) {
                $program_id    = $tmp->program_id;
                $vid['source'] = $tmp->url;
            }

            if (!$program_id) {
                $tmp = grab_row("SELECT p.name, p.program_id FROM programs p LEFT JOIN ads_programs adp ON adp.program_id = p.program_id LEFT JOIN ads_videos av ON adp.ad_id = av.ad_id WHERE av.video_id = $video_id", $old);
                if ($tmp) {
                    $program_id          = $tmp->program_id;
                    $vid['source_group'] = $tmp->name;
                }
            }
        }

        if ((int) $video->sponsor !== 0) {
            if ($vid['source'] == '') {
                $source = grab_row("SELECT * FROM sponsors WHERE sponsor_id = $video->sponsor", $old);
                if ($source) {
                    $vid['source'] = $source->url;
                    $program_id    = $source->program_id;
                }
            }
        }

        if ($vid['source_group'] == '' and $program_id !== false) {
            $tmp = grab_row("SELECT * FROM programs WHERE program_id = $program_id", $old);
            if ($tmp) {
                $vid['source_group'] = $tmp->name;
            }

        }

        if ($nogroup !== false) {
            unset($vid['source_group']);
        }

        $vid = array_map('trim', $vid);
        array_walk($vid, function (&$v, $k) {
            if (strpos($v, "\n") !== false) {
                $v = htmlspecialchars(str_replace("\n", "<br/>", $v));
            }

            $v = trim($v);
        });

        $slice[] = implode("|", array_values($vid));
        $do_videos--;
        print '.';
        if (count($slice) >= $per_slice || $do_videos <= 0) {
            $file  = sprintf("data/%s-%02d.txt", $filep, $filen);
            $data  = trim(implode("\n", $slice));
            $write = write_flock($file, $data);
            if ($write) {
                print "[$file] written with " . count($slice) . " entries." . NL . "(slice: $per_slice): ";
                $slice = [];
                $filen++;
            } else {
                exit("Failed writing $file" . NL);
            }
        }
        if ($do_videos <= 0) {
            break;
        }

    }
    $cur_cycle++;
}

if (count($slice) > 0) {
    $file  = sprintf("data/%s-%02d.txt", $filep, $filen);
    $data  = trim(implode("\n", $slice));
    $write = write_flock($file, $data);
    if ($write) {
        print "[$file] written with " . count($slice) . " entries." . NL . "(slice: $per_slice): ";
        $slice = [];
        $filen++;
    } else {
        exit("Failed writing $file" . NL);
    }
}
