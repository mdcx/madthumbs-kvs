<?php
/**
 * Make a pretty text table out of an array
 * @param  array  $data
 * @return string The text table
 */
function text_table($data, $print = false, $title = false, $no_header = false)
{
    if (!is_array($data)) {
        return '';
    }

    foreach ($data as $k => $v) {
        $data[$k] = (array) $v;
    }

    $keys = array_keys(end($data));
    $size = array_map('mb_strlen', $keys);
    $arv  = array_map('array_values', $data);

    foreach ($arv as $e) {
        $strlen_e = array_map('mb_strlen', $e);
        $size     = array_map('max', $size, $strlen_e);
    }

    foreach ($size as $n) {
        $form[] = "%-{$n}s";
        $line[] = str_repeat('-', $n);
    }
    $formc = count($form);
    $form  = '| ' . implode(' | ', $form) . " |\n";
    $line  = '+-' . implode('-+-', $line) . "-+\n";

    $rows = $no_header ? [] : [vsprintf($form, array_pad($keys, $formc, ''))];

    foreach ($data as $e) {
        $e      = array_pad($e, $formc, '');
        $rows[] = vsprintf($form, $e);
    }

    $ret = $line . implode($line, $rows) . $line;

    if ($title) {
        $act_len = mb_strlen($line);
        $title   = " $title";

        $title_len = $act_len - (3 + mb_strlen($title));
        $title     = '|' . $title . str_repeat(' ', $title_len) . '|';

        $title = "+" . str_repeat('-', $act_len - 3) . "+\n" . $title;
        $ret   = $title . "\n" . $ret;
    }

    if ($print) {
        print $ret;
    }

    return $ret;
}
/**
 * @param  string  $str
 * @param  int     $charlim
 * @return mixed
 */
function word_wrap($str, $charlim = 76)
{
    // Set the character limit
    is_numeric($charlim) or $charlim = 76;

    // Reduce multiple spaces
    $str = preg_replace('| +|', ' ', $str);

    // Standardize newlines
    if (strpos($str, "\r") !== false) {
        $str = str_replace(["\r\n", "\r"], "\n", $str);
    }

    // If the current word is surrounded by {unwrap} tags we'll
    // strip the entire chunk and replace it with a marker.
    $unwrap = [];

    if (preg_match_all('|\{unwrap\}(.+?)\{/unwrap\}|s', $str, $matches)) {
        for ($i = 0, $c = count($matches[0]); $i < $c; $i++) {
            $unwrap[] = $matches[1][$i];
            $str      = str_replace($matches[0][$i], '{{unwrapped' . $i . '}}', $str);
        }
    }

    // Use PHP's native function to do the initial wordwrap.
    // We set the cut flag to FALSE so that any individual words that are
    // too long get left alone. In the next step we'll deal with them.
    $str = wordwrap($str, $charlim, "\n", false);

    // Split the string into individual lines of text and cycle through them
    $output = '';

    foreach (explode("\n", $str) as $line) {
        // Is the line within the allowed character count?
        // If so we'll join it to the output and continue
        if (mb_strlen($line) <= $charlim) {
            $output .= $line . "\n";
            continue;
        }

        $temp = '';

        while (mb_strlen($line) > $charlim) {
            // If the over-length word is a URL we won't wrap it
            if (preg_match('!\[url.+\]|://|www\.!', $line)) {
                break;
            }
            // Trim the word down
            $temp .= mb_substr($line, 0, $charlim - 1);
            $line = mb_substr($line, $charlim - 1);
        }

        // If $temp contains data it means we had to split up an over-length
        // word into smaller chunks so we'll add it back to our current line
        if ($temp !== '') {
            $output .= $temp . "\n" . $line . "\n";
        } else {
            $output .= $line . "\n";
        }
    }

    // Put our markers back
    if (!empty($unwrap)) {
        foreach ($unwrap as $key => $val) {
            $output = str_replace('{{unwrapped' . $key . '}}', $val, $output);
        }
    }

    return $output;
}

/**
 * Automatically applies <p> and <br /> markup to text. Basically nl2br() on steroids.
 *
 * @param  string   subject
 * @return string
 */
function auto_p($str)
{
    // Trim whitespace
    if (($str = trim($str)) === '') {
        return '';
    }

    // Standardize newlines
    $str = str_replace(["\r\n", "\r"], "\n", $str);

    // Trim whitespace on each line
    $str = preg_replace('~^[ \t]+~m', '', $str);
    $str = preg_replace('~[ \t]+$~m', '', $str);

    // The following regexes only need to be executed if the string contains html
    if ($html_found = (strpos($str, '<') !== false)) {
        // Elements that should not be surrounded by p tags
        $no_p = '(?:p|div|h[1-6r]|ul|ol|li|blockquote|d[dlt]|pre|t[dhr]|t(?:able|body|foot|head)|c(?:aption|olgroup)|form|s(?:elect|tyle)|a(?:ddress|rea)|ma(?:p|th))';

        // Put at least two linebreaks before and after $no_p elements
        $str = preg_replace('~^<' . $no_p . '[^>]*+>~im', "\n$0", $str);
        $str = preg_replace('~</' . $no_p . '\s*+>$~im', "$0\n", $str);
    }

    // Do the <p> magic!
    $str = '<p>' . trim($str) . '</p>';
    $str = preg_replace('~\n{2,}~', "</p>\n\n<p>", $str);

    // The following regexes only need to be executed if the string contains html
    if ($html_found !== false) {
        // Remove p tags around $no_p elements
        $str = preg_replace('~<p>(?=</?' . $no_p . '[^>]*+>)~i', '', $str);
        $str = preg_replace('~(</?' . $no_p . '[^>]*+>)</p>~i', '$1', $str);
    }

    // Convert single linebreaks to <br />
    $str = preg_replace('~(?<!\n)\n(?!\n)~', "<br />\n", $str);

    return $str;
}

/**
 * Compute similiarity of 2 strings. Faster than similiar_text and doesn't use recursion.
 *
 * @param string $str1
 * @param string $str2
 */
function similarity($str1, $str2)
{
    $len1 = strlen($str1);
    $len2 = strlen($str2);

    $max        = max($len1, $len2);
    $similarity = $i = $j = 0;

    while (($i < $len1) && isset($str2[$j])) {
        if ($str1[$i] == $str2[$j]) {
            $similarity++;
            $i++;
            $j++;
        } elseif ($len1 < $len2) {
            $len1++;
            $j++;
        } elseif ($len1 > $len2) {
            $i++;
            $len1--;
        } else {
            $i++;
            $j++;
        }
    }

    return round($similarity / $max, 2);
}
