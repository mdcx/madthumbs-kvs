<?php

/**
 * AnimeTransmission
 *
 * @created 1/9/18 5:08 AM
 * @author  Mike Campbell <mike@mdcx.io>
 */
class Avatars
{
    /**
     * Check for black thumb
     *
     * @param string $screenshot
     */
    public static function analyze_screenshot($screenshot)
    {
        if (!function_exists('imagecreatefromjpeg')) {
            return true;
        }
        $im        = imagecreatefromjpeg($screenshot);
        $sum_avg_r = 0;
        $sum_avg_g = 0;
        $sum_avg_b = 0;
        for ($i = 0; $i < imagesx($im); $i++) {
            for ($j = 0; $j < imagesy($im); $j++) {
                $rgb = imagecolorat($im, $i, $j);
                $r   = ($rgb >> 16) & 0xFF;
                $g   = ($rgb >> 8) & 0xFF;
                $b   = $rgb & 0xFF;
                $sum_avg_r += $r;
                $sum_avg_g += $g;
                $sum_avg_b += $b;
            }
        }
        $avg_color_r = $sum_avg_r / (imagesx($im) * imagesy($im));
        $avg_color_g = $sum_avg_g / (imagesx($im) * imagesy($im));
        $avg_color_b = $sum_avg_b / (imagesx($im) * imagesy($im));
        if ($avg_color_r > 120 && $avg_color_r < 140 && $avg_color_g > 120 && $avg_color_g < 140 && $avg_color_b > 120 && $avg_color_b < 140) {
            imagedestroy($im);

            return false;
        }
        imagedestroy($im);

        return true;
    }


    public static function crop_resize($img_src, $img_dst, $thumb_width = null, $thumb_height = null)
    {
        if ( strpos($thumb_width, 'x') !== false ) {
            list($thumb_width, $thumb_height) = explode('x', $thumb_width);
        }

        $error = [];
        if (!file_exists($img_src)) {
            $error[] = "File $img_src doesn't exist.";
        }

        if (($img_data = @getimagesize($img_src)) == false) {
            $error[] = "Failed to open image to get meta/size.";
        }

        if ($thumb_width == null) {
            $thumb_width = 320;
        }

        if ($thumb_height == null) {
            $thumb_height = 180;
        }

        if (count($error) == 0) {
            $mime   = $img_data['mime'];
            $width  = $img_data[0];
            $height = $img_data[1];

            $max_width  = $width > $thumb_width ? $thumb_width : $width;
            $max_height = $height > $thumb_height ? $thumb_height : $height;

            $src_ratio = $width / $height;
            $dst_ratio = $max_width / $max_height;

            switch ($mime) {
                case 'image/jpeg':
                    $old_img = imagecreatefromjpeg($img_src);
                    break;

                case 'image/gif':
                    $old_img = imagecreatefromgif($img_src);
                    break;

                case 'image/png':
                    $old_img = imagecreatefrompng($img_src);
                    break;
            }

            if ($old_img) {
                /* If ratio mismatch, we need to crop first. */
                if ($src_ratio != $dst_ratio) {
                    $crop_scale = min($width / $max_width, $height / $max_height);
                    $crop_x     = $width - ($crop_scale * $max_width);
                    $crop_y     = $height - ($crop_scale * $max_height);

                    $crop_w = $width - $crop_x;
                    $crop_h = $height - $crop_y;

                    $crop = imagecreatetruecolor($crop_w, $crop_h);

                    $crop_x_mid = $crop_x / 2;
                    $crop_y_mid = $crop_y / 2;
                    imagecopy($crop, $old_img, 0, 0, $crop_x_mid, $crop_y_mid, $crop_w, $crop_h);
                }

                $new_img = imagecreatetruecolor($max_width, $max_height);
                if ($new_img) {
                    if (isset($crop)) {
                        if (!imagecopyresampled($new_img, $crop, 0, 0, 0, 0, $max_width, $max_height, $crop_w, $crop_h)) {
                            $error[] = "Failed to copyresampled from \$crop to \$new_img";
                        }

                        imagedestroy($crop);
                    } else {
                        if (!imagecopyresampled($new_img, $old_img, 0, 0, 0, 0, $max_width, $max_height, $width, $height)) {
                            $error[] = "Failed to copyresampled from \$new_img to \$old_img";
                        }

                    }
                    if (!imagejpeg($new_img, $img_dst)) {
                        $error[] = "Failed to properly resize imagejpeg.";
                    }

                    imagedestroy($new_img);
                }
                imagedestroy($old_img);
            }
        }
        if (count($error) == 0) {
            return true;
        } else {
            $error = implode("\n", $error);
            msg($error);

            return false;
        }
    }

    /**
     * Find the size of any horizontal or vertical black border around an image.
     * make_movie_thumb( array( 'video_url'  => $video_url, 'thumb_file' => $thumb_file ) );
     *
     * @param  string $path  Path to the image you wish to analyze.
     * @param  bool   $debug Echo information about what the function is doing.
     * @return array  border size x, border size y
     */
    public static function detect_border($path, $debug = false)
    {
        $pad      = 20; // The # of color pts you are willing to give in order to get the TRUE movie
        $border_y = 0;
        $border_x = 0;

        if (!file_exists($path)) {
            return false;
        }

        $im = @imagecreatefromjpeg($path);
        if (!$im) {
            return false;
        }

        $height = imagesy($im);
        $width  = imagesx($im);

        /* Let's start at 0, 0 and keep going till we hit a color */
        if ($debug) {
            msg("Height: $height / Width: $width\n", 3);
        }

        /* Border Height(Y) */
        $center_width = ceil($width / 2);
        for ($i = 0; $i < $height; $i++) {
            $rgb = imagecolorat($im, $center_width, $i);
            $r   = ($rgb >> 16) & 0xFF;
            $g   = ($rgb >> 8) & 0xFF;
            $b   = $rgb & 0xFF;
            if ($debug) {
                msg("($center_width,$i) R: $r / G: $g / B: $b\n", 3);
            }

            if ($r >= $pad || $g >= $pad || $b >= $pad) {
                $border_y = $i;
                if ($border_y == $height) {
                    $border_y = 0;
                }

                break;
            }
        }

        /* Border Width(X) */
        $center_height = ceil($height / 2);
        for ($i = 0; $i < $width; $i++) {
            $rgb = imagecolorat($im, $i, $center_height);
            $r   = ($rgb >> 16) & 0xFF;
            $g   = ($rgb >> 8) & 0xFF;
            $b   = $rgb & 0xFF;
            if ($debug) {
                msg("($i,$center_height) R: $r / G: $g / B: $b\n", 3);
            }

            if ($r >= $pad || $g >= $pad || $b >= $pad) {
                $border_x = $i;
                if ($border_x == $width) {
                    $border_x = 0;
                }

                break;
            }
        }
        if ($border_x != 0) {
            $border_x /= 2;
            $border_x = round($border_x);
            $border_x *= 2;
        }
        if ($border_y != 0) {
            $border_y /= 2;
            $border_y = round($border_y);
            $border_y *= 2;
        }

        return [$border_x, $border_y];
    }



    /**
     * Try to reduce an image's size/quality
     *
     * @param  string          $file Path to file to reduce
     * @param  $min_filesize
     * @return null
     */

    public static function image_reduce($file, $min_filesize = REDUCE_SIZE)
    {
        clearstatcache();
        $this_file = $file;
        $file_size = filesize($this_file);
        if ($file_size > $min_filesize) {
            $file_diff = $file_size - $min_filesize;
            $file_lose = floor(100 - (($file_diff / $file_size) * 100));
            echo ("Size: $file_size - Diff: $file_diff - As %: $file_lose -- ");
            if ($file_size < 1024 * 1024 * 1024) {
                $img = imagecreatefromjpeg($this_file);
            } else {
                $img = false;
                echo ("TOO BIG!!!\n");
            }

            if ($img) {
                if (($x = imagejpeg($img, $this_file, $file_lose))) {
                    echo (" reduced.");
                } else {
                    echo ("error.");
                }

                clearstatcache();
                print " New size: " . filesize($this_file) . "\n";
                imagedestroy($img);
            } else {
                echo ("Error opening.\n");
            }
        } else {
            echo (" OK - $file_size\n");
        }

        return;
    }

    /*
     * make_movie_thumb()
     * @param array parameters
     * @todo
     */

    public function image_resize($img_src, $img_dst, $max_height = null, $max_width = null, $constrain = false)
    {
        $error = [];
        if (($max_height == null && $max_width == null)) {
            $error[] = "Both max_height and max_width cannot be null.";
        }
        if ((!null === $max_height && $max_height <= 0) || (!null === $max_width && $max_width <= 0)) {
            $error[] = "Max_height and max_width have to be above 0 or null.";
        }
        if (!file_exists($img_src)) {
            $error[] = "File $img_src doesn't exist.";
        }
        if (($img_data = @getimagesize($img_src)) == false) {
            $error[] = "Failed to open image to get meta/size.";
        }

        if (count($error) == 0) {
            $mime   = $img_data['mime'];
            $width  = $img_data[0];
            $height = $img_data[1];

            if ($max_height == null) {
                $max_height = $height;
            }
            if ($max_width == null) {
                $max_width = $width;
            }

            switch ($mime) {
                case 'image/jpeg':
                    $old_img = imagecreatefromjpeg($img_src);
                    break;

                case 'image/gif':
                    $old_img = imagecreatefromgif($img_src);
                    break;

                case 'image/png':
                    $old_img = imagecreatefrompng($img_src);
                    break;
            }

            if ($old_img) {
                /*
                 * Get new dimensions
                 */
                if ($width > $max_width && $height > $max_height) {
                    $new_width  = $max_width;
                    $new_height = $max_height;
                } else if ($width > $max_width && $height <= $max_height) {
                    $ratio      = $width / $height;
                    $amount     = $width - $max_width;
                    $new_width  = $width - $amount;
                    $new_height = $height - ($amount / $ratio);
                } else if ($width <= $max_width && $height > $max_height) {
                    $ratio      = $height / $width;
                    $amount     = $height - $max_height;
                    $new_height = $height - $amount;
                    $new_width  = $width - ($amount / $ratio);
                } else {
                    $new_height = $height;
                    $new_width  = $width;
                }

                $new_img = imagecreatetruecolor($new_width, $new_height);
                if ($new_img) {
                    if (!@imagecopyresampled($new_img, $old_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height)) {
                        $error[] = "Failed to copyresampled from \$new_img to \$old_img";
                    }
                    if (!@imagejpeg($new_img, $img_dst)) {
                        $error[] = "Failed to properly resize imagejpeg.";
                    }
                } else {
                    $error[] = "Failed to create truecolor image.";
                }
            } else {
                $error[] = "Failed to create new image: imagecreatefrom - $mime";
            }
        }

        @imagedestroy($old_img);
        @imagedestroy($new_img);

        if (count($error) == 0) {
            return true;
        } else {
            $error = implode("\n", $error);
            msg($error);

            return false;
        }
    }

    /**
     * @param $img_src
     * @param $img_dst
     * @param $thumbnail_width
     * @param $thumbnail_height
     * @param $debug
     */
    public static function thumb_centered($img_src, $img_dst, $thumbnail_width, $thumbnail_height = 0, $debug = false)
    {

        if ( strpos($thumbnail_width, 'x') !== false ) {
            list($thumbnail_width, $thumbnail_height) = explode('x', $thumbnail_width);
        }

        $success = true;
        if (file_exists($img_src)) {
            if (($image_dimn = @getimagesize($img_src))) {
                list($width_orig, $height_orig) = $image_dimn;

                if ($width_orig < $thumbnail_width || $height_orig < $thumbnail_height) {
                    $success = "{$width_orig}x{$height_orig} too small.";
                } else {
                    $ratio_orig = $width_orig / $height_orig;
                    if ($thumbnail_width / $thumbnail_height > $ratio_orig) {
                        $new_height = $thumbnail_width / $ratio_orig;
                        $new_width  = $thumbnail_width;
                    } else {
                        $new_width  = $thumbnail_height * $ratio_orig;
                        $new_height = $thumbnail_height;
                    }
                    $x_mid  = $new_width / 2;  //horizontal middle
                    $y_mid  = $new_height / 2; //vertical middle
                    $my_img = @imagecreatefromjpeg($img_src);
                    if ($my_img) {
                        $process = @imagecreatetruecolor(round($new_width), round($new_height));
                        if ($process) {
                            @imagecopyresampled($process, $my_img, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
                            $thumb = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
                            if ($thumb) {
                                if (!@imagecopyresampled($thumb, $process, 0, 0, ($x_mid - ($thumbnail_width / 2)), ($y_mid - ($thumbnail_height / 2)), $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height)) {
                                    msg("Image copy resampled from \$process to \$thumb failed.\n");
                                }
                                if (!@imagejpeg($thumb, $img_dst)) {
                                    msg("imagejpeg from \$thumb to $img_dst failed\n");
                                }
                                imagedestroy($thumb);
                            } else {
                                $success = "Failed to create \$thumb.";
                            }
                            imagedestroy($process);
                        } else {
                            $success = "Failed to create true color \$process.";
                        }
                        imagedestroy($my_img);
                    } else {
                        $success = "Failed to icfj $img_src";
                    }
                }
            } else {
                $success = "$img_src isn't a valid image.";
            }
        } else {
            $success = "$img_src doesn't exist.";
        }
        if ($success !== true) {
            $success = "[ $img_src / $img_dst ] $success\n";
        }

        return $success;
    }

    /**
     * @param $movie_path
     * @param $image_path
     */
    public static function video_frame($movie_file, $thumb_file, $frame = 10)
    {
        @unlink($thumb_file);
        $cmd = "ffmpeg -ss $frame -i $movie_file -vframes 1 -y -f mjpeg -q:v 1 $thumb_file 2>&1";

        $ret = `$cmd`;
        if ( is_file($thumb_file) && filesize($thumb_file) != 0 )
            return true;
        unlink($thumb_file);
        return false;
    }

    /**
     * Returns some meta data about a movie. This could be expanded to include more
     * meta, however, I've just added what we need to know.
     *
     * @param  string $path       Path to local movie file.
     * @return array  'duration', 'dhms', 'hours', 'mins', 'secs', 'dimn', 'height', 'width'
     */
    public static function video_meta($path)
    {
        $error = [];
        $ret   = false;
        if (file_exists($path)) {
            $data = `ffmpeg -i $path 2>&1`;

            $duration = $dimn = $height = $width = $hours = $mins = $secs = false;
            // Find the HH:MM:SS.ss length ofthe movie
            $m = preg_match('/Duration: (\d+):(\d+):(\d+)\.(\d+),/', $data, $match);
            if ($m) {
                $hours    = $match[1];
                $mins     = $match[2];
                $secs     = $match[3];
                $duration = ($hours * 60 * 60) + ($mins * 60) + ($secs);
                $raw      = secs_to_dhms($duration);
                $dhms     = $raw['dhms'];
            }

            if ($duration !== false && $duration != 0) {
                // Find standard 'Video: string, string, WxH' output and deduce amts.
                $m = preg_match('/Video: (.+?), (.+?), (\d+)x(\d+)/', $data, $match);
                if ($m) {
                    $height = $match[4];
                    $width  = $match[3];
                    $dimn   = $width . "x" . $height;
                }
            }

            /*
             * Go ahead and send the array of broken down ifnformation.
             */
            if (false !== $duration && false !== $dimn) {
                msg("Found the movie metadata via ffmpeg.\n");
                $ret = compact('duration', 'dhms', 'hours', 'mins', 'secs', 'dimn', 'height', 'width');

                return $ret;
            }
        }
        msg("Failed to retrieve metadata.\n");

        return $ret;
    }

}
