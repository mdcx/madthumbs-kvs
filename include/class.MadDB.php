<?php
/**
 * MadDB Generic Database Abstraction
 * Based off of EZSQL {@link http://php.justinvincent.com Justin Vincent (justin@visunet.ie)}
 *
 * @version     0.2
 * @author Mike Campbell <mike@mdcx.io>
 * @copyright Copyright (c) 2017, mdcx.io
 */
define('R_ARRAY', 'R_ARRAY');
define('R_ARRAY_K', 'R_ARRAY_K');
define('R_ASSOC', 'R_ASSOC');
define('R_ASSOC_K', 'R_ASSOC_K');
define('R_OBJECT', 'R_OBJECT');
define('R_OBJECT_K', 'R_OBJECT_K');
define('R_ERROR_PREFIX', 'MadDB ERROR: ');
if (!defined('ALL')) {
    define('ALL', 'all');
}
if (!defined('IS_NULL')) {
    define('IS_NULL', ' is null');
}
if (!defined('NOT_NULL')) {
    define('NOT_NULL', ' <> ""');
}

class MadDB
{
    const PLACEHOLDER_ERROR_PREFIX = 'PREP_ERR:';

    /**
     * @var string
     */
    public $_escape_char = '`';

    /**
     * @var string
     */
    public $_like_escape_chr = '';

    /**
     * @var string
     */
    public $_like_escape_str = '';

    /**
     * @var mixed
     */
    public $field_types;

    /**
     * @var bool
     */
    public $save_queries = false;

    /**
     * @var array
     */
    private $_aliased_tables = array();

    /**
     * @var bool
     */
    private $_protect_identifiers = true;

    /**
     * @var array
     */
    private $_reserved_identifiers = array('*');

    /**
     * @var int
     */
    private $affected_rows = 0;

    /**
     * @var string
     */
    private $bind_marker = '?';

    /**
     * @var string
     */
    private $charset = 'utf8';

    /**
     * @var array
     */
    private $col_info;

    /**
     * @var array
     */
    private $col_names = array();

    /**
     * @var string
     */
    private $collate = 'utf8_general_ci';

    /**
     * @var string
     */
    private $database_host;

    /**
     * @var string
     */
    private $database_name;

    /**
     * @var string
     */
    private $database_pass;

    /**
     * @var string
     */
    private $database_port;

    /**
     * @var string
     */
    private $database_user;

    /**
     * @var \mysqli
     */
    private $dbh = false;

    /**
     * @var bool|int
     */
    private $dbh_age = false;

    /**
     * @var bool
     */
    private $dbh_passed = false;

    /**
     * @var int
     */
    private $dbh_timeout = 15;

    /**
     * @var string
     */
    private $error_handler = 'msg';

    /**
     * @var string
     */
    private $last_error = '';

    /**
     * @var bool
     */
    private $last_found_rows_result = false;

    /**
     * @var int
     */
    private $last_insert_id = 0;

    /**
     * @var string
     */
    private $last_query;

    /**
     * @var array|bool
     */
    private $last_result;

    /**
     * @var int
     */
    private $num_queries = 0;

    /**
     * @var int
     */
    private $num_rows = 0;

    /**
     * @var array
     */
    private $queries = array();

    /**
     * @var int
     */
    private $query_id = 0;

    /**
     * @var bool
     */
    private $ready = false;

    /**
     * @var bool
     */
    private $real_escape = true;

    /**
     * @var \mysqli_result
     */
    private $result;

    /**
     * @var bool
     */
    private $show_errors = true;

    /**
     * @var array
     */
    private $timers = [];

    /**
     * MadDB constructor
     *
     * @return bool|\MadDB
     */
    public function __construct()
    {

        if ( !class_exists('Timer') ) {
            $check = dirname(__FILE__) . '/class.Timer.php';
            if ( file_exists($check) ) require_once($check);
            else die("Couldn't load $check\n");
        }

        $database_user = false;
        $database_pass = false;
        $database_name = false;
        $database_host = 'localhost';
        $database_port = false;
        $charset       = $this->charset;
        $collate       = $this->collate;
        $save_queries  = $this->save_queries;
        $error_handler = $this->error_handler;

        $args = func_get_args();

        $argless = false;
        if (count($args) == 0) {
            $argless = true;
        } elseif (count($args) == 1) {
            $args = $args[0];
            /*
             * Is it an array of parameters?
             */
            if (is_array($args)) {
                if (empty($args['database_user']) || empty($args['database_pass']) || empty($args['database_name'])) {
                    die("MadDB: The parameter array must contain the following values: database_user, database_pass, database_name. (opt: database_host, database_port)\n");
                } else {
                    extract($args, EXTR_IF_EXISTS);
                }

                if ($charset != $this->charset) {
                    $this->charset = $charset;
                }

                if ($collate != $this->collate) {
                    $this->collate = $collate;
                }

                if ($save_queries != $this->save_queries) {
                    $this->save_queries = $save_queries;
                }

                if ($error_handler != $this->error_handler) {
                    $this->error_handler = $error_handler;
                }
            }
            /*
             * Or is it an already live db handle?
             */
            elseif (is_object($args)) {
                $this->dbh        = $args;
                $this->ready      = true;
                $this->dbh_passed = true;
            } else {
                die('wtf');
            }
        } elseif (count($args) < 3) {
            var_dump($args);
            die("MadDB: Parameters for this class must be sent in the form of a single indexed array OR in typical \$user, \$pass, \$database_name form.\n");
        } else {
            list($database_user, $database_pass, $database_name) = $args;
            if (isset($args[3])) {
                $database_host = $args[3];
            }
            if (isset($args[4])) {
                $database_port = $args[4];
            }
        }

        /*
         * Set charset vars
         */
        $this->_init_charset();
        if (!$argless && !$this->dbh) {
            $this->database_user = $database_user;
            $this->database_pass = $database_pass;
            $this->database_name = $database_name;
            $this->database_host = $database_host;

            if (strpos($database_host, ':') === false) {
                if ($database_port) {
                    $this->database_host .= ':' . $database_port;
                    $this->database_port = $database_port;
                }
                // else
                //     $this->database_host .= ':3306';
            }
            $this->ready = $this->db_connect();
        }

        /*
         * Assuming all went well, finalize.
         */
        $this->db_init();
        if (!$this->ready) {
            return false;
        }

        return $this;
    }

    /**
     * _delete_helper
     *
     * @param  null          $table
     * @param  array         $where
     * @param  array         $like
     * @param  bool          $limit
     * @return bool|string
     */
    public function _delete_helper($table = null, $where = array(), $like = array(), $limit = false)
    {
        if (null === $table) {
            return false;
        }

        if (!is_array($where)) {
            $where = array($where);
        }

        if (!is_array($like)) {
            $like = array($like);
        }

        $conditions = '';

        if (count($where) > 0 or count($like) > 0) {
            $conditions = "\nWHERE ";
            $conditions .= implode("\n", $where); //

            if (count($where) > 0 && count($like) > 0) {
                $conditions .= " AND ";
            }
            $conditions .= implode("\n", $like);
        }

        $limit = (!$limit) ? '' : ' LIMIT ' . $limit;

        return "DELETE FROM " . $table . $conditions . $limit;
    }

    /**
     * _escape_identifiers
     *
     * @param  string  $item
     * @return mixed
     */
    public function _escape_identifiers($item)
    {
        if ($this->_escape_char == '') {
            return $item;
        }

        foreach ($this->_reserved_identifiers as $id) {
            if (strpos($item, '.' . $id) !== false) {
                $str = $this->_escape_char . str_replace('.', $this->_escape_char . '.', $item);

                // remove duplicates if the user already included the escape

                return preg_replace('/[' . $this->_escape_char . ']+/', $this->_escape_char, $str);
            }
        }

        if (strpos($item, '.') !== false) {
            $str = $this->_escape_char . str_replace('.', $this->_escape_char . '.' . $this->_escape_char, $item) . $this->_escape_char;
        } else {
            $str = $this->_escape_char . $item . $this->_escape_char;
        }

        // remove duplicates if the user already included the escape

        return preg_replace('/[' . $this->_escape_char . ']+/', $this->_escape_char, $str);
    }

    /**
     * Protect Identifiers
     * This function is used extensively by the Active Record class, and by
     * a couple functions in this class.
     * It takes a column or table name (optionally with an alias) and inserts
     * the table prefix onto it.  Some logic is necessary in order to deal with
     * column names that include the path.  Consider a query like this:
     * SELECT * FROM hostname.database.table.column AS c FROM hostname.database.table
     * Or a query with aliasing:
     * SELECT m.member_id, m.member_name FROM members AS m
     * Since the column name can include up to four segments (host, DB, table, column)
     * or also have an alias prefix, we need to do a bit of work to figure this out and
     * insert the table prefix (if it exists) in the proper position, and escape only
     * the correct identifiers.
     *
     * @param  string                     $item
     * @param  bool                       $prefix_single
     * @param  null                       $protect_identifiers
     * @param  bool                       $field_exists
     * @return array|bool|mixed|string
     */
    public function _protect_identifiers($item, $prefix_single = false, $protect_identifiers = null, $field_exists = true)
    {
        if (!is_bool($protect_identifiers)) {
            $protect_identifiers = $this->_protect_identifiers;
        }

        if (is_array($item)) {
            $escaped_array = array();

            foreach ($item as $k => $v) {
                $escaped_array[$this->_protect_identifiers($k)] = $this->_protect_identifiers($v);
            }

            return $escaped_array;
        }

        // This is basically a bug fix for queries that use MAX, MIN, etc.
        // If a parenthesis is found we know that we do not need to
        // escape the data or add a prefix.  There's probably a more graceful
        // way to deal with this, but I'm not thinking of it -- Rick
        if (strpos($item, '(') !== false) {
            return $item;
        }

        // Convert tabs or multiple spaces into single spaces
        $item = preg_replace('/[\t ]+/', ' ', $item);

        // If the item has an alias declaration we remove it and set it aside.
        // Basically we remove everything to the right of the first space
        $alias = '';
        if (strpos($item, ' ') !== false) {
            $alias = strstr($item, " ");
            $item  = substr($item, 0, -strlen($alias));
        }

        // Break the string apart if it contains periods, then insert the table prefix
        // in the correct location, assuming the period doesn't indicate that we're dealing
        // with an alias. While we're at it, we will escape the components
        if (strpos($item, '.') !== false) {
            $parts = explode('.', $item);

            // Does the first segment of the exploded item match
            // one of the aliases previously identified?  If so,
            // we have nothing more to do other than escape the item
            if (in_array($parts[0], $this->_aliased_tables)) {
                if ($protect_identifiers === true) {
                    foreach ($parts as $key => $val) {
                        if (!in_array($val, $this->_reserved_identifiers)) {
                            $parts[$key] = $this->_escape_identifiers($val);
                        }
                    }

                    $item = implode('.', $parts);
                }

                return $item . $alias;
            }

            if ($protect_identifiers === true) {
                $item = $this->_escape_identifiers($item);
            }

            return $item . $alias;
        }

        if ($protect_identifiers === true and !in_array($item, $this->_reserved_identifiers)) {
            $item = $this->_escape_identifiers($item);
        }

        return $item . $alias;
    }

    /**
     * change_dbh
     *
     * @param  \mysqli $dbh
     * @param  string  $database_name
     * @param  string  $database_host
     * @param  string  $database_user
     * @param  string  $database_pass
     * @return bool
     */
    public function change_dbh($dbh, $database_name = '', $database_host = '', $database_user = '', $database_pass = '')
    {
        $this->ready = false;
        if (is_object($dbh) && @$dbh->ping()) {
            if ($database_name && $database_name != '') {
                $this->database_name = $database_name;
                $this->db_init();
            }
            foreach (['host', 'user', 'pass'] as $f) {
                $fn = 'database_' . $f;
                if (${$fn} !== '') {
                    $this->$fn = ${$fn};
                }
            }
            $this->dbh        = $dbh;
            $this->dbh_passed = true;
            $this->ready      = true;
        }

        return $this->ready;
    }

    /**
     * Close the DB Handle
     *
     * @return bool
     */
    public function close()
    {
        return $this->dbh->close();
    }

    /**
     * col_info
     *
     * @return array
     */
    public function col_info()
    {
        return $this->col_info;
    }

    /**
     * col_names
     *
     * @param  string  $table
     * @return array
     */
    public function col_names($table = '')
    {
        if ($table == '') {
            return $this->col_names;
        }

        if (empty($this->col_names[$table])) {
            $columns = $this->get_results('SHOW COLUMNS FROM ' . $table, 'R_OBJECT');
            foreach ($columns as $ck => $column) {
                $this->col_names[$table][$column->Field] = 1;
            }
        }
        $this->flush();

        return array_keys($this->col_names[$table]);
    }

    /**
     * Compile Bindings
     *
     * @param  $sql     string the sql statement
     * @param  $binds   array  an array of bind data
     * @return string
     */
    public function compile_binds($sql, $binds)
    {
        if (strpos($sql, $this->bind_marker) === false) {
            return $sql;
        }

        if (!is_array($binds)) {
            $binds = array($binds);
        }

        // Get the sql segments around the bind markers
        $segments = explode($this->bind_marker, $sql);

        // The count of bind should be 1 less then the count of segments
        // If there are more bind arguments trim it down
        if (count($binds) >= count($segments)) {
            $binds = array_slice($binds, 0, count($segments) - 1);
        }

        // Construct the binded query
        $result = $segments[0];
        $i      = 0;
        foreach ($binds as $bind) {
            if (!is_numeric($bind)) {
                $bind = $this->escape($bind, true);
            }
            $result .= $bind;
            $result .= $segments[++$i];
        }

        return $result;
    }

    /**
     * Retrieve the database handle
     *
     * @return \mysqli
     */
    public function conn()
    {
        return $this->dbh;
    }

    /**
     * db_do
     *
     * @param  string  $function The name of the class method to run.
     * @param  null    $p1
     * @param  null    $p2
     * @param  null    $p3
     * @param  null    $p4
     * @param  null    $p5
     * @return mixed
     */
    public function db_do($function, $p1 = null, $p2 = null, $p3 = null, $p4 = null, $p5 = null)
    {
        switch (func_num_args()) {
            case 1:
                return $this->{$function}();
            case 2:
                return $this->{$function}($p1);
                break;
            case 3:
                return $this->{$function}($p1, $p2);
                break;
            case 4:
                return $this->{$function}($p1, $p2, $p3);
                break;
            case 5:
                return $this->{$function}($p1, $p2, $p3, $p4);
                break;
            case 6:
                return $this->{$function}($p1, $p2, $p3, $p4, $p5);
        }

        return $this->{$function}();
    }

    // public interface to internal driver methods

    /**
     * Database Select
     * Allows you to change the current DB for the current connection, or
     * a different one if a db handle is passed.
     *
     * @param  string    $db
     * @param  \mysqli   $dbh
     * @return boolean
     */
    public function db_select($db, $dbh = null)
    {
        if (null === $dbh) {
            $dbh = $this->dbh;
        }

        if (!$this->ready) {
            $this->fatal("No connection available.");

            return false;
        }

        if (!@$dbh->select_db($db)) {
            $this->ready = false;
            $this->fatal("Failed to select database.");

            return false;
        }

        return true;
    }

    /**
     * delete
     *
     * @param  string     $table
     * @param  array      $where
     * @return bool|int
     */
    public function delete($table, $where = array())
    {
        if (!is_array($where)) {
            return false;
        }

        if (count($where) == 0) {
            return $this->truncate($table);
        }

        $cond = array();
        foreach ($where as $k => $v) {
            $cond[] = $this->_protect_identifiers($k) . ' = ' . $this->_real_escape($v);
        }

        $cond  = implode(' AND ', $cond);
        $query = "DELETE FROM $table WHERE $cond";

        return $this->query($query);
    }

    /**
     * Escape a string using real_escape if available, addslashes if not.
     *
     * @param  string|array $data
     * @param  bool         $add_quotes
     * @return mixed
     */
    public function escape($data, $add_quotes = false)
    {
        if (is_array($data)) {
            foreach ((array) $data as $k => $v) {
                $data[$k] = $this->escape($v, $add_quotes);
            }
        } else {
            if (is_numeric($data)) {
                $int_str = (int) $data;
                if ($int_str == $data) {
                    return $data;
                }
            }

            if (is_string($data)) {
                $data = $this->_real_escape($data);
                if ($add_quotes) {
                    $data = "'" . $data . "'";
                }
            } elseif (is_bool($data)) {
                $data = ($data === false) ? 0 : 1;
            } elseif (null === $data) {
                $data = 'NULL';
            }
        }

        return $data;
    }

    /**
     * Execute a query with place holders, return result resource.
     *
     * @param  string         $query The parametrized q uery.
     * @return bool|\stdClass False on failure, or the query results.
     */
    /**
     * @return mixed
     */
    public function execute($query)
    {
        $format = R_OBJECT;
        $args   = func_get_args();

        $query = array_shift($args);

        $sql = $this->prepare($query, $args);

        if ($sql) {
            return $this->query_execute($sql);
        } else {
            vd([$query, $args]);
            die("prep failed for: $query");
        }
    }

    /**
     * Execute query with placeholders, return a result array
     *
     * @param  string $query
     * @return mixed  Database query results
     */
    public function fetch()
    {
        $format = R_OBJECT;
        $args   = func_get_args();

        $query = array_shift($args);

        if (count($args) === 0) {
            return $this->get_results($query, $format);
        }

        // allow for (q,arg,arg,'R_OBJECT')
        if (is_string($args[count($args) - 1]) && substr($args[count($args) - 1], 0, 2) == 'R_') {
            $format = array_pop($args);
        }

        $sql = $this->prepare($query, $args);

        if ($sql) {
            return $this->get_results($sql, $format);
        } else {
            die("prep failed for: $query");
        }
    }

    /**
     * Used for performing a query using the bind method for inserting variables.
     * Also accepts the normal return type R_* vars.
     *
     * @param  string  $query
     * @return array
     */
    public function fetch_row($query)
    {
        $format = R_OBJECT;
        $args   = func_get_args();
        $query  = array_shift($args);

        // allow for (q,arg,arg,'R_OBJECT')
        if (substr($args[count($args) - 1], 0, 2) == 'R_') {
            $format = array_pop($args);
        }

        if (count($args) !== substr_count($query, '?')) {
            $row = array_pop($args);
        } else {
            $row = 0;
        }
        $sql = $this->prepare($query, $args);

        return $this->get_row($sql, $row, $format);
    }

    /**
     * @return mixed
     */
    public function fetch_var()
    {
        $format  = R_OBJECT;
        $args    = func_get_args();
        $query   = array_shift($args);
        $default = [0, 0];
        if (count($args) > 0) {
            if (($ac = count($args)) > ($pc = substr_count($query, '?'))) {
                $pop = $ac - $pc;
                for ($i = 0; $i < $pop; $i++) {
                    $default[$i] = array_shift($args);
                }
            }
        }
        $sql = $this->prepare($query, $args);

        return $this->get_var($sql, $default[0], $default[1]);
    }

    /**
     * @return mixed
     */
    public function found_rows()
    {
        return $this->last_found_rows_result;
    }

    /**
     * @return mixed
     */
    public function free_result()
    {
        return $this->flush();
    }

    /**
     * Retrieve one column from the database.
     * Executes a SQL query and returns the column from the SQL result.
     * If the SQL result contains more than one column, this function returns the column specified.
     * If $query is null, this function returns the specified column from the previous SQL result.
     *
     * @param  string|null $query   Optional. SQL query. Defaults to previous query.
     * @param  int         $x       Optional. Column to return. Indexed from 0.
     * @return array       Database query result. Array indexed from by SQL result row number.
     */
    public function get_col($query = null, $x = 0)
    {
        if ($query) {
            $this->query($query);
        }

        $new_array = array();
        // Extract the column values
        for ($i = 0, $j = count($this->last_result); $i < $j; $i++) {
            if (!is_numeric($x)) {
                $new_array[$i] = isset($this->last_result[$i]->$x) ? $this->last_result[$i]->$x : '';
            } else {
                $new_array[$i] = $this->get_var(null, $x, $i);
            }
        }

        return $new_array;
    }

    /**
     * get_col_info
     *
     * @param  string       $info_type
     * @param  int          $col_offset
     * @return array|bool
     */
    public function get_col_info($info_type = 'name', $col_offset = -1)
    {
        if ($this->col_info) {
            if ($col_offset == -1) {
                $i         = 0;
                $new_array = array();
                foreach ((array) $this->col_info as $col) {
                    $new_array[$i] = $col->{$info_type};
                    $i++;
                }

                return $new_array;
            } else {
                return $this->col_info[$col_offset]->{$info_type};
            }
        }

        return false;
    }

    /**
     * @param  $query
     * @return mixed
     */
    public function get_keyd_col($query = null)
    {
        if ($query) {
            $this->query($query);
        }
        $new_array = array();
        for ($i = 0, $j = count($this->last_result); $i < $j; $i++) {
            $cur             = (array) $this->last_result[$i];
            $key             = array_shift($cur);
            $new_array[$key] = array_shift($cur);
        }

        return $new_array;
    }

    /**
     * @param  $query
     * @return mixed
     */
    public function get_objects($query = null)
    {
        return $this->get_results($query, R_OBJECT);
    }

    /**
     * Retrieve an entire SQL result set from the database (i.e., many rows)
     * Executes a SQL query and returns the entire SQL result.
     *
     *                       Each row is an associative array (column => value, ...), a numerically indexed arr ay (0 => value, ...), or an object. ( ->column = value ), respectively.
     *                       With OBJECT_K, return an associative array of row objects keyed by the value of each row's first column's value.  Duplicate keys are discarded.
     * @param  string $query   SQL query.
     * @param  string $output  Optional. Any of ARRAY_A | ARRAY_N | OBJECT | OBJECT_K constants. With one of the first three, return an array of rows indexed from by SQL result row number.
     * @return mixed  Database query results
     */
    public function get_results($query = null, $output = R_OBJECT)
    {

        $is_outer = $this->timer()->add('full');

        $this->timer()->start('get_results');

        if ($query) {
            $this->query($query);
        }

        $new_array = array();

        if (!$this->last_result) {
            $new_array = [];
        } elseif ($output == R_OBJECT) {
            // Return an integer-keyed array of row objects
            $new_array = $this->last_result;
        } elseif ($output == R_OBJECT_K) {
            // Return an array of row objects with keys from column 1
            // (Duplicates are NOT  overwritten)
            foreach ($this->last_result as $row) {
                $var_by_ref = get_object_vars($row);
                $key        = array_shift($var_by_ref);
                $new_array[$key] = $row;
            }
        } elseif ($output == R_ARRAY || $output == R_ASSOC || $output == R_ASSOC_K || $output == R_ARRAY_K) {
            // Return an integer-keyed array of...
            if ($this->last_result) {
                foreach ((array) $this->last_result as $row) {
                    if ($output == R_ARRAY) {
                        // ...integer-keyed row arrays
                        $new_array[] = array_values(get_object_vars($row));
                    } elseif ($output == R_ASSOC) {
                        // ...column name-keyed row arrays
                        $new_array[] = get_object_vars($row);
                    } elseif ($output == R_ARRAY_K) {
                        $var_by_ref      = get_object_vars($row);
                        $tmp_row         = $var_by_ref;
                        $key             = array_shift($var_by_ref);
                        $new_array[$key] = array_values($tmp_row);
                    } elseif ($output == R_ASSOC_K) {
                        $var_by_ref      = get_object_vars($row);
                        $tmp_row         = $var_by_ref;
                        $key             = array_shift($var_by_ref);
                        $new_array[$key] = $tmp_row;
                    }
                }
            }
        }

        $this->timer()->stop('get_results');
        if ($is_outer) {
            $this->timer_log();
        }

        return $new_array;
    }

    /**
     * Retrieve one row from the database.
     * Executes a SQL query and returns the row from the SQL result.
     *
     *                            a numerically indexed array (0 => value, ...) or an object ( ->column = value ), respectively.
     * @param  string|null $query   SQL query.
     * @param  int         $y       Optional. Row to return. Indexed from 0.
     * @param  string      $output  Optional. one of ARRAY_A | ARRAY_N | OBJECT constants. Return an associative array (column => value, ...),
     * @return mixed       Database query result in format specified by $output or null on failure
     */
    public function get_row($query = null, $y = 0, $output = R_OBJECT)
    {
        if ($query) {
            $this->query($query);
        } elseif (!$this->last_result) {
            return null;
        }

        if (!isset($this->last_result[$y])) {
            return null;
        } else {
            switch ($output) {
                case R_OBJECT:return $this->last_result[$y];
                    break;
                case R_ASSOC:return get_object_vars($this->last_result[$y]);
                    break;
                case R_ARRAY:return array_values(get_object_vars($this->last_result[$y]));
                    break;
                default:return null;
            }
        }
    }

    /**
     * Retrieve one variable from the database.
     * Executes a SQL query and returns the value from the SQL result.
     * If the SQL result contains more than one column and/or more than one row, this function returns the value in the column and row specified.
     * If $query is null, this function returns the value in the specified column and row from the previous SQL result.
     *
     * @param  string|null $query   Optional. SQL query. Defaults to null, use the result from the previous query.
     * @param  int         $x       Optional. Column of value to return. Indexed from 0.
     * @param  int         $y       Optional. Row of value to return. Indexed from 0.
     * @return string|null Database query result (as string), or null on failure
     */
    public function get_var($query = null, $x = 0, $y = 0)
    {
        if ($query) {
            $this->query($query);
        }

        // Extract var out of cached results based x,y vals
        $values = !empty($this->last_result[$y]) ? array_values(get_object_vars($this->last_result[$y])) : [];

        // If there is a value return it else return null

        return (isset($values[$x]) && $values[$x] !== '') ? $values[$x] : null;
    }

    /**
     * Format an array for usage in queries.
     * Takes an input array and escapes the strings, adds single quotes and then
     * implodes on a comma.
     *
     * @param  array    $data
     * @param  string   $sep
     * @param  bool     $add_quotes
     * @return string
     */
    public function imploder($data, $sep = ",", $add_quotes = true)
    {
        if (!is_array($data)) {
            $data = array($data);
        }

        $data = $this->escape($data, $add_quotes);

        return implode($sep, $data);
    }

    /**
     * insert
     *
     * @param  $table
     * @param  $data
     * @param  null        $format
     * @param  bool        $return
     * @return false|int
     */
    public function insert($table, $data, $return = false)
    {
        $return = $return !== false ? 'RETURN' : 'INSERT';

        return $this->_insert_replace_helper($table, $data, $return);
    }

    /**
     * @return mixed
     */
    public function insert_id()
    {
        if ($this->last_insert_id != 0) {
            return $this->last_insert_id;
        }

        return false;
    }

    /**
     * Send an array of data to insert.
     *
     * @param  string  $table
     * @param  array   $data
     * @param  string  $format
     * @return array
     */
    public function insert_multiple($table, $data, $format)
    {
        $ret = array();
        if (is_array($data)) {
            foreach ($data as $dk => $item) {
                $ret[$dk] = $this->_insert_replace_helper($table, $item, $format, 'INSERT');
            }
        }

        return $ret;
    }

    /**
     * insert_or_update
     *
     * @param  $table
     * @param  $key
     * @param  array                   $data
     * @return bool|false|int|string
     */
    public function insert_or_update($table, $key, $data = array())
    {
        if (empty($data[$key])) {
            return false;
        }

        $check = "SELECT $key FROM $table WHERE $key = '" . $this->escape($data[$key]) . "'";
        $check = $this->get_objects($check);
        if (count($check) > 0) {
            return $this->update($table, $key, $data);
        } else {
            return $this->insert($table, $data);
        }
    }

    /**
     * @return mixed
     */
    public function last_error()
    {
        return $this->last_error;
    }

    /**
     * @return mixed
     */
    public function last_query()
    {
        return $this->last_query;
    }

    /**
     * @return mixed
     */
    /**
     * @return mixed
     */
    public function num_rows()
    {
        return $this->num_rows;
    }

    /**
     * prepare arguments for query
     * @param $query  string The query with ? placeholders
     * @param $params array  An array of replacements [1,2,3]
     */
    public function prepare()
    {
        $args = func_get_args();

        if (is_array($args[1])) {
            $result = $this->_prepare_exec($this->last_query = $args[0], $args[1], $error);
        } else {
            $tmpl   = array_shift($args);
            $result = $this->_prepare_exec($this->last_query = $tmpl, $args, $error);
        }

        if ($result === false) {
            $this->print_error($error);

            return false;
        } else {
            return $result;
        }
    }

    /**
     * @return mixed
     */
    public function prepare_debug()
    {
        $args   = func_get_args();
        $tmpl   = array_shift($args);
        $result = $this->_prepare_exec($tmpl, $args, $error);
        if ($result === false) {
            $error = "Placeholder substitution error. Diagnostics: \"$error\"";
            if (function_exists("debug_backtrace")) {
                $bt = debug_backtrace();
                $error .= " in " . @$bt[0]['file'] . " on line " . @$bt[0]['line'];
            }
            trigger_error($error, E_USER_WARNING);

            return false;
        }

        return $result;
    }

    /**
     * protect_identifiers
     *
     * @param  string                    $item
     * @param  bool                      $prefix_single
     * @param  null                      $protect_identifiers
     * @param  bool                      $field_exists
     * @return array|bool|mixed|string
     */
    public function protect_identifiers($item, $prefix_single = false, $protect_identifiers = null, $field_exists = true)
    {
        return $this->_protect_identifiers($item, $prefix_single, $protect_identifiers, $field_exists);
    }

    /**
     * Perform a query. Results will be stored in the DB object.
     *
     * @param  string       $query
     * @param  null|\mysqli $dbh     MySQL Connection Resource.
     * @return int
     */
    public function query($query, $dbh = null)
    {
        $is_outer = $this->timer()->add('full');

        $dbh !== null or $dbh = $this->dbh;
        $this->query_execute($query, $dbh);
        /*
         * Decide upon what to return here. For these statements, we just
         * return the raw result as they can only be true/false.
         */
        $this->timer()->start('query_fetch');
        if (preg_match('/^\s*(create|alter|truncate|drop) /i', $query)) {
            $return_val = $this->result;
        }
        /*
         * Take note of affected rows, as well as insert id.
         */
        elseif (preg_match('/^\s*(insert|delete|update|replace) /i', $query)) {
            $this->affected_rows = mysqli_affected_rows($dbh);
            if (preg_match('/^\s*(insert|replace) /i', $query)) {
                $this->last_insert_id = mysqli_insert_id($dbh);
                $return_val           = $this->last_insert_id;
            } else {
                $return_val = $this->affected_rows;
            }
        }
        /*
         * Otherwise, we have a query that is returning something.
         */
        else {
            $this->timer()->start('cols');
            $i = 0;
            while ($i < @mysqli_num_fields($this->result)) {
                $this->col_info[$i]                      = $tmp                      = @mysqli_fetch_field($this->result);
                $this->col_info[$tmp->table][$tmp->name] = $tmp;
                if (empty($this->col_names[$tmp->table]) || empty($this->col_names[$tmp->table][$tmp->name])) {
                    $this->col_names[$tmp->table][$tmp->name] = 1;
                }
                $i++;
            }
            $this->timer()->stop('cols');

            $this->timer()->start('rows');
            $num_rows = 0;
            while ($row = @mysqli_fetch_object($this->result)) {
                $this->last_result[$num_rows] = $row;
                $num_rows++;
            }
            $this->timer()->stop('rows');
            @mysqli_free_result($this->result);
            $this->result = null;

            // Log number of rows the query returned
            // and return number of rows selected
            $this->num_rows = $num_rows;
            $return_val     = $num_rows;
        }
        $this->timer()->stop('query_fetch');

        if ($is_outer) {
            $this->timer_log();
        }

        return $return_val;
    }

    /**
     * Perform a query and return the result object
     *
     * @param  string           $query
     * @param  null|\mysqli     $dbh     MySQL Connection Resource.
     * @return \mysqli_result
     */
    public function query_execute($query, $dbh = null)
    {
        $is_outer = $this->timer()->add('full');

        $this->check_dbh_age();

        if (is_array($query)) {
            $dbh   = $query[1];
            $query = $query[0];
        }

        if ($dbh == null) {
            $dbh = $this->dbh;
        }

        if (!$this->ready) {
            return false;
        }

        /*
         * Reset the query to query vars.
         */
        $this->flush();

        if (!$query) {
            return false;
        }

        $this->last_query = $query;

        /*
         * Run the query
         */
        if (preg_match('/^\s*SELECT\s+FOUND_ROWS(\s*)/i', $query) && is_resource($this->last_found_rows_result)) {
            $this->result = $this->last_found_rows_result;
        } else {

            $this->timer()->start('query_exec');
            $this->result = mysqli_query($dbh, $query);
            $this->timer()->stop('query_exec');
            $this->num_queries++;

            if (preg_match('/^\s*SELECT\s+SQL_CALC_FOUND_ROWS\s/i', $query)) {
                if (false === strpos($query, "NO_SELECT_FOUND_ROWS")) {
                    $this->last_found_rows_result = mysqli_query($dbh, "SELECT FOUND_ROWS(), FOUND_ROWS() as total");
                    $this->num_queries++;
                    $query .= "; SELECT FOUND_ROWS()";
                }
            } else {
                $this->last_found_rows_result = null;
            }
        }

        /*
         * Handle errors.
         */
        if (!$this->last_result && ($this->last_error = mysqli_error($dbh))) {
            $this->print_error($this->last_error);
            $this->last_result = false;
        }

        if ($is_outer) {
            $this->timer_log();
        }

        return $this->result;
    }

    public function query_report()
    {
        if (!is_cli()) {
            print '<pre>';
        }

        foreach ($this->queries as $query_id => $query_data) {
            extract($query_data);
            $format = "Query [ID: %4s] [total: %s] [ %s ]\n      ... %s\n\n";
            $l = [];
            $query = str_replace("','","', '", $query);
            $query = wordwrap($query, 100);
            $query = str_replace("\n", "\n      ... ", $query);
            foreach($timers as $tk=>$tv) {
                $l[] = "$tk: $tv";
            }

            print sprintf($format, $query_id, $total_time, implode(' | ', $l), $query);
        }
        if (!is_cli()) {
            print '</pre>';
        }

    }

    /**
     * replace
     *
     * @param  $table
     * @param  $data
     * @param  null        $format
     * @return false|int
     */
    public function replace($table, $data, $format = null)
    {
        return $this->_insert_replace_helper($table, $data, $format, 'REPLACE');
    }

    /**
     * Retrieve entire last result.
     *
     * @return array|bool
     */
    public function result()
    {
        return $this->last_result;
    }

    /**
     * Sets the connection's character set.
     *
     * @param \mysqli $dbh     The resource given by mysqli_connect
     * @param string  $charset The character set (optional)
     * @param string  $collate The collation (optional)
     */
    public function set_charset($dbh, $charset = null, $collate = null)
    {
        if (!isset($charset)) {
            $charset = $this->charset;
        }
        if (!isset($collate)) {
            $collate = $this->collate;
        }
        if ($this->has_cap('collation', $dbh) && !empty($charset)) {
            if (function_exists('mysqli_set_charset') && $this->has_cap('set_charset', $dbh)) {
                mysqli_set_charset($dbh, $charset);
            } else {
                $this->real_escape = false;
                $query             = $this->prepare('SET NAMES %s', $charset);
                if (!empty($collate)) {
                    $query .= $this->prepare(' COLLATE %s', $collate);
                }
                $this->dbh->query($query);
            }
        }
    }

    /**
     * truncate
     *
     * @param  $table
     * @return int
     */
    public function truncate($table)
    {
        $table = $this->_protect_identifiers($table);

        return $this->query("TRUNCATE $table");
    }

    /**
     * Update a table row setting array_key = array_value. Handles escaping.
     *
     * @param  string      $table The table the values are from.
     * @param  bool|string $key   The primary KEY for the query
     * @param  array       $arr   The array of table_field_names => field_values
     * @return string      mySQL Query to execute to update the array values in the DB.
     */
    public function update($table, $key = false, $arr = array())
    {
        $check = false;
        if (is_object($arr)) {
            $arr = (array) $arr;
        }

        $id_limit = false;

        /*
         * Allow for:
         *      $db->where('id',1);
         *      $db->update('tablename',$values);
         */
        if (isset($this->_activerecord) && $this->_activerecord !== false) {
            if (count($this->_activerecord['set']) > 0) {
                $arr = $this->_activerecord['set'];
            } elseif (is_array($key)) {
                $arr = $key;
                $key = false;
            }

            if (count($this->_activerecord['where']) > 0) {
                $id_limit = implode(' ', $this->_activerecord['where']);
                $check    = $key    = true;
            }
        }
        /*
         * Otherwise go with regular route.
         */
        else {
            if (!$key) {
                return false;
            } else {
                if (isset($arr[$key])) {
                    $val = $arr[$key];
                } else {
                    return false;
                }
            }
            unset($arr[$key]);

            if (!is_numeric($val)) {
                $val = "'" . $this->escape($val) . "'";
            }

            $id_limit = $key . ' = ' . $val;
        }

        /*
         * Make escaped update array.
         */
        $upd = array();
        foreach ($arr as $k => $v) {
            if (!is_numeric($v)) {
                $v = "'" . $this->escape($v) . "'";
            }
            $upd[] = "$k = $v";
        }

        if (count($upd) == 0) {
            return false;
        }

        $upd = implode(", ", $upd);

        $table = $this->_protect_identifiers($table);

        /*
         * Only check it's a real value when we're not using activerecord.
         */
        if (!$check) {
            if (!$key) {
                return false;
            }

            $check = $this->get_var("SELECT $key FROM $table WHERE $id_limit");
            if ($check === false) {
                return false;
            }
        }

        $query = "UPDATE $table SET $upd WHERE $id_limit";
        $x     = $this->query($query);

        return $x;
    }

    // --------------------------------------------------------------------

    /**
     * flush
     *
     * @return $this
     */
    protected function flush()
    {
        $this->last_result    = array();
        $this->col_info       = false;
        $this->last_query     = null;
        $this->last_insert_id = null;
        $this->num_rows       = 0;
        $this->affected_rows  = null;

        return $this;
    }

    /**
     * Retrieve the name of the function that called wpdb.
     * Searches up the list of functions until it reaches
     * the one that would most logically had called this method.
     *
     * @return string The name of the calling function
     */
    protected function get_caller()
    {
        $trace  = array_reverse(debug_backtrace());
        $caller = array();

        foreach ($trace as $call) {
            if (isset($call['class']) && __CLASS__ == $call['class']) {
                continue;
            } // Filter out wpdb calls.
            $caller[] = isset($call['class']) ? "{$call['class']}->{$call['function']}" : $call['function'];
        }

        return join(', ', $caller);
    }

    /**
     * Escape a string by reference.
     *
     * @param  $string
     * @return mixed|string
     */
    private function _escape_by_ref(&$string)
    {
        $string = $this->_real_escape($string);

        return $string;
    }

    /**
     * Set the collate/charset defaults. Uses utf8 if nothing is set
     * in the DB_COLLATE or DB_CHARSET constants.
     */
    private function _init_charset()
    {
        if (defined('DB_COLLATE')) {
            $this->collate = DB_COLLATE;
        }

        if (defined('DB_CHARSET')) {
            $this->charset = DB_CHARSET;
        }
    }

    /**
     * Helper function for insert and replace.
     * Runs an insert or replace query based on $type argument.
     *
     *                             A format is one of '%d', '%f', '%s' (integer, float, string). If omitted, all values in $data will be treated as strings unless otherwise specified in wpdb::$field_types.
     * @param  string       $table  table name
     * @param  array        $data   Data to insert (in column => value pairs). Both $data columns and $data values should be "raw" (neither should be SQL escaped).
     * @param  array|string $format Optional. An array of formats to be mapped to each of the value in $data. If string, that format will be used for all of the values in $data.
     * @param  string       $type   Optional. What type of operation is this? INSERT or REPLACE. Defaults to INSERT.
     * @return int|false    The number of rows affected, or false on error.
     */
    private function _insert_replace_helper($table, $data, $type = 'INSERT')
    {
        if (!in_array(strtoupper($type), array('REPLACE', 'INSERT', 'RETURN'))) {
            return false;
        }
        $table = $this->_protect_identifiers($table);

        $return = false;

        if ($type === "RETURN") {
            $type   = 'INSERT';
            $return = true;
        }

        $sql = "{$type} INTO $table SET ?%";

        $sql = $this->prepare($sql, [$data]);
        if ($return) {
            return $sql;
        }

        return $this->query($sql);
    }

    // function sql_compile_placeholder(string $tmpl)
    // list(
    //   list(
    //     $key,
    //     $type,   '@'|'%'|'#'|''
    //     $start,
    //     $length
    //   ),
    //   $tmpl,
    //   $has_named
    // )
    /**
     * @param $tmpl
     */
    private function _prepare_compile($tmpl)
    {
        $compiled  = array();
        $p         = 0;
        $i         = 0;
        $has_named = false;
        while (false !== ($start = $p = strpos($tmpl, "?", $p))) {
            switch ($c = substr($tmpl, ++$p, 1)) {
                case '%':case '@':case '#':
                    $type = $c; ++$p;
                    break;
                default:
                    $type = '';
                    break;
            }

            if (preg_match('/^((?:[^\s[:punct:]]|_)+)/', substr($tmpl, $p), $pock)) {
                $key = $pock[1];
                if ($type != '#') {
                    $has_named = true;
                }

                $p += strlen($key);
            } else {
                $key = $i;
                if ($type != '#') {
                    $i++;
                }
            }

            $compiled[] = array($key, $type, $start, $p - $start);
        }

        return array($compiled, $tmpl, $has_named);
    }

    /**
     * acccepts a template (query) + parameters
     * placeholders can be: ?, ?@ (IN ?@ )  or ?% (k=v)
     */
    private function _prepare_exec($tmpl, $args, &$errormsg)
    {
        if (is_array($tmpl)) {
            $compiled = $tmpl;
        } else {
            $compiled = $this->_prepare_compile($tmpl);
        }

        list($compiled, $tmpl, $has_named) = $compiled;

        if ($has_named) {
            $args = @$args[0];
        }

        $p     = 0;
        $out   = '';
        $error = false;

        foreach ($compiled as $num => $e) {
            list($key, $type, $start, $length) = $e;

            // Pre-string.
            $out .= substr($tmpl, $p, $start - $p);
            $p = $start + $length;

            $repl   = '';
            $errmsg = '';
            do {
                if ($type === '#') {
                    $repl = @constant($key);
                    if (null === $repl) {
                        $error = $errmsg = "UNKNOWN_CONSTANT_$key";
                    }

                    break;
                }
                if (!isset($args[$key])) {
                    $error = $errmsg = "UNKNOWN_PLACEHOLDER_$key";
                    break;
                }
                $a = $args[$key];
                if ($type === '') {
                    if (is_array($a)) {
                        $error = $errmsg = "NOT_A_SCALAR_PLACEHOLDER_$key";
                        break;
                    }
                    if (strlen($a) < 10) {
                        $temp_my = intval($a);
                        if ($temp_my === $a) {
                            $repl = $a;
                        } else {
                            $repl = "'" . $this->escape($a) . "'";
                        }
                        //$repl = preg_match('/^\d+$/', $a)? $a : "'".mysql_real_escape_string($a)."'";
                    } else {
                        $repl = "'" . $this->escape($a) . "'";
                    }
                    break;
                }
                //     .
                if (!is_array($a)) {
                    $error = $errmsg = "NOT_AN_ARRAY_PLACEHOLDER_$key";
                    break;
                }
                if ($type === '@') {
                    foreach ($a as $v) {
                        $repl .= ($repl === '' ? "" : ",") . "'" . $this->escape($v) . "'";
                    }
                } elseif ($type === '%') {
                    $lerror = array();
                    foreach ($a as $k => $v) {
                        if (!is_string($k)) {
                            $lerror[$k] = "NOT_A_STRING_KEY_{$k}_FOR_PLACEHOLDER_$key";
                        } else {
                            $k = preg_replace('/[^a-zA-Z0-9_]/', '_', $k);
                        }
                        $repl .= ($repl === '' ? "" : ", ") . $k . "='" . @$this->escape($v) . "'";
                    }

                    if (count($lerror)) {
                        $repl = '';
                        foreach ($a as $k => $v) {
                            if (isset($lerror[$k])) {
                                $repl .= ($repl === '' ? "" : ", ") . $lerror[$k];
                            } else {
                                $k = preg_replace('/[^a-zA-Z0-9_-]/', '_', $k);
                                $repl .= ($repl === '' ? "" : ", ") . $k . "=?";
                            }
                        }
                        $error = $errmsg = $repl;
                    }
                }
            } while (false);
            if ($errmsg) {
                $compiled[$num]['error'] = $errmsg;
            }

            if (!$error) {
                $out .= $repl;
            }
        }
        $out .= substr($tmpl, $p);

        if ($error) {
            $out = '';
            $p   = 0; //
            foreach ($compiled as $num => $e) {
                list($key, $type, $start, $length) = $e;
                $out .= substr($tmpl, $p, $start - $p);
                $p = $start + $length;
                if (isset($e['error'])) {
                    $out .= $e['error'];
                } else {
                    $out .= substr($tmpl, $start, $length);
                }
            }
            $out .= substr($tmpl, $p);
            $errormsg = $out;

            return false;
        } else {
            $errormsg = false;

            return $out;
        }
    }

    /**
     * Use mysqli_real escape, and addslashes as a fallback to escape a string.
     *
     * @param  string         $string
     * @param  bool           $like
     * @return mixed|string
     */
    private function _real_escape($string, $like = false)
    {
        if (function_exists('mysqli_real_escape_string') && is_resource($this->dbh)) {
            $string = mysqli_real_escape_string($this->dbh, $string);
        } elseif (function_exists('mysqli_escape_string')) {
            $string = mysqli_escape_string($this->dbh, $string);
        } else {
            $string = addslashes($string);
        }

        return $string;
    }

    /**
     * _weak_escape
     *
     * @param  string   $string
     * @return string
     */
    private function _weak_escape($string)
    {
        return addslashes($string);
    }

    /**
     * Keep the DB connection from timing out.
     * Checks the age of the connection, will re-init/connect etc.
     *
     * @return boolean
     */
    private function check_dbh_age()
    {
        if ($this->dbh_age && !$this->dbh_passed) {
            $age_actual = time() - $this->dbh_age;
            if ($age_actual > $this->dbh_timeout) {
                if (!$this->dbh->ping()) {
                    msg("Restarting connection due to age of $age_actual.\n");
                    $this->ready = $this->db_connect();
                    if ($this->ready) {
                        $this->db_init();
                    }

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Attempt a DB connection using the connecton parameters found in class.
     *
     * @return boolean
     */
    private function db_connect()
    {
        $this->dbh = new mysqli($this->database_host, $this->database_user, $this->database_pass, $this->database_name);
        if ($this->dbh->connect_errno) {
            $this->last_error = printf("Connect failed: %s\n", $this->dbh->connect_error);
            $this->print_error();

            return false;
        } else {
            $this->dbh_age = time();

            return true;
        }
    }

    // --------------------------------------------------------------------

    /**
     * Do basic DB initialization such as setting the charset, selecting db.
     */
    private function db_init()
    {
        if ($this->ready && !$this->dbh_passed) {
            $this->set_charset($this->dbh);
            $this->db_select($this->database_name, $this->dbh);
        }
    }

    /**
     * The database version number.
     *
     * @param  null|\mysqli $dbh
     * @return false|string false on failure, version number on success
     */
    private function db_version($dbh = null)
    {
        if ($dbh == null) {
            $dbh = &$this->dbh;
        }

        return preg_replace('/[^0-9.].*/', '', $dbh->server_version);
    }

    /**
     * Fire off a fatal error and end script execution.
     *
     * @param string|null $msg
     */
    private function fatal($msg = null)
    {
        if ($msg != null) {
            $this->last_error = $msg;
        }

        $this->print_error();
        die;
    }

    /**
     * Determine if a database supports a particular feature
     *
     * @param  string       $db_cap the feature
     * @param  null|\mysqli $dbh
     * @return bool
     */
    private function has_cap($db_cap, $dbh = null)
    {
        $version = $this->db_version($dbh);
        switch (strtolower($db_cap)) {
            case 'collation':    // @since 2.5.0
            case 'group_concat': // @since 2.7
            case 'subqueries':   // @since 2.7
                return version_compare($version, '4.1', '>=');
            case 'set_charset':
                return version_compare($version, '5.0.7', '>=');
        };

        return false;
    }

    /**
     * Print the most recent error message.
     *
     * @param  null|string $msg
     * @return void
     */
    private function print_error($msg = null)
    {
        $tag = ' ';
        if ($msg != null) {
            $this->last_error = $msg;
        }

        if ($this->database_host != '' && $this->database_name != '') {
            $tag = '[' . $this->database_name . '@' . $this->database_host . '] ';
        }

        $error = "MadDB Error:" . $tag . "[$this->last_error]";
        if (!empty($this->last_query)) {
            $error .= " [$this->last_query]";
        }

        if (function_exists('error_log') && ($log_file = @ini_get('error_log')) && ('syslog' == $log_file || @is_writable($log_file))) {
            @error_log($error . "\n");
        }

        if (!$this->show_errors) {
            return;
        }

        if (!empty($this->error_handler) && function_exists($this->error_handler)) {
            call_user_func($this->error_handler, $error);
        } elseif (empty($_SERVER['DOCUMENT_ROOT'])) {
            echo ($error . NL);
        } else {
            echo (htmlspecialchars($error) . NL);
        }
    }

    /**
     * @return mixed
     */
    private function timer()
    {
        if (isset($this->timers[$this->query_id])) {
            return $this->timers[$this->query_id];
        }

        return $this->timers[$this->query_id] = new Timer;
    }

    /**
     * @param $name
     */
    private function timer_log()
    {
        // get all the timers:
        $timers = $this->timer()->all();

        if ($this->save_queries || (defined('SAVE_QUERIES') && SAVE_QUERIES)) {
            $full = $timers['full'];
            // unset($timers['full']);

            $this->queries[$this->query_id] = [
                'query'      => $this->last_query,
                'total_time' => $full['elapsed'],
                'timers'     => array_column($timers, 'elapsed', 'name'),
            ];
        }
        unset($this->timers[$this->query_id]);
        $this->query_id++;
    }
}