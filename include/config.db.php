<?php
defined('DB_H') OR define('DB_H','db.madthumbs.com');
defined('DB_U') OR define('DB_U','kvs');
defined('DB_P') OR define('DB_P','nFm2rBoGVBED');
defined('DB_D') OR define('DB_D','madthumbs');

class MTDB extends MadDB {

    function __construct( $params = array() )
    {
        $p = array(
            'database_user' => DB_U,
            'database_pass' => DB_P,
            'database_host' => DB_H,
            'database_name' => 'madthumbs_test'
        );

        $p = array_merge( $p, $params );

        return parent::__construct( $p );
    }
}

class KVSDB extends MadDB {

    function __construct( $params = array() )
    {
        $p = array(
            'database_user' => DB_U,
            'database_pass' => DB_P,
            'database_host' => DB_H,
            'database_name' => 'madthumbs'
        );

        $p = array_merge( $p, $params );

        return parent::__construct( $p );
    }
}
