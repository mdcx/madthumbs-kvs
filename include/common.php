<?php
defined('NL') or define('NL', "\n");
defined('KVS_ROOT') or define('KVS_ROOT', '/home/web1/madthumbs.com/htdocs/');
defined('CUSTOM_ROOT') or define('CUSTOM_ROOT', KVS_ROOT . 'custom/');

$default_load = array(
    'general'       => 'helper',
    'Args'          => 'class',
    'db'            => 'helper',
    'MadDB'         => 'class',
    'Timer'         => 'class',
    'config.db.php' => 'config',
    'url'           => 'helper',
);

foreach ($default_load as $load_file => $load_type) {
    load_file($load_file, $load_type);
}

$args = new Args();

/**
 * @param $which
 * @param $path
 * @param null     $force
 */
function &get_loaded()
{
    /**
     * @var mixed
     */
    static $_loaded;
    if (empty($_loaded)) {
        $_loaded = array();
    }

    return $_loaded;
}

/**
 * load a file into the global scope
 *
 * @param string $which the name of the file
 * @param string $what  what are we loading
 * @param string $path  rel path to included file
 * @param bool   $force require/include 'once' or not
 */
function load_file($which, $what = 'helper', $path = null, $force = false)
{
    $default_path            = CUSTOM_ROOT . '/include/';
    !null === $path or $path = $default_path;

    $_loaded  = &get_loaded();
    $filename = strpos($which, '.') === false ? "{$what}.{$which}.php" : $which;
    if ($which[0] === '/') {
        $path     = dirname($which) . '/';
        $filename = basename($filename);
    }

    $fullpath = $path . $filename;

    if ((empty($_loaded[$fullpath]) || $force) && file_exists($fullpath)) {
        if ($force) {
            include $fullpath;
        } else {
            require_once $fullpath;
        }

        $_loaded[$filename] = time();
    }

    return isset($_loaded[$filename]) ? $_loaded[$filename] : false;
}

/**
 * load a helper
 *
 * @param string $which the name of the file
 * @param string $path  rel path to included file
 * @param bool   $force require/include 'once' or not
 */
function load_helper($which, $path = null, $force = false)
{
    return load_file($which, 'helper', $path, $force);
}

/**
 * load a class
 *
 * @param string $which the name of the file
 * @param string $path  rel path to included file
 * @param bool   $force require/include 'once' or not
 */
function load_class($which, $path = null, $force = false)
{
    return load_file($which, 'class', $path, $force);
}

if (!function_exists('msg')) {
    /**
     * @param $label
     * @param $msg
     * @param $dump
     */
    function msg($label, $msg = '', $dump = false)
    {

        $labels = ['debug', 'error', 'info', 'warn', 'notice'];
        $gc     = get_caller();
        if (!is_string($label) || !in_array(strtolower($label), $labels)) {
            $args = func_get_args();
            if (count($args) == 2) {
                if (is_string($args[1])) {
                    $gc  = $args[0];
                    $msg = $args[1];
                } else {
                    $msg  = $args[0];
                    $dump = $args[1];
                }
            } else {
                $msg = $args[0];
            }
            $label = 'info';
        }

        $label = strtoupper($label);

        $out = sprintf("[%s] %-6s[%s]", date('Y-m-d H:i:s'), $label, $gc);

        if (is_string($msg)) {
            $out .= " $msg";
        } elseif (is_array($msg) || is_object($msg)) {
            $dump = $msg;
            $msg  = "dump";
        }

        if ($dump !== false) {

            $c    = 0;
            $dump = print_r($dump, true);
            $dump = str_replace(['Array', 'stdClass Object'], ['%%%A%%%', '%%%O%%%'], $dump);
            while (strpos($dump, '%%%') !== false && $c++ <= 3) {
                $dump = preg_replace("/%%%(A|O)%%%\s+\((\s*?.*?\s+)\)/ism", "$1 [$2]", $dump);
                // print "$c: $dump";
            }
            $dump = explode("\n", $dump);
            array_walk($dump, function (&$item, $k) {$item = "    {$item}";});

            $dump = implode("\n", $dump);
            $out .= NL . "  " . trim($dump) . NL . "";
        }
        if (function_exists('log_output')) {
            log_output($out);
        } else {
            print $out . NL;
        }

    }
}
