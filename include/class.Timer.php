<?php


class Timer
{
    /**
     * List of all timers.
     *
     * @var array
     */
    protected $timers = [];

    /**
     * Add a timer if it doesn't exist.
     *
     * @param  string $name The name of this timer.
     * @return bool   true if it was added, false if it existed.
     */
    public function add($name)
    {
        if (false === $this->has($name)) {
            $this->start($name);
            return true;
        }

        return false;
    }

    /**
     * Returns the array of timers, with the duration pre-calculated for you.
     *
     * @param  int     $decimals Number of decimal places
     * @return array
     */
    public function all($decimals = 4)
    {
        foreach (array_keys($this->timers) as $name) {
            $this->elapsed($name, $decimals);
        }

        return $this->timers;
    }

    /**
     * Returns a float representing the number of seconds elapsed while that timer was running.
     *
     * @param  string     $name     The name of the timer.
     * @param  int        $decimals Number of decimal places.
     * @return null|float Returns null if no timer exists by that name.
     */
    public function elapsed($name, $decimals = 4)
    {
        $name = strtolower($name);
        if (empty($this->timers[$name])) {
            return null;
        }

        if (empty($this->timers[$name]['end'])) {
            $this->timers[$name]['end'] = microtime(true);
        }

        if (empty($this->timers[$name]['elapsed'])) {
            $this->timers[$name]['elapsed'] = (float) number_format($this->timers[$name]['end'] - $this->timers[$name]['start'], $decimals);
        }

        return $this->timers[$name]['elapsed'];
    }

    /**
     * @param  $name
     * @return mixed
     */
    public function get($name)
    {
        if (isset($this->timers[$name])) {
            return $this->timers[$name];
        }

        return false;
    }

    /**
     * Checks whether or not a timer with the specified name exists.
     *
     * @param  string $name
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists(strtolower($name), $this->timers);
    }

    //--------------------------------------------------------------------

    /**
     * Starts a timer running.
     *
     * @param  string  $name The name of this timer.
     * @param  float   $time Allows user to provide time.
     * @return Timer
     */
    public function start($name, $time = null)
    {
        $this->timers[strtolower($name)] = [
            'name'    => $name,
            'start'   => !empty($time) ? $time : microtime(true),
            'end'     => null,
            'elapsed' => null,
        ];

        return $this;
    }

    /**
     * Stops a running timer.
     *
     * If the timer is not stopped before the timers() method is called,
     * it will be automatically stopped at that point.
     *
     * @param  string  $name The name of this timer.
     * @return Timer
     */
    public function stop($name)
    {
        $name = strtolower($name);
        if (empty($this->timers[$name])) {
            $this->timers[$name] = ['name'=>$name,'start'=>0,'end'=>0, 'duration'=>0];
        } else {
            $this->timers[$name]['end'] = microtime(true);
        }

        return $this;
    }
}
