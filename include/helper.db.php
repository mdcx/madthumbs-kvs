<?php
function _escpq($str, $link=null) {
    return "'" . do_query("ESCAPE $str", $link) . "'";
}
function _escp($str, $link) {
    return do_query("ESCAPE $str", $link);
}

/**
 * Execute a MySQL Query. Very basic wrapper.
 *
 * @param   query       The query you want to run
 * @param   link        A MySQL Link resource pointer.
 * @param   index       If you want the results indexed on a field, name it here.
 * @param   error_cmd   Do you want error messages echo'd or do you want an exit.
 * @param   fetch_type  array, row, assoc
 */
function do_query($query, $link = null, $index = false, $error_cmd = "exit", $fetch_type = "obj")
{
    GLOBAL $query_count, $last_query;
    static $last_used;

    if ( is_array($query) ) {
        if ( isset($query['query']) )
            extract($query);
    }

    /**
     * Since we will be working with databases on different hosts we will
     * need to make sure we are always working with the correct database.
     * The statement below is for queries that do not provide a connection
     * resource to use, therefore it must be the default db.
     */
    if ( empty($link) || $link === null ) {

        $link = dbh_get();
    }

    if ( !dbh_run('ping', $link) )
        trigger_error("Connection failed.", E_USER_ERROR);

    // query printer
    $cmd = strtolower(substr($query, 0, 6));
    if ($cmd != 'escape' && defined('QUERY_PRINT') && QUERY_PRINT == true) {
        $t = date("h:i:s", time());
        echo("[ $t ] $query\n");
    }

    switch ($cmd) {
        CASE 'escape':
            $str = str_replace("ESCAPE ", "", $query);
            // $str = mysql_real_escape_string($str, $link);
            return dbh_run('real_escape_string', $link, $str);
            break;
        CASE 'descri':
        CASE 'show t':
        CASE 'select':
            $result = dbh_run('query', $link, $query);
            $ret = false;
            if (!$result) {
                mysql_error_msg("Error: " . dbh_run('error', $link), $query);
                return false;
            }

            if ( ($num_rows = dbh_run('num_rows',$link,$result)) > 0) {
                $ret = array();
                if ( $fetch_type === 'obj' ) $fetch_type = 'object';
                while ( $row = dbh_run('fetch_'.$fetch_type,$link,$result) ) {
                    if ( $index ) $ret[$fetch_type === 'object' ? $row->$index : $row[$index]] = $row;
                    else $ret[] = $row;
                }
            }
            return $ret;
            break;
        CASE 'error ':
            return dbh_run('error',$link);
            break;
        CASE 'replac':
        CASE 'insert':
            $result = dbh_run('query', $link, $query);
            if (!$result) {
                mysql_error_msg("Error: " . dbh_run('error', $link), $query);
                return false;
            }
            return dbh_run('insert_id',$link);
        break;
        CASE 'update':
        CASE 'delete':
        CASE 'trunca':
            $result = dbh_run('query', $link, $query);
            if (!$result) {
                mysql_error_msg("Error: " . dbh_run('error', $link), $query);
                return false;
            }
            return dbh_run('affected_rows', $link);
        break;
        default:
            $result = dbh_run('query', $link, $query);
            if (!$result) {
                mysql_error_msg("Error: " . dbh_run('error', $link), $query);
                return false;
            }
            return $result;
    }
}


function dbh_get($which='default', $type='mysqli') {
    static $connections;

    if ( $which === 'all' ) {
        return $connections;
    }

    if ( $which === 'default' )
        $which = 'kvs';


    $after_queries = [
       "SET NAMES utf8",
       "SET SESSION sql_mode = 'NO_ENGINE_SUBSTITUTION'",
       "SET SESSION SQL_BIG_SELECTS = 1",
       "SET SESSION wait_timeout = 3600",
    ];

    $credentials = [
        'kvs' => [DB_H,DB_U,DB_P,'madthumbs'],
        'old_mt' => [DB_H,DB_U,DB_P,'madthumbs_test']
    ];

    if ( empty($credentials[$which]) )
        trigger_error('Invalid DB selection ...',E_USER_ERROR);

    // setup new conn
    $cname = $which .'_'. $type;
    if ( empty($connections[$cname]) || !dbh_run('ping', $connections[$cname]) ) {

        // setup mysqli connection
        if ( $type == 'mysqli') {
            $connections[$cname] = @mysqli_connect( $credentials[$which][0],$credentials[$which][1], $credentials[$which][2], $credentials[$which][3] ) or
                trigger_error( 'Could not connect to MySQLi' . mysqli_connect_error() . "\n", E_USER_ERROR );
        }
        // setup MySQL connection
        elseif ( $type == 'mysql' ) {
            $connections[$cname] =  mysql_connect($credentials[$which][0],$credentials[$which][1], $credentials[$which][2]) or
                trigger_error( 'Could not connect to db.', E_USER_ERROR );
            if( !is_resource($connections[$cname]) || !mysql_select_db( $credentials[$which][3], $connections[$cname] ) )
                trigger_error( 'Could not select db', E_USER_ERROR );
        }
        dbh_run('set_charset', $connections[$cname], 'utf8');
        foreach($after_queries as $query)
            dbh_run('query', $connections[$cname], $query);
    }
    return $connections[$cname];
}

function dbh_run($func, $link, $query = null ) {
    $skip_link = ['mysql_num_rows', 'mysqli_num_rows', 'mysql_result', 'mysqli_result'];
    $skip_query = [];
    if ( strpos($func, 'mysql') === false )
    {
        if ( is_a($link, 'mysqli') || is_a($query, 'mysqli_result') )
            $func = 'mysqli_'.$func;
        elseif ( is_resource($link) && get_resource_type($link) == 'mysql link' )
            $func = 'mysql_'.$func;
    }

    if ( $query === null  || in_array($func, $skip_query)) {
        return $func($link);
    }
    elseif ( in_array($func, $skip_link) || strpos($func, 'fetch_') !== false ) {
        return  $func($query);
    }
    if ( strpos($func, 'mysqli') !== false )
        return $func($link, $query);
    return $func($query, $link);
}


function get_row(&$ret, $row, $index = false, $fetch_type = '')
{

}


/**
 * grab_value()
 * Grab a single database value and return it
 *
 * @param query THe query to select from. If you send more than 1 field it will only return the first field.
 * @param link  A resource
 * @return mixed
 */
function grab_value($query, $link = null)
{
    $get = do_query($query, $link, false, "exit", 'array');
    if ($get === false) {
        return false;
    }

    return $get[0][0];
}

/**
 * grab_row()
 * Grab a single database row and return it
 *
 * @param query THe query to select from. If you send more than 1 field it will only return the first field.
 * @param link  A resource
 * @return mixed
 */
function grab_row($query, $link = null, $row = 0, $fetch_type = 'obj')
{
    if (strpos($query, ' LIMIT ') === false) {
        $query .= ' LIMIT 1';
    }
    $index = false;
    if ( is_array($row) )
        list($index,$row) = $row;

    $get = do_query($query, $link, $index, 'exit', $fetch_type);
    if ($get && isset($get[$row]) ) return $get[$row];

    return false;
}

function mysql_error_msg($msg, $query = '')
{
    print "MYSQL ERROR: $msg\n";
    if ( $query != '' ) print "QUERY: $query\n\n";
}

