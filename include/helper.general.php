<?php
/**
 * Manage getting advisory locks on a specific file.
 *
 *                               If 'task-9', it uses it to generate a lock file path
 *                               If 'videos/task-9', it will put the file in a folder
 *                               If /videos/task-9' it uses the value as an abs path
 * @param  bool|string     $lock_id If false, unlocks all known locks created by the function.
 * @param  int             $wait    If lock fails, wait this many seconds and try again.
 * @param  bool            $debug   If true, echo some debug messages.
 * @return bool|resource
 */
function lock_manager($lock_id = false, $wait = 0, $debug = false)
{
    global $config;
    /**
     * @var mixed
     */
    static $_locks;
    if (empty($_locks)) {
        $_locks = [];
    }

    if ($lock_id === false) {
        foreach ($_locks as $lock_id => $lock) {
            lock_manager($lock_id, 0, $debug);
        }

        return true;
    }

    $lock_dir = "$config[project_path]/admin/data/system/locks/";
    if (strpos($lock_id, '/') !== false) {
        $parts   = explode('/', $lock_id);
        $lock_id = array_pop($parts);
        $lock_dir .= implode('/', $parts) . '/';
    }
    $lock_file = $lock_dir . rtrim($lock_id, '.lock') . '.lock';

    // if lock exists, let's unlock it.
    if (isset($_locks[$lock_id])) {
        if (is_resource($_locks[$lock_id])) {
            $unlock = flock($_locks[$lock_id], LOCK_UN);
            if (!$unlock) {
                if ($debug) {
                    log_output("[locker] Failed to unlock lock named $lock_id...");
                }

                return false;
            }
            fclose($_locks[$lock_id]);
            unlink($lock_file);
            unset($_locks[$lock_id]);
            if ($debug) {
                log_output("[locker] Unlocked lock for $lock_id ...");
            }

            return true;
        }
        if ($debug) {
            log_output("[locker] $lock_id doesn't have a valid file pointer.");
        }

        return false;
    }
    // attempt to create new lock
    else {
        $fp = fopen($lock_file, 'c');
        if (!$fp) {
            if ($debug) {
                log_output("[locker] Failed to open/create lock file: $lock_file");
            }

            return false;
        }

        if (!($lock = flock($fp, LOCK_EX | LOCK_NB))) {
            if ($debug) {
                log_output("[locker] Failed to obtain lock: $lock_file");
            }

            return false;
        } else {
            if ($debug) {
                log_output("[locker] Obtained lock for $lock_file");
            }

            fwrite($fp, getmypid());

            return $_locks[$lock_id] = $fp;
        }
    }

    return false;
}

/**
 * Convert seconds to a DD:HH:MM:SS timestamp (array format);
 * @param  int   $int
 * @return array Array of days, hours, mins, secs
 */
function secs_to_dhms($int)
{
    $format = '%h:%m:%s';

    if ($int <= 0) {
        return false;
    }

    $days   = (($int / 60) / 60) / 24;
    $days_r = $days - floor($days);
    $days   = $days - $days_r;

    $hours   = $days_r * 24;
    $hours_r = $hours - floor($hours);
    $hours   = $hours - $hours_r;

    $mins   = $hours_r * 60;
    $mins_r = $mins - floor($mins);
    $mins   = $mins - $mins_r;

    $secs = $mins_r * 60;

    $days  = sprintf('%02d', $days);
    $hours = sprintf('%02d', $hours);
    $mins  = sprintf('%02d', $mins);
    $secs  = sprintf('%02d', $secs);

    $p    = $r    = array();
    $p[0] = '/%d/';
    $p[1] = '/%h/';
    $p[2] = '/%m/';
    $p[3] = '/%s/';

    $r[0] = $days;
    $r[1] = $hours;
    $r[2] = $mins;
    $r[3] = $secs;

    $dhms = preg_replace($p, $r, $format);

    return compact('dhms', 'days', 'hours', 'mins', 'secs');
}


function hms_to_secs($time) {
    sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
    $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
    return $time_seconds;
}


/**
 * @param $var
 */
function vdd($var)
{

    $gc = get_caller();
    msg('var dump', get_caller());
    var_dump($var);
    die;
}

/**
 * @param $var
 */
function vd($var)
{
    $gc = get_caller();
    msg("var_dump", $gc);
    var_dump($var);
}

/**
 * Is CLI?
 *
 * Test to see if a request was made from the command line.
 *
 * @return bool
 */
function is_cli()
{
    return (PHP_SAPI === 'cli' || defined('STDIN'));
}
/**
 * Get Caller
 * Find out which function or functions called the func/include/class you are
 * currently in.
 *
 * @param  type   $filter Filter out calls containing $filter
 * @param  type   $short  Only return the closest func to the current.
 * @return type
 */
function get_caller($filter = false, $short = true)
{

    $default_filters = ['require_once', 'include_once', 'require', 'include', 'get_caller', 'msg', 'vd', 'vdd'];
    if (!$filter || $filter == '') {
        $filter = [];
    }
    if (!is_array($filter)) {
        $filter = [$filter];
    }
    $filter = array_merge($default_filters, $filter);
    $trace  = array_reverse(debug_backtrace());
    $caller = [];

    foreach ($trace as $call) {
        $debug_str = isset($call['class']) ? "{$call['class']}->{$call['function']}" : $call['function'];

        if (isset($call['line'])) {
            $debug_str .= ":{$call['line']}";
        }

        if (strpos_array($debug_str, $filter) !== false) {
            continue;
        }
        $caller[] = $debug_str;
    }
    if (count($caller) === 0) {
        $inc      = get_included_files();
        $main     = array_shift($inc);
        $main_d   = explode('/', dirname($main));
        $main_f   = basename($main);
        $caller[] = array_pop($main_d) . '/' . $main_f;
    }
    if ($short) {
        if ($short === true) {
            return array_pop($caller);
        } else {
            $caller = array_slice($caller, -$short);
        }
    }

    return join(', ', $caller);
}

/**
 * Converts tabs to the appropriate amount of spaces while preserving formatting
 *
 * @version     1.2.0
 * @author      Aidan Lister <aidan@php.net>
 *
 * @link        http://aidanlister.com/2004/04/handling-tab-to-space-conversions/
 *
 * @param  string $text   The text to convert
 * @param  int    $spaces Number of spaces per tab column
 * @return string The text with tabs replaced
 */
function tab2space($text, $spaces = 4)
{
    // Explode the text into an array of single lines
    $lines = explode("\n", $text);

    // Loop through each line
    foreach ($lines as $line) {

        // Break out of the loop when there are no more tabs to replace
        while (false !== $tab_pos = strpos($line, "\t")) {

            // Break the string apart, insert spaces then concatenate
            $start = substr($line, 0, $tab_pos);
            $tab   = str_repeat(' ', $spaces - $tab_pos % $spaces);
            $end   = substr($line, $tab_pos + 1);
            $line  = $start . $tab . $end;
        }

        $result[] = $line;
    }

    return implode("\n", $result);
}

/**
 * Convert a size string to bytes.
 *
 * @param  mixed $size
 * @return int
 */
function size_to_bytes($size, $si = false)
{
    return rbytes($size, $si);
}

/**
 * Convert bytes to a specific factor.
 *
 * @param  int   $bytes
 * @param  mixed $force_unit
 * @return array [$num, $suffix]
 */
function bytes_to_size($bytes, $force_unit = null)
{
    return explode(' ', bytes($bytes, $force_unit));
}

/**
 * Takes a human readable size and converts it to bytes.
 *
 * @see  Aidan Lister: http://aidanlister.com/repos/v/function.size_readable.php
 *
 * @param  string    size Accepts: 20M, 20MiB, 20MB as formats.
 * @param  boolean   $si  whether to use SI prefixes or IEC
 * @return integer
 */
function rbytes($size, $si = true)
{
    // IEC prefixes (binary)
    if ($si == false || strpos($size, 'i') !== false) {
        $units = ['B', 'Ki?B?', 'Mi?B?', 'Gi?B?', 'Ti?B?', 'Pi?B?'];
        $mod   = 1024;
    }
    // SI prefixes (decimal)
    else {
        $units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
        $mod   = 1000;
    }

    for ($i = count($units) - 1; $i >= 0; $i--) {
        $m = preg_match('/(' . $units[$i] . ')/i', $size, $match);
        if ($m) {
            $size_raw = trim(preg_replace('/' . $units[$i] . '/i', '', $size));

            return $size_raw * pow($mod, $i > 0 ? $i : 1);
        }
    }
    echo get_caller();
    die("Not well formed size: '$size'\n");
}

/**
 * Returns human readable sizes.
 * @see  Based on original functions written by:
 * @see  Aidan Lister: http://aidanlister.com/repos/v/function.size_readable.php
 * @see  Quentin Zervaas: http://www.phpriot.com/d/code/strings/filesize-format/
 *
 * @param  integer  size    in bytes
 * @param  string   a       definitive unit
 * @param  string   the     return string format
 * @param  boolean  whether to use SI prefixes or IEC
 * @return string
 */
function bytes($bytes, $force_unit = null, $format = '%01.2f %s', $si = true)
{
    $force_unit = (string) $force_unit;
    $units      = [
        'bin' => ['mod' => 1024, 'suffixes' => ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB']],
        'si'  => ['mod' => 1000, 'suffixes' => ['B', 'kB', 'MB', 'GB', 'TB', 'PB']],
    ];

    extract($si === false || strpos($force_unit, 'i') !== false ? $units['bin'] : $units['si']);

    // Determine unit to use
    if (($power = array_search($force_unit, $suffixes)) === false) {
        $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
    }

    return sprintf($format, $bytes / pow($mod, $power), $suffixes[$power]);
}

function getmicrotime()
{
    list($usec, $sec) = explode(' ', microtime());

    return (float) $usec + (float) $sec;
}

/**
 * Remnove CDATA from xml element.
 *
 * @param  type   $string
 * @return type
 */
function strip_cdata($string)
{
    preg_match_all('/<!\[cdata\[(.*?)\]\]>/is', $string, $matches);

    return str_replace($matches[0], $matches[1], $string);
}
/**
 * Pretty Date Diff
 * Takes the result of DATE1 - DATE2 and turns it into a human readble string
 * or array. For example: $ts = 453534 would be 1 week, 3 days, 2 mins (or something)
 *
 * @param  int   $seconds The numnber of seconds to diff
 * @param  bool  $str     Return it as a pretty textual string?
 * @return array Array containing d/h/m/s
 */
function pretty_diff($seconds, $str = true, $precision = 'all')
{
    if (is_numeric($seconds)) {
        $seconds = (int) $seconds;
    } else {
        $seconds = strtotime($seconds);
        $seconds = time() - $seconds;
    }

    $rem = $seconds;
    $ret = [
        'y' => 0,
        'w' => 0,
        'd' => 0,
        'h' => 0,
        'm' => 0,
        's' => 0,
    ];

    $count = 0;
    $rstr  = '';

    $str_part = [];
    while ($rem > 0) {
        switch ($rem) {
            // seconds
            case $rem < 60:
                $ret['s']  = $rem;
                $ret['s']  = round($ret['s'], 2);
                $rem       = 0;
                $this_part = $ret['s'] . " second";
                if ($ret['s'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
                break;
            // minutes
            case $rem < 3600:
                $tmp       = $rem / 60; // 1.65
                $ret['m']  = floor($tmp);
                $rem       = $rem - ($ret['m'] * 60);
                $this_part = $ret['m'] . " minute";
                if ($ret['m'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
                break;
            // this means it's less than days etc.
            case $rem < 86400:
                $tmp       = $rem / 3600;
                $ret['h']  = floor($tmp);
                $rem       = $rem - ($ret['h'] * 3600);
                $this_part = $ret['h'] . " hour";
                if ($ret['h'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
                break;
            // days
            case $rem < 604800:
                $ret['d']  = floor($rem / 86400);
                $rem       = $rem - ($ret['d'] * 86400);
                $this_part = $ret['d'] . " day";
                if ($ret['d'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;

                break;
            // weeks
            case $rem < 31449600:
                $ret['w']  = floor($rem / 604800);
                $rem       = $rem - ($ret['w'] * 604800);
                $this_part = $ret['w'] . " week";
                if ($ret['w'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
            // no break
            default:
                $ret['y'] = floor($rem / (52 * 604800));
                $rem -= $ret['y'] * (52 * 604800);
                $this_part = $ret['y'] . ' year';
                if ($ret['y'] > 1) {
                    $this_part .= 's';
                }

                $str_part[] = $this_part;
        }
        $count++;
        if ($count > 10) {
            break;
        }
    }

    // 5 days 3 hours 10 mintes 5 sec
    if ($str != false) {
        if ($precision != 'all' && is_numeric($precision)) {
            $precision = max([$precision, count($str_part)]);
            $str_part  = array_slice($str_part, 0, $precision);
        }

        return implode(' ', $str_part);
    } else {
        return $ret;
    }
}
/**
 * Slash String
 * Return the $str with a right slash added if none existed.
 *
 * @param  type   $str
 * @return type
 */
function slashr($str)
{
    return rtrim($str, '/') . '/';
}
/**
 * Return str with leading slash if none existed.
 *
 * @param  string   $str
 * @return string
 */
function slashl($str)
{
    return '/' . ltrim($str, '/');
}
/**
 * Return str with leading/trailing slash if none existed.
 *
 * @param  string   $str
 * @return string
 */
function slashrl($str)
{
    return '/' . trim($str, '/') . '/';
}

/**
 * Checks if a process is currently running. used to prevent more than 1
 * copy of a script from starting.
 *
 * @param  type            $procn
 * @return boolean|array
 */
function process_running($procn)
{
    $out       = [];
    $pid       = getmypid();
    $check_cmd = 'ps auxwww | egrep ' . escapeshellarg($procn) . ' | grep -v "' . $pid . '" | grep -v "grep" | grep -v "sh"';

    exec($check_cmd, $out);

    if (count($out) > 0) {
        return true;
    }

    return false;
}
/**
 * Check a string for an array of values ..
 *
 * @param  string  $haystack The string to search
 * @param  array   $needles  An array of values to match
 * @return boolean if a match was made
 */
function strpos_array($haystack, $needles)
{

    if (is_array($needles)) {
        foreach ($needles as $needle) {
            $pos = is_array($needle) ? strpos_array($haystack, $needle) : strpos($haystack, $needle);
            if ($pos !== false) {
                return $pos;
            }
        }

        return false;
    }

    return strpos($haystack, $needles);
}
/**
 * Sort an associative array on a specified key.
 *
 * @param  array     $array The array to sort
 * @param  string    $on    The field to sort on
 * @param  php_order $order Default SORT_ASC
 * @return array     the array sorted on $on, while maintaining key assocations
 */
function array_sort($array, $on, $order = SORT_ASC)
{
    $new_array      = [];
    $sortable_array = [];

    $find_value = function ($obj, $property) {
        if (false !== strpos($property, '->')) {
            $parts = explode('->', $property);
            foreach ($parts as $pn) {
                if (isset($obj->$pn)) {
                    $obj = $obj->$pn;
                } else {
                    die("Value $on not found on object." . NL);
                }
            }
        }

        return $obj;
    };

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } elseif (is_object($v)) {
                $sortable_array[$k] = $find_value($v, $on);
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);

                break;
            case SORT_DESC:
                arsort($sortable_array);

                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

/**
 * write_flock()
 * Take's the danger out of the stranger.
 *
 * @param  mixed  $path
 * @param  mixed  $what
 * @param  string $mode
 * @param  mixed  $chmod
 * @return bool
 */
function write_flock($path, $what, $mode = 'w+', $chmod = 0777)
{
    $fp = fopen($path, $mode);
    if ($fp) {
        if (flock($fp, LOCK_EX)) {
            fwrite($fp, $what);
            chmod($path, 0777);
            flock($fp, LOCK_UN);
            fclose($fp);

            return true;
        }
    }

    return false;
}
