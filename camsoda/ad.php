<?php
include 'helper.camsoda.php';
if (isset($_GET['sample'])) {
    $ads = Helper_Camsoda::ad_list();
    foreach(array_keys($ads) as $size) {
        $tmp = $ads[$size];
        shuffle($tmp);
        $ad = array_pop($tmp);
        print "<div><h2>{$ad['size']}</h2>" . Helper_Camsoda::format_ad(Helper_Camsoda::affiliate_link(), $ad['url'], $ad['height'], $ad['width']) . "</div>";
    }
} elseif ( isset($_GET['test']) ) {
    $tmp = session_start();
    print '<pre>';
    print_r($_SESSION['ads_shown']);
    print '</pre>';
    for($i=0; $i<$_GET['num']; $i++) {
       $ad = Helper_Camsoda::image_ad(isset($_GET['size']) ? $_GET['size'] : '300x250');
       print "<div>$ad</div>\n\n";
    }
    print '<pre>';
    print_r($_SESSION['ads_shown']);
    print '</pre>';

} else {
    $ad = Helper_Camsoda::image_ad(isset($_GET['size']) ? $_GET['size'] : '300x250');
    if ($ad) {
        print $ad;
    }
}
