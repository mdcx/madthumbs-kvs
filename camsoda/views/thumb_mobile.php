<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>CamSoda: Watch live sex shows for free!</title>
		<style type="text/css">
		html, body { 
			margin: 0; 
			padding: 0; 
			background-color: <?=$background_color?>;
			color: <?=$text_color?>;	
			font-family: Arial, Helvetica, Geneva, Verdana, sans-serif;
			font-size: 100%;
			font-style: normal;			
		}
		a {
			color: <?=$link_color?>;
			text-decoration: none;
		}
		.model_name {
			font-size: 14px;
			font-weight: bold;
		}
		.live_now {
			color: <?=$text_color;?>;
			font-size: 12px;
		}
		
		.thumbTable {
			margin, padding: 0px;
		}
		.thumbTable td, tr {
			margin: 0px;
			padding: 0px;
		}
		</style>
	</head>
	<body>
		<div style="margin-left: 0px;">	
			<?php
		
				if ( isSet($thumbs ) )
				{		
					print "\t\t";
					print '<table class="thumbTable">';
					print "\n";
					$ct = 0;
					for($row=0; $row<$num_rows; $row++)
					{
						print "\t\t\t<tr>\n";
						for($col=0; $col < $num_cols; $col++)
						{
							if ( isSet($thumbs[$ct]) )
							{
								$current = $thumbs[$ct];
								$thumb_url = strpos($current->thumb,'http') === false ? 'http://' . $current->thumb : $current->thumb;
								$target = $open_new_window ? ' target="_blank"' : '';
								$img_style = "height: $thumb_height; width: $thumb_width; border: 1px solid #333;";
							?>
							<td>
								<div style="margin-right: 8px; margin-left: 0px;">
									<a href="<?=$current->link?>"<?=$target?>><img src="<?=$thumb_url?>" style="<?=$img_style?>"><br/>
									<span class="model_name"><?=$current->name?></span><br/>
									<span class="live_now"><?=ucfirst(strtolower(substr($current->subject,0,18)))?>...</span></a>
								</div>
							</td>
							<?php						
								$ct++;
							}
						}
					}
				}				
			?>
		</div>
	</body>
</html>