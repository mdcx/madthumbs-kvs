<?php

/**
 * @project BigTits
 * @organization mdcx.io
 * @created May 25, 2018
 * @author  Mike Campbell <mike@mdcx.io>
 */
require_once '../include/common.php';
load_file('curl', 'helper');
class Helper_Camsoda
{
    const CACHE_EXPIRE = 600;

    /**
     * @var string
     */
    private static $CACHE_DIR = '';

    /**
     * @var array
     */
    private static $_iframe_config = array(
        'iframe_url' => '/custom/camsoda/index.php?a=1&cmp=bt_liverow&',
        'height'     => '410px',
        'width'      => '860px',
    );

    /**
     * @var mixed
     */
    private static $_init = false;

    /**
     * @var mixed
     */
    private static $_json = false;

    /**
     * @var mixed
     */
    private static $_run_on_create = true;

    /**
     * @var array
     */
    private static $_thumb_config = array(
        'affiliate_id'     => 'sexshows',
        'thumb_height'     => 'auto',
        'thumb_width'      => '100%',
        'text_color'       => '#CCC',
        'link_color'       => 'white',
        'background_color' => '#000',
        'num_cols'         => 5,
        'num_rows'         => 2,
        'table_or_css'     => 'css',
        'videobox_width'   => '168px',
        'videobox_height'  => '180px',
        'open_new_window'  => true,
        'no_header'        => 0,
        'offset'           => 0,
    );

    /**
     * @param array $params
     */
    public function __construct($params = array())
    {
        self::init($params);
    }

    /**
     * @param $affiliate_id
     */
    public static function _grab_json($affiliate_id = 'sexshows')
    {
        $url = "http://feed.camsoda.com/api/v1/browse/online_embed?id=$affiliate_id&cmp=bt_liverow&type=TRIM_PATROL";
        $raw = self::fetch_and_cache($url, 'camsoda-cache');

        if ($raw) {
            $res = @json_decode($raw);
            if ($res) {
                self::$_json = $res->results;
                if (is_array(self::$_json) && count(self::$_json) > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public static function ad_list()
    {
        $ads = file_get_contents("../data/camsoda-ads.dat");
        if ($ads) {
            $ads = unserialize($ads);

            return $ads;
        }
    }

    /**
     * @param $id
     */
    public static function affiliate_link($id = 'sexshows')
    {
        return "https://www.camsoda.com/enter.php?alt=1&id=$id&t=top_model_array&join_form=1&token_form=1";
    }

    /**
     * @param $ads
     * @return mixed
     */
    public static function filter_ads($ads)
    {
        shuffle($ads);
        $ttl = isset($_GET['ttl']) ? $_GET['ttl'] : 6;

        foreach ($ads as $ad) {
            if ( isset($_SESSION['ads_shown'][$ad['url']]) && time() - $_SESSION['ads_shown'][$ad['url']] > $ttl) {
                print "<!-- removing {$ad['url']} -->\n";
                unset($_SESSION['ads_shown'][$ad['url']]);
            }
            if (!isset($_SESSION['ads_shown'][$ad['url']])) {
                $_SESSION['ads_shown'][$ad['url']] = time();
                return $ad;
            } else {
                print "<!-- skipping {$ad['url']} -->\n";
            }
        }
        return array_pop($ads);
    }

    /**
     * @param $link
     * @param $img_url
     * @param $height
     * @param $width
     */
    public static function format_ad($link, $img_url, $height, $width)
    {
        $format = "<a href='%s' target='_blank'><img src='%s' height='%d' width='%d' alt='CamSoda - Live Cams'/></a>";

        return sprintf($format, $link, $img_url, $height, $width);
    }

    /**
     * @param array $config
     */
    public static function iframe($config = array())
    {
        if (is_array($config) && count($config) > 0) {
            foreach ($config as $k => $v) {
                if (isset(self::$_iframe_config[$k])) {
                    self::$_iframe_config[$k] = $v;
                } elseif (isset(self::$_thumb_config[$k])) {
                    self::$_thumb_config[$k] = $v;
                }
            }
        }
        self::init(self::$_iframe_config);
        extract(self::$_iframe_config);
        unset(self::$_thumb_config['iframe_url']);
        $iframe_url .= http_build_query(self::$_thumb_config);
        $style = 'margin: 0; padding: 0; border: none; width: ' . $width . '; height: ' . $height . ';';
        echo '<div class="box rCol">';
        echo '<iframe src="' . $iframe_url . '" style="' . $style . '" scrolling="no"></iframe>';
        echo '</div>';
    }

    /**
     * @param array $config
     */
    public static function iframe_mobile($config = array())
    {
        if (is_array($config) && count($config) > 0) {
            foreach ($config as $k => $v) {
                if (isset(self::$_iframe_config[$k])) {
                    self::$_iframe_config[$k] = $v;
                } elseif (isset(self::$_thumb_config[$k])) {
                    self::$_thumb_config[$k] = $v;
                }
            }
        }

        self::init(self::$_iframe_config);
        extract(self::$_iframe_config);
        unset(self::$_thumb_config['iframe_url']);
        $iframe_url .= http_build_query(self::$_thumb_config);

        $style = 'margin: 0 auto; display: block; padding: 0; border: none; width: ' . $width . '; height: ' . $height . ';';

        echo '<iframe src="' . $iframe_url . '" style="' . $style . '" scrolling="no"></iframe>';
    }

    /**
     * @param $size
     */
    public static function image_ad($size = '')
    {
        $ads = static::ad_list();
        if ($ads) {
            if (isset($ads[$size])) {
                $ads = $ads[$size];
                $ad  = static::filter_ads($ads);
                if ( !$ad ) return "<!-- no ad available -->";
                return static::format_ad(static::affiliate_link(), $ad['url'], $ad['height'], $ad['width']);
            } else {
                return "<!-- no ads for size: $size -->\n";
            }
        }
    }

    /**
     * @param array $p
     */
    public static function run($p = array())
    {
        if (false == self::$_init) {
            self::init($p);
        }

        extract(self::$_thumb_config);

        $grab = self::_grab_json($affiliate_id);
        if (!$grab) {
            return '';
        }

        $total_thumbs = $num_cols * $num_rows;

        if ($total_thumbs > ($total_results = count(self::$_json))) {
            $total_thumbs = $total_results;
        }

        if ($offset >= $total_results) {
            $offset = $total_results - $num_cols;
        }
        $thumbs = array_slice(self::$_json, $offset, $total_thumbs);

        if ('table' == $table_or_css || 'css' == $table_or_css || 'mobile' == $table_or_css) {
            require dirname(__FILE__) . '/views/thumb_' . $table_or_css . '.php';
        }
    }

    /**
     * @param $print
     */
    public static function scroller_items($print = true)
    {
        $raw = self::fetch_and_cache('https://www.camsoda.com/api/v1/featuredshows?id=sexshows&cmp=bt_fshows', 'scroller-items');
        if ($raw) {
            $res = @json_decode($raw);
            if ($res && is_array($res) && count($res) > 0) {
                echo $raw;
            }
        }
    }

    /**
     * @param  $url
     * @param  $cache_key
     * @return mixed
     */
    private static function fetch_and_cache($url, $cache_key)
    {
        $raw        = false;
        $cache_file = self::$CACHE_DIR . $cache_key . '.json';
        $cache_ok   = file_exists($cache_file) && time() - filemtime($cache_file) < self::CACHE_EXPIRE;
        if ($cache_ok) {
            $raw = file_get_contents($cache_file);
        }

        if (!$raw) {
            $raw = curl_grab($url, '', array('return' => 'content'));
            if ($raw) {
                file_put_contents($cache_file, $raw);
            }

        }

        if ($raw) {
            return $raw;
        }

        return false;
    }

    /**
     * @param array $config
     */
    private static function init($config = array())
    {
        session_start();
        static::$CACHE_DIR = dirname(__FILE__) . '/cache/';
        if (!is_dir(static::$CACHE_DIR)) {
            mkdir(static::$CACHE_DIR, 0777, true);
        }

        if (is_array($config) && count($config) > 0) {
            foreach ($config as $k => $v) {
                self::$_thumb_config[$k] = $v;
            }
        } elseif (is_array($_GET) && count($_GET) > 0) {
            foreach (self::$_thumb_config as $k => $v) {
                if (isset($_GET[$k])) {
                    self::$_thumb_config[$k] = $_GET[$k];
                }
            }
        }
        self::$_init = true;
    }
}
