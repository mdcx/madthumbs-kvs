<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require "helper.camsoda.php";
if (isset($_GET['mobile'])) {
    $cfg = array(
        'num_rows'     => 1,
        'num_cols'     => 2,
        'width'        => '380px',
        'height'       => '200px',
        'thumb_height' => '113px',
        'thumb_width'  => '150px',
        'table_or_css' => 'mobile',
    );
    Helper_Camsoda::iframe_mobile($cfg);
} elseif (isset($_GET['a'])) {
    Helper_Camsoda::run();
} elseif (isset($_GET['debug'])) {
    $raw = file_get_contents("https://feed.camsoda.com/api/v1/browse/online_embed?id=affiliate_id&cmp=bt_liverow&type=TRIM_PATROL");
    print($raw);
} elseif (isset($_GET['topad'])) {
    $cfg = array(
        'num_rows'     => 1,
        'num_cols'     => 7,
        'width'        => '1235px',
        'height'       => '230px',
        'thumb_height' => 'auto',
        'table_or_css' => 'css',
        'thumb_width'  => '100%',
    );
    Helper_Camsoda::iframe($cfg);
} else {
    Helper_Camsoda::iframe();
}

