<?php
$auto = require_once __DIR__ . '/vendor/autoload.php';

defined('KVS_ROOT') or define('KVS_ROOT', realpath(dirname(__FILE__) . '/../../') . '/');
defined('APP_ROOT') or define('APP_ROOT', dirname(__FILE__) . '/');
defined('LIB_ROOT') or define('LIB_ROOT', APP_ROOT . 'lib/');
defined('SRC_ROOT') or define('SRC_ROOT', APP_ROOT . 'src/');
define('COMPOSER_PATH', APP_ROOT . 'vendor/autoload.php');
defined('APP_NAMESPACE') or define('APP_NAMESPACE', 'App');
defined('NL') or define('NL', empty($_SERVER['REMOTE_ADDR']) ? "\n" : "<br/>\n");
ini_set('display_errors', 1);

/**
 * @return mixed
 */
function container()
{
    static $container;
    if (empty($container)) {

        $container = new League\Container\Container;
        $container->delegate(
            new League\Container\ReflectionContainer
        );
    }

    return $container;
}

/**
 * @param $part
 * @return mixed
 */
function app($part = null)
{
    static $app;
    if (empty($app)) {
        $app = new \Mad\App\MadApp(container());
    }

    if (null !== $part) {
        return $app->$part();
    }

    return $app;
}

if (!function_exists('env')) {

    /**
     * Allows user to retrieve values from the environment
     * variables that have been set. Especially useful for
     * retrieving values set from the .env file for
     * use in config files.
     *
     * @param  string  $key
     * @param  null    $default
     * @return mixed
     */
    function env(string $key, $default = null)
    {
        $value = getenv($key);
        if ($value === false) {
            $value = isset($_ENV[$key]) ? $_ENV[$key] : isset($_SERVER[$key]) ? $_SERVER[$key] : false;
        }

        // Not found? Return the default value
        if ($value === false) {
            return $default;
        }

        // Handle any boolean values
        switch (strtolower($value)) {
            case 'true' :
                return true;
            case 'false' :
                return false;
            case 'empty' :
                return '';
            case 'null' :
                return;
        }

        return $value;
    }

}

//--------------------------------------------------------------------

if (!function_exists('esc')) {

    /**
     * Performs simple auto-escaping of data for security reasons.
     * Might consider making this more complex at a later date.
     *
     * If $data is a string, then it simply escapes and returns it.
     * If $data is an array, then it loops over it, escaping each
     * 'value' of the key/value pairs.
     *
     * Valid context values: html, js, css, url, attr, raw, null
     *
     * @param  string|array    $data
     * @param  string          $context
     * @param  string          $encoding
     * @return string|array
     */
    function esc($data, $context = 'html', $encoding = null)
    {
        if (is_array($data)) {
            foreach ($data as $key => &$value) {
                $value = esc($value, $context);
            }
        }

        if (is_string($data)) {
            $context = strtolower($context);

            // Provide a way to NOT escape data since
            // this could be called automatically by
            // the View library.
            if (empty($context) || $context == 'raw') {
                return $data;
            }

            if (!in_array($context, ['html', 'js', 'css', 'url', 'attr'])) {
                throw new \InvalidArgumentException('Invalid escape context provided.');
            }

            if ($context == 'attr') {
                $method = 'escapeHtmlAttr';
            } else {
                $method = 'escape' . ucfirst($context);
            }

            // @todo Optimize this to only load a single instance during page request.
            $escaper = new \Zend\Escaper\Escaper($encoding);

            $data = $escaper->$method($data);
        }

        return $data;
    }

}

//--------------------------------------------------------------------

if (!function_exists('session')) {

    /**
     * A convenience method for accessing the session instance,
     * or an item that has been set in the session.
     *
     * Examples:
     *    session()->set('foo', 'bar');
     *    $foo = session('bar');
     *
     * @param  string                                    $val
     * @return \CodeIgniter\Session\Session|mixed|null
     */
    function session($val = null)
    {
        // Returning a single item?
        if (is_string($val)) {
            helper('array');

            return dot_array_search($val, $_SESSION);
        }

        return \Config\Services::session();
    }

}

//--------------------------------------------------------------------

if (!function_exists('timer')) {

    /**
     * A convenience method for working with the timer.
     * If no parameter is passed, it will return the timer instance,
     * otherwise will start or stop the timer intelligently.
     *
     * @param  string|null                      $name
     * @return \CodeIgniter\Debug\Timer|mixed
     */
    function timer(string $name = null)
    {
        $timer = \Config\Services::timer();

        if (empty($name)) {
            return $timer;
        }

        if ($timer->has($name)) {
            return $timer->stop($name);
        }

        return $timer->start($name);
    }

}

//--------------------------------------------------------------------

if (!function_exists('service')) {

    /**
     * Allows cleaner access to the Services Config file.
     * Always returns a SHARED instance of the class, so
     * calling the function multiple times should always
     * return the same instance.
     *
     * These are equal:
     *  - $timer = service('timer')
     *  - $timer = \CodeIgniter\Config\Services::timer();
     *
     * @param  string   $name
     * @param  array    ...$params
     * @return mixed
     */
    function service(string $name, ...$params)
    {
        return Services::$name(...$params);
    }

}

if (!function_exists('msg')) {
    /**
     * @param $label
     * @param $msg
     * @param $dump
     */
    function msg($label, $msg = '', $dump = false)
    {

        $labels = ['debug', 'error', 'info', 'warn', 'notice'];
        $gc     = get_caller();
        if (!is_string($label) || !in_array(strtolower($label), $labels)) {
            $args = func_get_args();
            if (count($args) == 2) {
                if (is_string($args[1])) {
                    $gc  = $args[0];
                    $msg = $args[1];
                } else {
                    $msg  = $args[0];
                    $dump = $args[1];
                }
            } else {
                $msg = $args[0];
            }
            $label = 'info';
        }

        $label = strtoupper($label);

        $out = sprintf("[%s] %-6s[%s]", date('Y-m-d H:i:s'), $label, $gc);

        if (is_string($msg)) {
            $out .= " $msg";
        } elseif (is_array($msg) || is_object($msg)) {
            $dump = $msg;
            $msg  = "dump";
        }

        if ($dump !== false) {

            $c    = 0;
            $dump = print_r($dump, true);
            $dump = str_replace(['Array', 'stdClass Object'], ['%%%A%%%', '%%%O%%%'], $dump);
            while (strpos($dump, '%%%') !== false && $c++ <= 3) {
                $dump = preg_replace("/%%%(A|O)%%%\s+\((\s*?.*?\s+)\)/ism", "$1 [$2]", $dump);
                // print "$c: $dump";
            }
            $dump = explode("\n", $dump);
            array_walk($dump, function (&$item, $k) {$item = "    {$item}";});

            $dump = implode("\n", $dump);
            $out .= NL . "  " . trim($dump) . NL . "";
        }
        if (function_exists('log_output')) {
            log_output($out);
        } else {
            print $out . NL;
        }

    }
}
