<?php
defined('DB_H') OR define('DB_H','db.madthumbs.com');
defined('DB_U') OR define('DB_U','kvs');
defined('DB_P') OR define('DB_P','nFm2rBoGVBED');
defined('DB_D') OR define('DB_D','madthumbs');
return [
    'default'     => env('DB_CONNECTION', 'kvs'),
    'connections' => [

        'kvs' => [
            'driver'    => 'mysql',
            'username'  => DB_U,
            'password'  => DB_P,
            'hostname'  => DB_H,
            'port'      => env('DB_PORT', '3306'),
            'database'  => 'madthumbs',
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => '',
            'strict'    => true,
            'engine'    => null,
        ],

        'mad' => [
            'driver'      => 'mysql',
            'username'    => DB_U,
            'password'    => DB_P,
            'hostname'    => DB_H,
            'port'        => env('DB_PORT', '3306'),
            'database'    => 'madthumbs_test',
            'unix_socket' => env('DB_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],
    ],

    'redis'       => [

        'client'  => 'predis',

        'default' => [
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port'     => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],
];
