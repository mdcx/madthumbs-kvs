<?php
$lang = \Mad\Core\Config::get('lang');

$ret = [
    'status_values'              => [
        0 => $lang['users']['user_field_status_disabled'],
        1 => $lang['users']['user_field_status_not_confirmed'],
        2 => $lang['users']['user_field_status_active'],
        3 => $lang['users']['user_field_status_active_premium'],
        4 => $lang['users']['user_field_status_anonymous'],
        6 => $lang['users']['user_field_status_webmaster'],
    ],

    'gender_values'              => [
        0 => ' ',
        1 => $lang['users']['user_field_gender_male'],
        2 => $lang['users']['user_field_gender_female'],
        3 => $lang['users']['user_field_gender_couple'],
        4 => $lang['users']['user_field_gender_transsexual'],
    ],

    'relationship_status_values' => [
        0 => ' ',
        1 => $lang['users']['user_field_relationship_status_single'],
        2 => $lang['users']['user_field_relationship_status_married'],
        3 => $lang['users']['user_field_relationship_status_open'],
        4 => $lang['users']['user_field_relationship_status_divorced'],
        5 => $lang['users']['user_field_relationship_status_widowed'],
    ],

    'orientation_values'         => [
        0 => ' ',
        1 => $lang['users']['user_field_orientation_unknown'],
        2 => $lang['users']['user_field_orientation_straight'],
        3 => $lang['users']['user_field_orientation_gay'],
        4 => $lang['users']['user_field_orientation_lesbian'],
        5 => $lang['users']['user_field_orientation_bisexual'],
    ],

    'hair_values'                => [
        0 => ' ',
        1 => 'black',
        2 => 'dark',
        3 => 'red',
        4 => 'brown',
        5 => 'blond',
        6 => 'grey',
        7 => 'bald',
        8 => 'wig',
    ],

    'eye_values'                 => [
        0 => ' ',
        1 => 'blue',
        2 => 'gray',
        3 => 'green',
        4 => 'amber',
        5 => 'brown',
        6 => 'hazel',
        7 => 'black',
    ],

];
unset($lang);

return $ret;
