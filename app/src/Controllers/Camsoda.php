<?php namespace KVS\Controllers;
use \Mad\Support\Collection;
use \Mad\Support\Arr;
use \Mad\Support\Fluent;

class Camsoda
{
    /**
     * @var mixed
     */
    protected $app;

    /**
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function ads()
    {
        $ads = $this->app->loader()->find('*.*', KVS_ROOT . 'images/ads/camsoda/');
        $ret = [];
        foreach ($ads as $k => $v) {
            $m = preg_match('#(?P<width>\d+)x(?P<height>\d+)#', $k, $match);
            if ($m) {
                $fn   = basename($k);
                $size = $match['width'] . 'x' . $match['height'];

                $item = [
                    'filename' => $fn,
                    'path'     => '/images/ads/camsoda/',
                    'url'      => '/images/ads/camsoda/' . $fn,
                    'height'   => $match['height'],
                    'width'    => $match['width'],
                    'size'     => $size,
                ];
                $ret[$size][] = $item;
            }
        }
        $this->app->loader()->helper('file');
        $data = serialize($ret);

        $write = write_file(APP_ROOT.'../data/camsoda-ads.dat', $data);
        if ( $write ) {
            print "Sucessfully wrote new CamSoda ads JSON".NL;
        }
    }
}
