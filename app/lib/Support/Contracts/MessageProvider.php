<?php

namespace Mad\Support\Contracts;

interface MessageProvider
{
    /**
     * Get the messages for the instance.
     *
     *@return \Mad\Support\Contracts\MessageBag
     */
    public function getMessageBag();
}
