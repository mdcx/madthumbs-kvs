<?php namespace Mad\Core;

use Nette\Utils\Finder;

class Loader
{
    /**
     * @var mixed
     */
    protected $app;

    /**
     * @var mixed
     */
    protected $cache = [
        'helpers' => false,
        'lang'    => false,
        'configs' => false,
    ];

    protected $_loaded = [];

    /**
     * @param Nette\Utils\Finder $finder
     */
    public function __construct()
    {
        $this->build_helper_cache();

        // $this->app = app();
    }

    /**
     * @param $what
     * @param $where
     */
    public function find($what, $where)
    {
        return Finder::findFiles($what)->in($where);
    }

    /**
     * @return mixed
     */
    public function findConfigs()
    {
        $configs = Finder::findFiles('*.php')->in(APP_ROOT . 'src/Config/');
        return $configs;
    }

    /**
     * @param $name
     */
    public function helper($name)
    {
        if (isset($this->cache['helpers']['funcs'][$name])) {
            $file = $this->cache['helpers']['funcs'][$name];
            if ( empty($this->_loaded[$file])) {
                require_once($file);
                return true;
            }
        }
        throw new \Exception("Failed to locate helper: '$name'");
    }

    private function build_helper_cache()
    {

        $dirs = [];
        if (is_dir(LIB_ROOT . 'Helpers/')) {
            $dirs[] = LIB_ROOT . 'Helpers/';
        }

        if (is_dir(SRC_ROOT . 'Helpers/')) {
            $dirs[] = SRC_ROOT . 'Helpers/';
        }

        $helpers                = Finder::findFiles("*.php")->in($dirs);
        $this->cache['helpers'] = [];
        foreach ($helpers as $k => $v) {
            $bn     = basename($k);
            $no_ext = substr($bn, 0, -4);
            $dn     = dirname($k);
            if (substr($no_ext, -7) === '_helper') {
                $helper_name = substr($no_ext, 0, -7);
                $type        = 'funcs';
            } elseif (substr($no_ext, 0, 7) === 'helper.') {
                $helper_name = str_replace('helper.', '', $no_ext);
                $type        = 'funcs';
            } else {
                $type        = 'class';
                $helper_name = $no_ext;
            }
            $this->cache['helpers'][$type][$helper_name] = $k;
        }
    }
}


