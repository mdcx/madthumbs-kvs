<?php
/**
 * This is my ghetto attempt to take this insane piece of software and localize some
 * of the more important information into a class rather than a having to constantly
 * re-use GLOBAL and a bunch of shit.
 *
 * Basically, the KVS $config array will be here, the $database_selectors, $database_tables, and some
 * hardcoded values such as hair color, eye color, etc.
 *
 * We will also import the localization/language data.
 *
 * On top of that, if we're wanting to run a script or something that depends on the KVS environment, we will
 * auto-load all of the required function files from within the KVS include path.
 *
 * That will be in addition to some basic and slightly more usable tools for doing things like:
 * CLI args, DB queries, Str transformations, thubmnails, benchmarking, and so forth.
 *
 * This is not a perfect solution, and was thrown together under an extremely tight deadline. However,
 * it did help to speed-up the tasks that were needing done.
 *
 */
namespace Mad\Core;

class Kernel
{
    /**
     * Absolute path to base directory of KVS installation
     */
    const KVS_BASE = '/home/web1/madthumbs.com/htdocs/';

    /**
     * Relative path to where the custom additions reside
     */
    const KVS_CUSTOM = 'custom/app/';

    /**
     * @var array
     */
    protected static $_autoload = [

        // 'helpers'       => [
        //     'general',
        //     'text',
        //     'url',
        //     'array',
        //     // 'curl',
        // ],
        'classes'       => [
        //     'BaseConfig'  => false,
        //     'Lock'        => true,
            'MadDB'       => false,
            'Timer'       => true,
            'Args'        => true,
        //     'VideoThumbs' => false,
        //     'Str'         => false,
        ],
        'admin_include' => [
            'functions_base',
            'functions_servers',
            'functions_screenshots',
            'functions_cron_conversion',
            'functions_admin',
            'functions',
            'placeholder',
            'pclzip.lib',
        ],
        'config'        => [
            'database',
        ],
        'instantiate'   => [
            'db'     => 'KVSDB',
            'db_old' => 'MTDB',
            'args'   => 'Args',
            'timer'  => 'Timer',
        ],
    ];

    /**
     * This array will hold all of the assorted values we obtain from
     * KVS
     *
     * @var array
     */
    protected static $_data = [
        'config'             => false,
        'lang'               => false,
        'countries'          => false,
        'regexp'             => false,
        'user_values'        => false,
        'ftp_servers'        => false,
        'conversion_servers' => false,
        'db_conn'            => false,
        'database_selectors' => false,
        'database_tables'    => false,
    ];

    /**
     * A list of functions that are used to do whatever is needed to get
     * data from KVS
     *
     * @var array
     */
    protected static $_gen_funcs = [
        'config'            => '_gen_config',
        'user_values'       => '_gen_user_values',
        'regexp'            => '_gen_regexp',
        // 'lang' => '_gen_lang',
        'db_conn'           => 'setup_db',
        'databaseSelectors' => '_gen_database_selectors',
    ];

    /**
     * @var mixed Have we initialized our paths yet.
     */
    protected static $_initialized = false;

    /**
     * @var array
     */
    protected static $_instances = [];

    /**
     * @var array
     */
    protected static $_loaded = [];

    /**
     * @var array
     */
    protected static $_paths = [
        // 'custom'         => '',
        // 'custom_include' => '',
        'custom'        => KVS_ROOT . 'custom/',
        'app'           => 'custom/app/',
        'app_classes'   => '',
        'app_helpers'   => '',
        'app_config'    => '',
        'app_include'   => '',
        'logs'          => 'admin/logs',
        'admin'         => '',
        'admin_include' => '',
        'admin_data'    => '',
        'contents'      => '',
        'cdn'           => 'cdn_dir/kvs',
        'tmp'           => '',
        'lang'          => 'admin/langs',
    ];

    /**
     * @var mixed
     */
    protected static $instance;

    /**
     * @param  $name
     * @param  $arguments
     * @throws Exception
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        if (static::$_initialized === false) {
            static::init();
        }

        if ($name === 'allPaths') {
            return static::$_paths;
        }

        if (substr($name, -4) === 'Path') {
            $name = substr($name, 0, -4);
            if (static::hasPath($name)) {
                return static::$_paths[$name] . (isset($arguments[0]) && $arguments[0] != '' ? $arguments[0] : '');
            } else {
                throw new Exception("Failed to locate path for '$name'");
            }
        } else if (substr($name, -4) === 'Data') {
            $name = substr($name, 0, -4);
            if (($data = static::hasData($name, $arguments))) {
                if ($data) {
                    if (count($data) === 2) {
                        return static::$_data[$data[0]][$data[1]];
                    }

                    return static::$_data[$data[0]];
                }
            }
            throw new Exception("Failed to locate data for '$name'");
        } elseif (substr($name, 0, 4) === 'load') {
            $load_what = strtolower(substr($name, 4));
            return static::loadFile($arguments[0], $load_what);
        } elseif (substr($name, 0, 3) === 'get') {
            return static::_get(substr($name, 3), $arguments);
        } else {
            $name = strtolower($name);
            if (isset(static::$_instances[$name])) {
                return static::$_instances[$name];
            }
        }

        return false;
    }

    /**
     * load_helper
     *
     * @param  $which
     * @param  null     $path
     * @param  bool     $force
     * @return bool
     */
    public static function helper($which, $path = null, $force = false)
    {
        $file = static::locateFile($which, $path);

        $which = static::_pretty_name($which, 'helper');
        if ($path === null) {
            $which = static::appHelpersPath($which);
        }

        return static::load_file($which, $path, $force);
    }

    /**
     * init
     */
    public static function init()
    {
        static::$instance = new self;

        if (!self::$_initialized) {
            foreach (self::$_paths as $k => $v) {
                $w1 = $w2 = false;
                if ($v === '') {
                    $v = strpos($k, '_') !== false ? str_replace('_', '/', $k) : $k;
                }
                if (strpos($k, '_') !== false) {
                    list($w1, $w2) = explode('_', $k);
                    unset(self::$_paths[$k]);
                    $k = $w1 . ucfirst($w2);
                }
                if ($w1 !== false && isset(self::$_paths[$w1])) {
                    $base = self::$_paths[$w1];
                    $v    = $w2;
                } else {
                    $base = self::KVS_BASE;
                }

                self::$_paths[$k] = $v[0] === '/' ? $v : $base . rtrim($v, '/') . '/';
            }
            self::$_initialized = true;
        }
        // pre-load the setup array
        static::configData();
        // go ahead and finish pre-loadng the important system aspects.
        static::autoload();

    }

    public static function instance()
    {
        return static::$instance;
    }

    /**
     * is_loaded
     *
     * @param  $filename
     * @param  null        $path
     * @return bool
     */
    public static function is_loaded($filename, $path = null)
    {
        $fullpath = static::_abs_path($filename, $path);

        return isset(static::$_loaded[$fullpath]);
    }

    /**
     * @param $file
     * @param $type
     */
    public static function loadFile($file, $type = null)
    {
        print "loadFile Args: ";
        var_dump(func_get_args());
        // maybe a direct link to file?
        if ($file[0] === '/' && file_exists($file)) {
            return static::requireFile($file);
        }

        $path = '';
        if (strpos($file, '/') !== false) {
            $parts = explode('/', $file);
            $file  = array_pop($parts);
            $path  = implode('/', $parts) . '/';
            if ( $type !== null )
                $file  = preg_replace(sprintf('#(%s\.|\_%s|\.php)#', $type), '', $file);
        }

        print "file pre: $file .... (type: $type)";
        if ($type === 'helper') {
            $searchNames = [
                "{$type}.{$file}.php",
                "{$file}_{$type}.php",
            ];
        } elseif (in_array($type, ['config', 'class', 'include'])) {
            $searchNames = ["$file.php"];
        } else {
            $searchNames = [static::_pretty_name($file)];
        }
        var_dump($search_names);
        var_dump($file);
        print "search names: "; var_dump($searchNames);
        $searchPaths = [
            'helper' => [static::appHelpersPath($path)],
            'class'  => [static::appClassesPath($path)],
            'config' => [static::appConfigPath($path)],
        ];

        if (isset($searchPaths[$type])) {
            $paths = $searchPaths[$type];
        } else {
            $paths = [];
        }

        $paths[] = static::appIncludePath();
        if ($path !== '') {
            array_unshift($paths, $path);
        }

        $file = static::locateFile($searchNames, $paths);
        if (!$file) {
            throw new Exception("Failed to locateFile: '$searchNames[0]'");
        }

        if (false === static::is_loaded($file)) {
            $require = static::requireFile($file);
            if (false === $require) {
                throw new Exception("Failed to requireFile: $file");
            }

        }

        return static::is_loaded($file);
    }

    /**
     * @param $data_id
     * @param $filename
     * @param $var_name
     */
    public static function load_file_var($data_id, $filename, $var_name = null)
    {
        $config = ($data_id !== 'config') ? static::configData() : [];
        if (file_exists($filename)) {
            if ($var_name === null) {
                static::$_data[$data_id] = require_once $filename;
            } else {
                // include file and try to find vars
                $ret = [];
                require_once $filename;
                if (!is_array($var_name)) {
                    $var_name = (array) $var_name;
                }

                foreach ($var_name as $v) {
                    if (isset(${$v})) {
                        if ($v === $data_id) {
                            static::$_data[$data_id] = $$v;
                        } else {
                            $ret[$v] = $$v;
                        }

                    }
                }
                if (count($ret) > 0) {
                    foreach ($ret as $k => $v) {
                        static::$_data[$k] = $v;
                    }
                }
            }
            static::$_loaded[$filename] = time();
        } else {
            static::$_data[$data_id] = false;
        }

        return static::$_data[$data_id];
    }

    /**
     * load_manual
     *
     * @param $fullpath
     */
    public static function load_manual($fullpath)
    {
        static::$_loaded[static::_pretty_path($fullpath)] = time();
    }

    /**
     * @param $filename
     * @param array       $path
     * @param $ext
     */
    public static function locateFile($names = [], $paths = [])
    {
        foreach ($paths as $path) {
            foreach ($names as $name) {
                $filepath = $path . $name;
                if (file_exists($filepath)) {
                    print "found; $filepath\n";
                    return $filepath;
                } else {
                    print "unable to locate: $filepath\n";
                }
            }
        }

        return false;
    }

    /**
     * @param $what
     */
    public static function make($what)
    {

    }

    /**
     * tbl
     *
     * @param  $name
     * @return null
     */
    public static function tbl($name)
    {
        $tables = static::databaseTables();

        return isset($tables[$name]) ? $tables[$name] : null;
    }

    /**
     * _abs_path
     *
     * @param  $filename
     * @param  null        $path
     * @return string
     */
    private static function _abs_path($filename, $path = null)
    {
        $default_path            = KVS_BASE . KVS_CUSTOM;
        !null === $path or $path = $default_path;

        if ($filename[0] === '/') {
            $path     = dirname($filename) . '/';
            $filename = basename($filename);
        }
        $filename = rtrim($filename, '.php') . '.php';

        return $path . $filename;
    }

    /**
     * _gen_config
     *
     * @return mixed
     */
    private static function _gen_config()
    {
        return static::load_file_var('config', static::adminIncludePath('setup.php'), 'config');
    }

    /**
     * _gen_countries
     */
    private static function _gen_countries()
    {
        require static::adminIncludePath('list_countries.php');
        static::$_data['countries'] = $list_countries;
        unset($list_countries);
    }

    /**
     * _gen_database_selectors
     *
     * @return mixed
     */
    private function _gen_database_selectors()
    {
        $config = static::configData();
        require static::adminIncludePath('database_selectors.php');
        static::$_data['database_selectors'] = $database_selectors;
        unset($database_selectors);

        return static::$_data['database_selectors'];
    }

    /**
     * _gen_database_tables
     *
     * @return mixed
     */
    private function _gen_database_tables()
    {
        $config = static::configData();
        require static::adminIncludePath('database_tables.php');

        foreach ($database_tables as $dt) {
            $base = str_replace([
                $config[tables_prefix_multi],
                $config[tables_prefix],
            ], '', $dt);
            static::$_data['database_tables'][$base] = $dt;
        }
        unset($database_tables);

        return static::$_data['database_tables'];
    }

    /**
     * }
     *
     * _gen_lang
     *
     * @param  string  $language
     * @return mixed
     */
    private static function _gen_lang($language = 'english')
    {
        require static::langPath('english.php');
        static::$_data['lang'] = $lang;
        unset($lang);

        return static::$_data['lang'];
    }

    /**
     * _gen_regexp
     *
     * @return array
     */
    private static function _gen_regexp()
    {
        $rx = [
            "include_tpl"             => "|{{include\ file\ *=['\"\ ]*([^}]*?\.tpl)['\"]|is",
            "insert_block"            => "|{{insert\ +name\ *=\ *['\"]getBlock['\"]\ +block_id\ *=\ *['\"](.*?)['\"]\ +block_name\ *=\ *['\"](.*?)['\"]|is",
            "insert_global"           => "|{{insert\ +name\ *=\ *['\"]getGlobal['\"]\ +global_id\ *=\ *['\"](.*?)['\"]|is",
            "insert_adv"              => "|{{insert\ +name\ *=\ *['\"]getAdv['\"]\ +place_id\ *=\ *['\"](.*?)['\"]|is",
            "valid_external_id"       => "|^[A-Za-z0-9_]+$|is",
            "valid_block_name"        => "|^[A-Za-z0-9\ ]+$|is",
            "valid_page_component_id" => "|^[^/^\\\\]+$|is",
            "valid_text_id"           => "|^[A-Za-z0-9_\.]+$|is",
            "check_email"             => "/^([^@])+@([^@])+\.([^@])+$/is",
            "check_alpha_numeric"     => "|^[a-zA-Z0-9_@\.\-]+$|is",
        ];

        return $rx;
    }

    /**
     * _gen_user_values
     *
     * @return mixed
     */
    private static function _gen_user_values()
    {
        return static::$_data['user_values'] = require_once static::appIncludePath('_gen_user_values.php');
    }

    /**
     * @param $name
     * @param $arguments
     */
    private static function _get($name, $arguments = null)
    {
        $name = '_' . ltrim(strtolower($name), '_');
        if (isset(static::$$name)) {
            return static::$$name;
        }

        return false;
    }

    /**
     * @param  $filename
     * @param  $type
     * @return mixed
     */
    private static function _pretty_name($filename, $type = '')
    {
        if ($type !== null && $type !== '') {
            $type .= '.';
        } else {
            $type = '';
        }
        return $type . rtrim($filename, '.php') . '.php';
    }

    /**
     * _pretty_path
     *
     * @param  $fullpath
     * @return mixed
     */
    private static function _pretty_path($fullpath)
    {
        return str_replace(KVS_BASE, '', $fullpath);
    }

    private static function autoload()
    {
        foreach (self::$_autoload as $type => $files) {
            foreach ($files as $k => $v) {
                if ($type === 'helpers') {
                    $load = static::loadHelper($v);
                } elseif ($type === 'classes') {
                    $class       = $k;
                    $instantiate = $v;
                    $load        = static::loadClass($class);
                } elseif ($type === 'admin_include') {
                    $load = static::loadFile(static::adminIncludePath($v));
                } elseif ($type === 'config') {
                    $load = static::loadConfig($v);
                } else {
                    $load = 'unknown';
                }

                if ( $load === false ) msg("loader", "failed to load $type -> $v");
            }
        }

        if (count(static::$_autoload['instantiate']) > 0) {
            $inst = static::$_autoload['instantiate'];
            foreach ($inst as $prop => $class) {
                static::$instance->$prop   = new $class();
                static::$_instances[$prop] = static::$instance->$prop;
            }
        }
    }

    /**
     * hasData
     *
     * @param  $name
     * @param  null         $key
     * @return bool|mixed
     */
    private static function hasData($name, $key = null)
    {
        $ret = [];

        if (strtolower($name) !== $name) {
            if (!ctype_lower($name)) {
                $name = preg_replace('/\s+/u', '', ucwords($name));
                $name = strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1' . '_', $name));
            }
        }

        if (static::$_data[$name] === false && ($generator = static::hasGenerator($name))) {
            $generated = static::$generator();
            if ($generated !== false) {
                static::$_data[$name] = $generated;
            }
        }

        if (is_array($key) && !isset($key[0])) {
            $key = null;
        }

        if (isset(static::$_data[$name]) && static::$_data[$name] !== false) {
            $ret[] = $name;
            if (null !== $key && isset(static::$_data[$name][$key])) {
                $ret[] = $key;
            }

            return $ret;
        }

        return false;
    }

    /**
     * hasGenerator
     *
     * @param  $name
     * @return bool|mixed|string
     */
    private static function hasGenerator($name)
    {
        $tmp_func = 'K::_gen_' . strtolower($name);

        return isset(static::$_gen_funcs[$name]) ? static::$_gen_funcs[$name] : is_callable($tmp_func) ? '_gen_' . strtolower($name) : false;
    }

    /**
     * hasPath
     *
     * @param  $name
     * @return bool
     */
    private static function hasPath($name)
    {
        return isset(static::$_paths[$name]);
    }

    /**
     * A central way to require a file is loaded. Split out primarily
     * for testing purposes.
     *
     * @codeCoverageIgnore
     *
     * @param  string $file
     * @return bool
     */
    private static function requireFile($file)
    {
        $file = static::sanitizeFilename($file);

        if (file_exists($file)) {
            require_once $file;
            static::$_loaded[$file] = time();

            return $file;
        }

        return false;
    }

    /**
     * Sanitizes a filename, replacing spaces with dashes.
     *
     * @param  string $filename
     * @return string The sanitized filename
     */
    private static function sanitizeFilename($filename)
    {
        // Only allow characters deemed safe for POSIX portable filenames.
        // Modified to allow backslash and colons for on Windows machines.
        $filename = preg_replace('/[^a-zA-Z0-9\s\/\-\_\.\:\\\\]/', '', $filename);

        // Clean up our filename edges.
        $filename = trim($filename, '.-_');

        return $filename;
    }

    /**
     * setup_db
     *
     * @return mixed
     */
    private function setup_db()
    {
        require static::adminIncludePath('setup_db.php');
        if (isset($conn)) {
            static::$_data['db_conn'] = $conn;

            return static::$_data['db_conn'];
        }
    }
}
