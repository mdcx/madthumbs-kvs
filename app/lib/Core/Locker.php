<?php
namespace Mad\Core;
class Locker
{
    /**
     * @var string
     */
    protected $_lock_dir = KVS_ROOT . 'admin/data/system/locks/';

    /**
     * @var array
     */
    protected $_locks = [];

    /**
     * Manage getting advisory locks on a specific file.
     *
     *                               If 'task-9', it uses it to generate a lock file path
     *                               If 'videos/task-9', it will put the file in a folder
     *                               If /videos/task-9' it uses the value as an abs path
     * @param  bool|string     $lock_id If false, unlocks all known locks created by the function.
     * @param  int             $wait    If lock fails, wait this many seconds and try again.
     * @param  bool            $debug   If true, echo some debug messages.
     * @return bool|resource
     */
    public function flip($lock_id = false, $wait = 0, $debug = false)
    {
        $lock_file = $this->_lock_file($lock_id);
        if ($this->is_locked($lock_id)) {
            return $this->forget($lock_id);
        } else {
            return $this->get($lock_id);
        }
    }

    /**
     * @param $lock_id
     * @param false      $wait
     * @param $debug
     */
    public function forget($lock_id = false, $wait = 0, $debug = false)
    {
        if ($lock_id === false) {
            foreach ($this->_locks as $lock_id => $lock) {
                $this->forget($lock_id, 0, $debug);
            }

            return true;
        }

        $lock_file = $this->_lock_file($lock_id);
        // if lock exists, let's unlock it.
        if (isset($this->_locks[$lock_id])) {
            if (is_resource($_locks[$lock_id])) {
                $unlock = flock($_locks[$lock_id], LOCK_UN);
                if (!$unlock) {
                    if ($debug) {
                        msg("locker","Failed to unlock lock named $lock_id...");
                    }

                    return false;
                }
                fclose($_locks[$lock_id]);
                unlink($lock_file);
                unset($_locks[$lock_id]);
                if ($debug) {
                    msg("locker","Unlocked lock for $lock_id ...");
                }

                return true;
            }
            if ($debug) {
                msg("locker","$lock_id doesn't have a valid file pointer.");
            }
        }

        return false;
    }

    /**
     * @param $lock_id
     * @param false      $wait
     * @param $debug
     */
    public function get($lock_id = false, $wait = 0, $debug = false)
    {
        $lock_file = $this->_lock_file($lock_id);
        $fp        = fopen($lock_file, 'c');
        if (!$fp) {
            if ($debug) {
                msg("locker","Failed to open/create lock file: $lock_file");
            }

            return false;
        }

        if (!($lock = flock($fp, LOCK_EX | LOCK_NB))) {
            if ($debug) {
                msg("locker","Failed to obtain lock: $lock_file");
            }

            return false;
        } else {
            if ($debug) {
                msg("locker","Obtained lock for $lock_file");
            }
            fwrite($fp, getmypid());

            return $this->_locks[$lock_id] = $fp;
        }

        return false;
    }

    /**
     * @param $lock_id
     * @param false      $wait
     * @param $debug
     */
    public function is_locked($lock_id = false, $wait = 0, $debug = false)
    {
        return isset($this->_locks[$lock_id]) && is_resource($this->_locks[$lock_id]);
    }

    /**
     * @param  $lock_id
     * @return mixed
     */
    private function _lock_file($lock_id)
    {
        $lock_dir = $this->_lock_dir;
        if (strpos($lock_id, '/') !== false) {
            $parts   = explode('/', $lock_id);
            $lock_id = array_pop($parts);
            $lock_dir .= implode('/', $parts) . '/';
        }

        return $lock_dir . rtrim($lock_id, '.lock') . '.lock';
    }
}
