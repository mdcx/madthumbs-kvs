<?php

/*
 * Anime Download Manager v0.5.1
 * @author Mike Campbell
 */
namespace Mad\Core;

define('NO_DEFAULT', '!NO_DEFAULT!');

class Args
{
    /**
     * @var mixed
     */
    static $instance = null;

    /**
     * @var array
     */
    private $_args = [];

    /**
     * @var array
     */
    private $_cast_map = [
        'false' => false,
        'true'  => true,
        'null'  => null,
        ['is_numeric', 'intval'],
    ];

    // alias for controller.
    /**
     * @var mixed
     */
    private $_controller;

    /**
     * @var string
     */
    private $_default_controller = 'help';

    /**
     * @var string
     */
    private $_default_method = 'index';

    /**
     * @var mixed
     */
    private $_method;

    /**
     * @var mixed
     */
    private $_parsed = false;

    /**
     * @var mixed
     */
    private $_prior_status_len = false;

    /**
     * @var array
     */
    private $_req_rules = [];

    /**
     * @var array
     */
    private $_req_rules_passed = [];

    /**
     * @var array
     */
    private $_required_args = [];

    /**
     * @var mixed
     */
    private $_required_parsed = false;

    /**
     * @var array
     */
    private $_route = [];

    /**
     * @var array
     */
    private $_trim = [];

    /**
     * @param $args
     */
    public function __construct($controller = '', $trim = [])
    {
        $this->_trim = $trim;
        if ( isset($_SERVER['argv']) && isset($_SERVER['argv'][0]) ) {
            $this->_default_controller = str_replace('.php','',$_SERVER['argv'][0]);
        }
        $this->_controller = $controller != '' ? $controller : $this->_default_controller;
        $this->_method     = $this->_default_method;
        $this->parse_args();
    }

    /**
     * @param  $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->_args[$name])) {
            return $this->_args[$name];
        }

        return $this->get($name);

        return null;
    }

    /**
     * Fetch from array.
     *
     * This is a helper function to retrieve values from global arrays
     *
     * @param  array
     * @param  string
     * @param  bool
     * @param  mixed    $index
     * @param  mixed    $default
     * @return mixed
     */
    public function _fetch_from_array(&$array, $index = '', $default = false)
    {
        // Check if a field has been provided
        if (null === $index) {
            return empty($array) || 0 === count($array) ? [] : $array;
        }

        return isset($array[$index]) ? $array[$index] : $default;
    }

    // --------------------------------------------------------------------

    /**
     * @param  mixed   $index
     * @param  mixed   $default
     * @return mixed
     */
    public function _get($index = null, $default = false)
    {
        return $this->_fetch_from_array($_GET, $index, $default);
    }

    public function _get_web_args()
    {
        return array_merge($_GET, $_POST);
    }

    // --------------------------------------------------------------------

    /**
     * @param  string|null $index
     * @param  mixed       $default
     * @return mixed
     */
    public function _post($index = null, $default = false)
    {
        return $this->_fetch_from_array($_POST, $index, $default);
    }

    /**
     * Add an argument if not exists.
     *
     * @param  type   $name  Name of arg
     * @param  type   $value Value of arg
     * @return type
     */
    public function add($name, $value)
    {
        if (isset($this->_args[$name])) {
            return;
        }

        $this->_args[$name] = $value;
    }

    /**
     * Present a Y/n confirm line.
     *
     * @deprecated Use prompt instead
     *
     * @param string $label
     * @param type   $die
     * @param type   $default
     */
    public function confirm($label = '', $die = true, $default = 'No')
    {
        if ('' == $label) {
            $label = 'Continue?';
        }

        $ret = $this->prompt($label, $default);
        if (true !== $ret && false !== $ret) {
            show_error('Invalid input received. Please try again.');
        }

        if ($die && !$ret) {
            die;
        }
    }

    /**
     * @return mixed
     */
    public function controller()
    {
        return $this->_controller;
    }

    /**
     * Erases a previously printed bar.
     *
     *             cursor position
     *
     * @param  bool   if       the bar should be cleared in addition to resetting the
     * @param  mixed  $len
     * @param  mixed  $clear
     * @return bool
     */
    public function erase($len, $clear = true)
    {
        if (!$clear) {
            echo str_repeat(chr(0x08), $len);
        } else {
            echo str_repeat(chr(0x08), $len),
            str_repeat(chr(0x20), $len),
            str_repeat(chr(0x08), $len);
        }
    }

    /**
     * @param  $name
     * @param  false   $default
     * @return mixed
     */
    public function get($name = false, $default = false)
    {
        if (false === $name) {
            return $this->_args;
        }

        if (isset($this->_args[$name])) {
            return $this->_args[$name];
        }

        if (($ret = $this->_get($name, $default))) {
            return $ret;
        }

        if (($ret = $this->_post($name, $default))) {
            return $ret;
        }

        return $default;
    }

    /**
     * Read from stdin.
     *
     * @param  mixed  $default
     * @return type
     */
    public function get_stdin($default = '')
    {
        $fp   = fopen('php://stdin', 'r');
        $line = trim(fgets($fp));
        fclose($fp);
        if ('' == $line && null !== $default) {
            $line = $default;
        }

        return $line;
    }

    /**
     * Read from stdin on the CLI.
     *
     * @param  type   $default
     * @param  string $msg       A message to write prior for opening stdin
     * @return type
     */
    public function input($default = null, $msg = false)
    {
        if ($msg && '' != $msg && strlen($msg) > 0) {
            echo "$msg [$default]: ";
        }

        return $this->get_stdin($default);
    }

    public static function instance()
    {
        if (empty(static::$instance)) {
            static::$instance = new self();
        }

        return static::$instance;
    }

    /**
     * @return mixed
     */
    public function is_parsed()
    {
        return $this->_parsed;
    }

    /**
     * Return method name if applicable.,.
     *
     * @return type
     */
    public function method()
    {
        return $this->_method;
    }

    /**
     * Enter a number of empty lines.
     *
     * @param int   Number of lines to output
     * @param mixed $num
     */

    /**
     * @return mixed
     */
    public function new_line($num = 1)
    {
        // Do it once or more, write with empty string gives us a new line
        for ($i = 0; $i < $num; ++$i) {
            $this->write();
        }
    }

    /**
     * Works just like prompt with the added benefit that the variable can be pre-passed via
     * the CLI invocation.. ie: --value dick.
     *
     * @param  type   $name
     * @param  type   $default
     * @param  string $ui_text
     * @param  type   $confirm
     * @return bool
     */
    public function param($name, $default = false, $ui_text = '', $confirm = false)
    {
        if ('' == $ui_text) {
            $ui_text = 'Please enter a value for ' . $name;
        }

        $ret = $default;

        if ($confirm) {
            $this->confirm($ui_text, $die, $default);
            $confirm = ui('Y/n', $ui_text);
            if (!in_array($confirm, ['Y/n', 'Y'])) {
                die("\n");
            }

            return true;
        }
        // check to see if --$name has been sent to the CLI.
        if (false === ($ret = $this->get($name))) {
            $ret = ui($default, $ui_text);
        }

        return $ret;
    }

    /**
     * Parses argv into.
     *
     * @usage index.php <action> <args> where <args>
     *         -abc            returns a = true, b = true, c = true
     *         --abc            returns: abc = true
     *         -a -a hello        returns array (true, hello)
     *         --a hello -a    returns a = true
     *         --num 1 --find Hello World -Ab -c 0
     *                         returns: num = 1, find = "Hello World", A, b = true, c = false
     *
     * @param  array|bool $args     List of args to search for along with the associated help
     * @param  bool       $force
     * @return array
     */
    public function parse_args($args = false, $force = false)
    {
        $params = [];
        if (!$force && $this->is_parsed()) {
            return $this->_args;
        }

        if (empty($_SERVER['argv'])) {
            $this->_args = array_merge($_GET, $_POST);
            if (!$this->_args) {
                $this->_args = [];
            }

            // grab the controller and method from the router.
            if (!($action = $this->get('a')) && !($action = $this->get('action'))) {
                $action = 'default';
            }

            $this->_controller = $action;
            $this->_method     = 'index';
            $this->set_parsed(true);

            return $this->_args;
        }

        $arg_list = $args ? $args : $_SERVER['argv'];
        if (empty($arg_list) || 0 == count($arg_list)) {
            $this->set_parsed();

            return $this->_args;
        }

        $this->_file =str_replace('.php', '', array_shift($arg_list));
        $slice       = 0;
        for ($i = 0; $i < count($arg_list); $i++) {
            // not an arg
            if ($arg_list[$i][0] != '-') {
                if (count($this->_trim) > 0 && in_array($arg_list[$i], $this->_trim)) {
                    continue;
                }
                $this->_route[] = $arg_list[$i];
                $slice          = $i + 1;
            } else {
                break;
            }
        }

        if ( count($this->_route) === 0 ) {
            $this->_route[] = $this->_file;
        } elseif ( count($this->_route) === 1 && $this->_controller === $this->_file ) {
            array_unshift($this->_route, $this->_file);
        }

        $this->_method = count($this->_route) > 1 ? array_pop($this->_route) : $this->_default_method;

        $this->_controller = implode('\\', $this->_route);

        if (count($arg_list) > 1) {
            $arg_list      = array_slice($arg_list, $slice);
            $current_arg   = false;
            $last_arg_was  = false;
            $last_arg_name = false;
            $no_name       = 0;

            // Start the arg loop ...
            while (count($arg_list) > 0) {
                // Get the current piece of the $argv
                $this_arg   = array_shift($arg_list);
                $arg_actual = $this_arg;
                // If it has a "-" it's a flag or param
                if ('-' == $this_arg[0]) {
                    /*
                     * If the current argument starts with - and the last
                     * argument started with - then that means the last
                     * was a single flag.
                     */
                    if (is_numeric($this_arg[1])) {

                    }

                    if (false != $last_arg_name) {
                        // arg is a negative number
                        if (is_numeric($this_arg[1])) {
                            $params[$last_arg_name] = $this_arg;
                        } elseif ('param' == $last_arg_was) {
                            $params[$last_arg_name] = (bool) true;
                        } elseif (false != $last_arg_name && 'flag' == $last_arg_was) {
                            for ($i = 0; $i < strlen($last_arg_name); ++$i) {
                                $params[$last_arg_name[$i]] = true;
                            }
                            unset($params[$last_arg_name]);
                        }
                    }

                    // Only get rid of leading -
                    $current_arg = ltrim($this_arg, '-');
                    if ('' == $current_arg || is_numeric($current_arg)) {
                        $last_arg_name = false;
                        continue;
                    }

                    // Account for param=value
                    if (false !== strpos($current_arg, '=')) {
                        list($current_arg, $val_tmp) = explode('=', $current_arg);
                        $params[$current_arg]        = $val_tmp;
                        $last_arg_was                = 'value';
                    } else {
                        // If this arg is of the -aCd variety
                        $last_arg_was         = '--' != substr($arg_actual, 0, 2) ? 'flag' : 'param';
                        $params[$current_arg] = '';
                    }

                    $last_arg_name = $current_arg;

                    /*
                     * If this is the last element, then we definitely won't be able to set a value
                     * so this must either be true or be a flagset.
                     */
                    if (0 == count($arg_list)) {
                        if ('--' == substr($arg_actual, 0, 2)) {
                            $params[$current_arg] = (bool) true;
                        } else {
                            $params[$current_arg] = true;
                            if (strlen($current_arg) > 1) {
                                for ($i = 0; $i < strlen($current_arg); ++$i) {
                                    $params[$current_arg[$i]] = true;
                                }

                                unset($params[$current_arg]);
                            }
                        }
                    }
                } else {
                    if (false !== strpos($this_arg, '+')) {
                        $this_arg = str_replace('+', ' ', $this_arg);
                    }

                    // If we never sent an - or -- initially.
                    if (false == $last_arg_name) {
                        $params[$no_name++] = " $this_arg";
                    } else {
                        $arg_value = '' == $params[$last_arg_name] ? $this_arg : ' ' . $this_arg;
                        $params[$last_arg_name] .= $arg_value;
                    }
                    $last_arg_was = 'value';
                }
            }
        }

        // Pretty up the params array, setting 'true' to TRUE and 'false' to FALSE
        foreach ($params as $param_name => $param_value) {
            if ('false' === $param_value) {
                $params[$param_name] = false;
            } elseif ('true' === $param_value) {
                $params[$param_name] = true;
            } elseif ('null' === $param_value) {
                $params[$param_name] = null;
            } elseif (is_numeric($param_value)) {
                $params[$param_name] = (int) $param_value;
            } elseif (true !== $param_value && false !== $param_value) {
                $params[$param_name] = trim($param_value);
            }
        }

        if (empty($params['method'])) {
            //$params['method'] = $this->_default_method;
            if (isset($params['cmd'])) {
                $params['method'] = $params['cmd'];
                $this->_method    = $params['cmd'];
                // unset($params['cmd']);
            } elseif (isset($params[0])) {
                $this->_method = $params[0];
                unset($params[0]);
            }
        }

        $this->_args = $params;

        $this->set_parsed(true);

        return $this->_args;
    }

    /**
     * @return mixed
     */
    public function parsed_args()
    {
        return $this->_args;
    }

    // BELOW HERE begins CLI user input functions.
    // --------------------------------------------------------------------

    /**
     * Asks the user for input.  This can have either 1 or 2 arguments.
     * // Waits for any key press.
     *
     * @usage $this->prompt();
     *
     * // Takes any input
     * @usage $color = $this->prompt('What is your favorite color?');
     *
     * // Takes any input, but offers default
     * @usage $color = $this->prompt('What is your favourite color?', 'white');
     *
     * // Will only accept the options in the array
     * @usage $ready = $this->prompt('Are you ready?', array('y','n'));
     *
     * @return string the user input
     */
    public function prompt()
    {
        $args    = func_get_args();
        $options = [];
        $output  = '';
        $default = null;

        // How many we got
        $arg_count = count($args);

        // Is the last argument a boolean? True means required
        $required = true === end($args);

        // Reduce the argument count if required was passed, we don't care about that anymore
        (true === $required) and --$arg_count;

        // This method can take a few crazy combinations of arguments, so lets work it out
        switch ($arg_count) {
            case 2:
                // E.g: $ready = CLI::prompt('Are you ready?', array('y','n'));
                if (is_array($args[1])) {
                    list($output, $options) = $args;
                }

                // E.g: $color = CLI::prompt('What is your favourite color?', 'white');
                elseif (is_string($args[1])) {
                    list($output, $default) = $args;
                }

                break;
            case 1:

                // No question (probably been asked already) so just show options
                // E.g: $ready = CLI::prompt(array('y','n'));
                if (is_array($args[0])) {
                    $options = $args[0];
                }

                // Question without options
                // E.g: $ready = CLI::prompt('What did you do today?');
                elseif (is_string($args[0])) {
                    $output = $args[0];
                }

                break;
        }

        // If a question has been asked with the read
        if ('' !== $output) {
            $extra_output = '';
            if (null !== $default) {
                $extra_output = ' [ Default: "' . $default . '" ]';
            } elseif ($options !== []) {
                $extra_output = ' [ ' . implode(', ', $options) . ' ]';
            }

            fwrite(STDOUT, $output . $extra_output . ': ');
        }

        // Read the input from keyboard.
        $input = trim($this->input());
        if ('' == $input) {
            $input = $default;
        }

        // No input provided and we require one (default will stop this being called)
        if (empty($input) and true === $required) {
            $this->write('This is required.');
            $this->new_line();
            $input = call_user_func_array([__CLASS__, 'prompt'], $args);
        }

        // If options are provided and the choice is not in the array, tell them to try again
        if (!empty($options) and !in_array($input, $options)) {
            $this->write('This is not a valid option. Please try again.');
            $this->new_line();
            $input = call_user_func_array([__CLASS__, 'prompt'], $args);
        }

        if (in_array(strtolower($input), ['y', 'yes', 'true'])) {
            $input = true;
        } elseif (in_array(strtolower($input), ['n', 'no', 'false'])) {
            $input = false;
        }

        return $input;
    }

    /**
     * Drop an argument.
     *
     * @param type $name
     */
    public function remove($name)
    {
        if (isset($this->_args[$name])) {
            unset($this->_args[$name]);
        }
    }

    /**
     * Get required CLI arguments.
     *
     * Takes a MadCLI Menu and creates an array of the args that have been
     * presented on the CLI. If an argument marked as 'required' is not found
     * it will show an error along with the help for that arg.
     *
     * @param  array   $req     array of params and description
     * @param  bool    $force
     * @return array
     */
    public function required($req = [], $force = false)
    {
        if (!$force && $this->_required_parsed) {
            return $this->_required_args;
        }

        $this->_req_rules = $req;

        if (0 == count($req)) {
            $this->_required_parsed = true;

            return [];
        }

        // Catch in case they sent the wrong part of the array
        if (isset($this->_req_rules['args'])) {
            $this->_req_rules = $this->_req_rules['args'];
        }

        ksort($this->_req_rules);
        $ret    = [];
        $errors = [];

        $all_args      = $this->parsed_args();
        $required_args = [];

        foreach ($this->_req_rules as $arg_name => $arg_help) {
            $rename  = $options  = $required  = $help  = false;
            $default = NO_DEFAULT;

            // if the arg_help portion is not an array, let's see if it has divider pipes
            if (!is_array($arg_help)) {
                $arg_help = strpos($arg_help, '|') !== false ? explode('|', $arg_help) : [$arg_help];
            }

            // now, this section will auto decide what the different arrays contain
            // but before we do that, we need to split off the first element if
            // it contains the arg_name.
            if (is_numeric($arg_name)) {
                unset($this->_req_rules[$arg_name]);
                $arg_name                    = isset($arg_help['name']) ? $arg_help['name'] : array_shift($arg_help);
                $this->_req_rules[$arg_name] = $arg_help;
            }

            $_default      = ['required' => false, 'default' => false, 'help' => false, 'options' => false, 'rename' => false];
            $_default_keys = array_keys($_default);
            if (is_array($arg_help) && count($arg_help) > 0) {
                foreach ($arg_help as $k => $v) {
                    $fn = is_numeric($k) && isset($_default_keys[$k]) ? $_default_keys[$k] : $k;
                    if (isset($_default[$fn])) {
                        $_default[$fn] = $v;
                        unset($arg_help[$k]);
                    }
                }

                if (is_numeric($_default['required'])) {
                    $_default['required'] = (bool) $_default['required'];
                }
                if ($_default['default'] === 'false') {
                    $_default['default'] = false;
                } elseif ($_default['default'] === 'true') {
                    $_default['default'] = true;
                } elseif (is_numeric($_default['default'])) {
                    $_default['default'] = (int) $_default['default'];
                }
            }

            extract($_default);

            $this->_req_rules[$arg_name] = $_default;

            // Allow for more than one --param_name pointing to a single final param.
            $possible_names = explode('|', $arg_name);
            foreach ($possible_names as $pn) {
                if (($check = $this->get($pn))) {
                    break;
                }
            }

            if ($check === false && $default !== NO_DEFAULT) {
                $check = $default;
            }

            // Check for allowed options
            if (
                ($required === false)
                ||
                ($required === true && ($check !== false || $check === $default))
            ) {
                if ($rename && $arg_name != $rename) {
                    $arg_name = $rename;
                }

                $this->_req_rules_passed[$arg_name] = $this->_req_rules[$arg_name];
                unset($this->_req_rules[$arg_name]);
                if (count($possible_names) > 0) {
                    foreach ($possible_names as $pn) {
                        $this->_args[$pn] = $this->_required_args[$pn] = $check;
                    }
                } else {
                    $this->_args[$arg_name] = $this->_required_args[$arg_name] = $check;
                }
            }
        }

        if (count($this->_req_rules) > 0) {
            return ['error' => $this->_req_rules];
        }

        $this->_required_parsed = true;

        return $this->_required_args;
    }

    public function route()
    {
        return [$this->controller(), $this->method()];
    }

    /**
     * Add/update an arg value.
     *
     * @param type $name
     * @param type $value
     */
    public function set($name, $value)
    {
        $this->_args[$name] = $value;
    }

    /**
     * @param $text
     */
    public function status_line($text = '')
    {
        if ($this->_prior_status_len) {
            $this->erase($this->_prior_status_len);
        }
        $this->_prior_status_len = strlen($text);
        $this->write($text);
    }

    /**
     * Write to STDOUT.
     *
     * @param type $text
     */
    public function write($text = '')
    {
        if (is_array($text)) {
            $text = implode(PHP_EOL, $text);
        }
        fwrite(STDOUT, $text . PHP_EOL);
    }

    /**
     * @param $value
     */
    private function set_parsed($value = true)
    {
        $this->_parsed = $value;
    }
}

/**
 * Create a single arg instance for all cron scripts.
 *
 * @staticvar Args $arg_obj
 * @return \Args
 */
function &get_args()
{
    /**
     * @var mixed
     */
    static $arg_obj;
    if (empty($arg_obj)) {
        $arg_obj = new Args();
    }

    return $arg_obj;
}

/**
 * Find an argument based on key.
 * Search the argument array generated by parse_args() as well as GET/POST;
 *
 * @param  string $arg     the argument name/index
 * @param  mixed  $default value to return if $arg is nto found
 * @return mixed  the requested argument if found, false if not
 */
function get_arg($arg = false, $default = false)
{
    $arg_obj = &get_args();

    return $arg_obj->get($arg, $default);
}

function get_route()
{
    $arg_obj = &get_args();

    return [$arg_obj->action(), $arg_obj->method()];
}

/**
 * Parses out the arguments passed in $argv.
 *
 * @param  array $argv   command line arguments
 * @param  mixed $args
 * @return array Array of flags
 */
function parse_args($args = false)
{
    $arg_obj = &get_args();

    return $arg_obj->parse_args($args);
}

/**
 * Take a parameter list and populate the arg array with defaults, and other
 * things to mnake it more useful.
 *
 * @param  type   $required
 * @param  type   $force
 * @return type
 */
function required_args($required = [], $force = false)
{
    $arg_obj = &get_args();

    return $arg_obj->required($required, $force);
}
