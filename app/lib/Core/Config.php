<?php namespace Mad\Core;

use \Mad\Config\Repository;

class Config
{
    /**
     * @var array
     */
    protected $_filters = ['Constants.php'];

    /**
     * @var array
     */
    protected static $_items = [];

    /**
     * @var mixed
     */
    protected $loader;

    /**
     * @param \Mad\Core\Loader $loader
     */
    public function __construct(\Mad\Core\Loader $loader)
    {
        $this->loader = $loader;
        $this->initialize();
    }

    /**
     * @param $key
     * @param $values
     */
    public static function create($key, $values)
    {
        return static::$_items[$key] = new Repository($values);
    }

    /**
     * @param $key
     */
    public static function get($key = null)
    {
        $key = strtolower($key);

        if ($key === null) {
            return static::$_items;
        } elseif (isset(static::$_items[$key])) {
            return static::$_items[$key];
        } else {
            throw new \Exception("Config Repository '$key' not found.");
        }
    }

    public function initialize()
    {
        $configs = $this->loader->findConfigs();
        foreach ($configs as $k => $v) {
            $basename = basename($k);
            $no_ext   = substr($basename, 0, -4);
            $key      = strtolower($no_ext);
            if (in_array($basename, $this->_filters)) {
                continue;
            }

            print "loading $k ..." . NL;
            $ret = include $k;
            if ($ret) {
                if (is_array($ret)) {
                    $this->$key = static::create($key, $ret);
                } else {
                    print "$k is an unknown file, let's try reading it.";
                    exit;
                }
            }
        }
    }
}
