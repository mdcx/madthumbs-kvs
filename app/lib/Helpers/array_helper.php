<?php

/**
 * Sort an associative array on a specified key.
 *
 * @param  array     $array The array to sort
 * @param  string    $on    The field to sort on
 * @param  php_order $order Default SORT_ASC
 * @return array     the array sorted on $on, while maintaining key assocations
 */
function array_sort($array, $on, $order = SORT_ASC)
{
    $new_array      = [];
    $sortable_array = [];

    $find_value = function ($obj, $property) {
        if (false !== strpos($property, '->')) {
            $parts = explode('->', $property);
            foreach ($parts as $pn) {
                if (isset($obj->$pn)) {
                    $obj = $obj->$pn;
                } else {
                    die("Value $on not found on object." . NL);
                }
            }
        }

        return $obj;
    };

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } elseif (is_object($v)) {
                $sortable_array[$k] = $find_value($v, $on);
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);

                break;
            case SORT_DESC:
                arsort($sortable_array);

                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}


if (!function_exists('array_value_dot')) {

    /**
     * Searches an array through dot syntax. Supports
     * wildcard searches, like foo.*.bar
     *
     * @param  string       $index
     * @param  array        $array
     * @return mixed|null
     */
    function dot_array_search($index, $array)
    {
        $segments = explode('.', rtrim(rtrim($index, '* '), '.'));

        return _array_search_dot($segments, $array);
    }

}

if (!function_exists('array_search_dot')) {

    /**
     * Used by array_value_dot to recursively search the
     * array with wildcards.
     *
     * @param  array        $indexes
     * @param  array        $array
     * @return mixed|null
     */
    function _array_search_dot($indexes, $array)
    {
        // Grab the current index
        $currentIndex = count($indexes) ? array_shift($indexes) : null;

        if (empty($currentIndex) || (!isset($array[$currentIndex]) && $currentIndex != '*')) {
            return null;
        }

        // Handle Wildcard (*)
        if ($currentIndex == '*') {
            // If $array has more than 1 item, we have to loop over each.
            if (is_array($array)) {
                foreach ($array as $key => $value) {
                    $answer = _array_search_dot($indexes, $value);

                    if ($answer !== null) {
                        return $answer;
                    }
                }

                // Still here after searching all child nodes?

                return null;
            }
        }

        // If this is the last index, make sure to return it now,
        // and not try to recurse through things.
        if (empty($indexes)) {
            return $array[$currentIndex];
        }

        // Do we need to recursively search this value?
        if (is_array($array[$currentIndex]) && count($array[$currentIndex])) {
            return _array_search_dot($indexes, $array[$currentIndex]);
        }

        // Otherwise we've found our match!

        return $array[$currentIndex];
    }

}

/**
 * Go through multiple levels of an array looking for $column_key
 * @param  array   $input      The array to search
 * @param  type    $column_key The key to look for
 * @param  type    $index_key  The key to use for found values.
 * @return array
 */
function array_column_recursive($input, $column_key, $index_key = false)
{
    $found = [];
    array_walk_recursive($input, function ($value, $key) use (&$found, $column_key, $index_key) {
        if ($key == $column_key) {
            if ($index_key) {
                $found[$index_key] = $value;
            }
        }

        $found[] = $value;
    });

    return $found;
}

/**
 * Grab 1 or more columns from a multi-dimensional array
 *
 * @param  $input       The input array
 * @param  $column_keys An  array of column keys to retrieve
 * @return array        A multi-dimensional array with only vthe values for @param $column_keys
 */
function array_column_multi($input, $column_keys)
{
    $result = [];
    if (!is_array($column_keys)) {
        $column_keys = [$column_keys];
    }

    $column_keys = array_flip($column_keys);
    foreach ($input as $key => $el) {
        $result[$key] = array_intersect_key($el, $column_keys);
    }

    return $result;
}

/**
 * Filter an array down to an array containing only keys that you specify via search.
 *
 * Keys starting with filter = filter*
 * Keys containing word = *word*
 * Keys ending in word = *word
 *
 * @param  array   $array  The array to filter
 * @param  string  $needle The key string to find
 * @param  boolean $case   Whether search should be case sensitive. (default: true)
 * @return array
 */
function array_key_filter(&$array, $needle, $case = true)
{
    if ($case == false) {
        $needle = strtolower($needle);
    }
    $last   = substr($needle, -1);
    $ret    = [];
    $search = str_replace('*', '', $needle);
    foreach ($array as $k => $v) {
        $check = $k;
        if (!$case) {
            $check = strtolower($check);
        }

        if ($needle[0] == '*' && $last == '*' && strpos($check, $search) !== false) {
            $ret[$k] = $v;
        } elseif ($needle[0] == '*' && $last != '*' && (substr($check, -(strlen($search))) == $search)) {
            $ret[$k] = $v;
        } elseif ($needle[0] != '*' && $last == '*' && substr($check, 0, strlen($search)) == $search) {
            $ret[$k] = $v;
        }

    }

    return $ret;
}

/**
 * @param  $array
 * @param  $search
 * @param  $case
 * @param  FALSE     $return_keys
 * @return mixed
 */
function array_search_partial(&$array, $search, $case = false, $return_keys = true)
{
    $matches = [];
    $func    = $case == false ? "stripos" : "strpos";

    foreach ($array as $key => $value) {
        if ($func($value, $search) !== false) {
            $matches[] = $return_keys ? $key : $value;
        }
    }

    if (count($matches) > 0) {
        return $matches;
    } else {
        return false;
    }

}
