<?php
/**
 * @param  $headers
 * @return mixed
 */
function parse_headers($headers)
{
    preg_match_all("/(.+):(.+)(?:(?<![\t ])" . "\r\n" . '|$)/Uis', $headers, $matches, PREG_SET_ORDER);
    $header = array();
    foreach ($matches as $match) {
        list($null, $field, $value) = $match;

        $value = trim($value);
        $value = preg_replace("/[\t ]\r\n/", "\r\n", $value);
        $field = strtolower($field);
        preg_match_all('/(?:^|(?<=-))[a-z]/U', $field, $offsets, PREG_OFFSET_CAPTURE);
        foreach ($offsets[0] as $offset) {
            $field = substr_replace($field, strtoupper($offset[0]), $offset[1], 1);
        }

        if (!isset($header[$field])) {
            $header[$field] = $value;
        } else {
            $header[$field] = array_merge((array) $header[$field], (array) $value);
        }
    }

    return $header;
}

/**
 * @param  $location
 * @param  $filename
 * @param  $mimeType
 * @return null
 */
function smartReadFile($location, $filename, $mimeType = 'application/octet-stream')
{
    if (!file_exists($location)) {
        header("HTTP/1.0 404 Not Found");

        return;
    }

    $size = filesize($location);
    $time = date('r', filemtime($location));

    $fm = @fopen($location, 'rb');
    if (!$fm) {
        header("HTTP/1.0 505 Internal server error");

        return;
    }

    $begin = 0;
    $end   = $size;

    if (isset($_SERVER['HTTP_RANGE'])) {
        if (preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches)) {
            $begin = intval($matches[1]);
            if (!empty($matches[2])) {
                $end = intval($matches[2]);
            }

        }
    }

    if ($begin > 0 || $end < $size) {
        header('HTTP/1.0 206 Partial Content');
    } else {
        header('HTTP/1.0 200 OK');
    }

    header("Content-Type: $mimeType");
    header('Cache-Control: public, must-revalidate, max-age=0');
    header('Pragma: no-cache');
    header('Accept-Ranges: bytes');
    header('Content-Length:' . ($end - $begin));
    header("Content-Range: bytes $begin-$end/$size");
    header("Content-Disposition: inline; filename=$filename");
    header("Content-Transfer-Encoding: binary\n");
    header("Last-Modified: $time");
    header('Connection: close');

    $cur = $begin;
    fseek($fm, $begin, 0);

    while (!feof($fm) && $cur < $end && (connection_status() == 0)) {
        print fread($fm, min(1024 * 16, $end - $cur));
        $cur += 1024 * 16;
    }
}

/**
 * curl_grab.
 *
 * @param  string $url       The url to grab
 * @param  string $cookiesIn A list of cookies from a prior curl request
 * @param  array  $p         Any curl opts you wish to override
 * @return array  the headers from the request
 */
function curl_grab($url, $cookiesIn = '', $p = array())
{
    /**
     * @var array A set of pre-configured curl opts
     */
    $sets = array(
        'raw_gz' => array(
            CURLOPT_ENCODING => 'gzip,deflate',
            // CURLOPT_HTTP_CONTENT_DECODING => false,
        ),
    );

    /**
     * @var array $options
     */
    $options = array(
        CURLOPT_AUTOREFERER    => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,  // timeout on connect
        CURLOPT_COOKIE         => $cookiesIn,
        CURLOPT_ENCODING       => '',   // handle all encodings
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_HEADER         => true, //return headers in addition to content
        CURLINFO_HEADER_OUT    => true,
        CURLOPT_HTTPHEADER     => array(),
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_MAXREDIRS      => 5,     // stop after 10 redirects
        CURLOPT_NOBODY         => false, // don't return anything other than head
        CURLOPT_RETURNTRANSFER => true,  // return web page
        CURLOPT_SSL_VERIFYPEER => false, // Disabled SSL Cert checks
        CURLOPT_TIMEOUT        => 120,   // timeout on response
        CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)',
    );

    $return = 'all';

    $other = array(
        'return' => 'all',
    );

    if (isset($p['shortcut'], $sets[$p['shortcut']])) {
        foreach ($sets[$p['shortcut']] as $k => $v) {
            $p[$k] = $v;
        }
        unset($p['shortcut']);
    }

    if (count($p) > 0) {
        foreach ($p as $k => $v) {
            if (isset($options[$k])) {
                $options[$k] = $v;
            } else {
                $other[$k] = $v;
            }
        }
    }

    extract($other);

    if (isset($options[CURLOPT_COOKIEFILE]) && '' == $options[CURLOPT_COOKIE]) {
        unset($options[CURLOPT_COOKIE]);
    }

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $rough_content = curl_exec($ch);
    $err           = curl_errno($ch);
    $errmsg        = curl_error($ch);
    $header        = curl_getinfo($ch);
    curl_close($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content   = trim(str_replace($header_content, '', $rough_content));
    $pattern        = '#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m';
    preg_match_all($pattern, $header_content, $matches);
    $cookiesOut = implode('; ', $matches['cookie']);

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['headers'] = parse_headers($header_content);
    $header['content'] = $body_content;
    $header['cookies'] = $cookiesOut;
    if ('all' != $return) {
        if (isset($header[$return])) {
            return $header[$return];
        }

        return false;
    }

    return $header;
}
