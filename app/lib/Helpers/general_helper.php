<?php
/**
 * Manage getting advisory locks on a specific file.
 *
 *                               If 'task-9', it uses it to generate a lock file path
 *                               If 'videos/task-9', it will put the file in a folder
 *                               If /videos/task-9' it uses the value as an abs path
 * @param  bool|string     $lock_id If false, unlocks all known locks created by the function.
 * @param  int             $wait    If lock fails, wait this many seconds and try again.
 * @param  bool            $debug   If true, echo some debug messages.
 * @return bool|resource
 */
function lock_manager($lock_id = false, $wait = 0, $debug = false)
{
    global $config;
    /**
     * @var mixed
     */
    static $_locks;
    if (empty($_locks)) {
        $_locks = [];
    }

    if ($lock_id === false) {
        foreach ($_locks as $lock_id => $lock) {
            lock_manager($lock_id, 0, $debug);
        }

        return true;
    }

    $lock_dir = "$config[project_path]/admin/data/system/locks/";
    if (strpos($lock_id, '/') !== false) {
        $parts   = explode('/', $lock_id);
        $lock_id = array_pop($parts);
        $lock_dir .= implode('/', $parts) . '/';
    }
    $lock_file = $lock_dir . rtrim($lock_id, '.lock') . '.lock';

    // if lock exists, let's unlock it.
    if (isset($_locks[$lock_id])) {
        if (is_resource($_locks[$lock_id])) {
            $unlock = flock($_locks[$lock_id], LOCK_UN);
            if (!$unlock) {
                if ($debug) {
                    log_output("[locker] Failed to unlock lock named $lock_id...");
                }

                return false;
            }
            fclose($_locks[$lock_id]);
            unlink($lock_file);
            unset($_locks[$lock_id]);
            if ($debug) {
                log_output("[locker] Unlocked lock for $lock_id ...");
            }

            return true;
        }
        if ($debug) {
            log_output("[locker] $lock_id doesn't have a valid file pointer.");
        }

        return false;
    }
    // attempt to create new lock
    else {
        $fp = fopen($lock_file, 'c');
        if (!$fp) {
            if ($debug) {
                log_output("[locker] Failed to open/create lock file: $lock_file");
            }

            return false;
        }

        if (!($lock = flock($fp, LOCK_EX | LOCK_NB))) {
            if ($debug) {
                log_output("[locker] Failed to obtain lock: $lock_file");
            }

            return false;
        } else {
            if ($debug) {
                log_output("[locker] Obtained lock for $lock_file");
            }

            fwrite($fp, getmypid());

            return $_locks[$lock_id] = $fp;
        }
    }

    return false;
}

/**
 * @param $var
 */
function vdd($var)
{

    $gc = get_caller();
    msg('var dump', get_caller());
    var_dump($var);
    die;
}

/**
 * @param $var
 */
function vd($var)
{
    $gc = get_caller();
    msg("var_dump", $gc);
    var_dump($var);
}

/**
 * Is CLI?
 *
 * Test to see if a request was made from the command line.
 *
 * @return bool
 */
function is_cli()
{
    return (PHP_SAPI === 'cli' || defined('STDIN'));
}
/**
 * Get Caller
 * Find out which function or functions called the func/include/class you are
 * currently in.
 *
 * @param  type   $filter Filter out calls containing $filter
 * @param  type   $short  Only return the closest func to the current.
 * @return type
 */
function get_caller($filter = false, $short = true)
{
    $default_filters = ['require_once', 'include_once', 'require', 'include', 'get_caller', 'msg', 'vd', 'vdd'];
    if (!$filter || $filter == '') {
        $filter = [];
    }
    if (!is_array($filter)) {
        $filter = [$filter];
    }
    $filter = array_merge($default_filters, $filter);
    $trace  = array_reverse(debug_backtrace());
    $caller = [];

    foreach ($trace as $call) {
        $debug_str = isset($call['class']) ? "{$call['class']}->{$call['function']}" : $call['function'];

        if (isset($call['line'])) {
            $debug_str .= ":{$call['line']}";
        }

        if (strpos_array($debug_str, $filter) !== false) {
            continue;
        }
        $caller[] = $debug_str;
    }
    if (count($caller) === 0) {
        $inc      = get_included_files();
        $main     = array_shift($inc);
        $main_d   = explode('/', dirname($main));
        $main_f   = basename($main);
        $caller[] = array_pop($main_d) . '/' . $main_f;
    }
    if ($short) {
        if ($short === true) {
            return array_pop($caller);
        } else {
            $caller = array_slice($caller, -$short);
        }
    }

    return join(', ', $caller);
}

function getmicrotime()
{
    list($usec, $sec) = explode(' ', microtime());

    return (float) $usec + (float) $sec;
}

/**
 * Remnove CDATA from xml element.
 *
 * @param  type   $string
 * @return type
 */
function strip_cdata($string)
{
    preg_match_all('/<!\[cdata\[(.*?)\]\]>/is', $string, $matches);

    return str_replace($matches[0], $matches[1], $string);
}

/**
 * Slash String
 * Return the $str with a right slash added if none existed.
 *
 * @param  type   $str
 * @return type
 */
function slashr($str)
{
    return rtrim($str, '/') . '/';
}
/**
 * Return str with leading slash if none existed.
 *
 * @param  string   $str
 * @return string
 */
function slashl($str)
{
    return '/' . ltrim($str, '/');
}
/**
 * Return str with leading/trailing slash if none existed.
 *
 * @param  string   $str
 * @return string
 */
function slashrl($str)
{
    return '/' . trim($str, '/') . '/';
}

/**
 * Checks if a process is currently running. used to prevent more than 1
 * copy of a script from starting.
 *
 * @param  type            $procn
 * @return boolean|array
 */
function process_running($procn)
{
    $out       = [];
    $pid       = getmypid();
    $check_cmd = 'ps auxwww | egrep ' . escapeshellarg($procn) . ' | grep -v "' . $pid . '" | grep -v "grep" | grep -v "sh"';

    exec($check_cmd, $out);

    if (count($out) > 0) {
        return true;
    }

    return false;
}
/**
 * Check a string for an array of values ..
 *
 * @param  string  $haystack The string to search
 * @param  array   $needles  An array of values to match
 * @return boolean if a match was made
 */
function strpos_array($haystack, $needles)
{

    if (is_array($needles)) {
        foreach ($needles as $needle) {
            $pos = is_array($needle) ? strpos_array($haystack, $needle) : strpos($haystack, $needle);
            if ($pos !== false) {
                return $pos;
            }
        }

        return false;
    }

    return strpos($haystack, $needles);
}
