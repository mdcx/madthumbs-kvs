<?php

/**
 * Convert a size string to bytes.
 *
 * @param  mixed $size
 * @return int
 */
function size_to_bytes($size, $si = false)
{
    return rbytes($size, $si);
}

/**
 * Convert bytes to a specific factor.
 *
 * @param  int   $bytes
 * @param  mixed $force_unit
 * @return array [$num, $suffix]
 */
function bytes_to_size($bytes, $force_unit = null)
{
    return explode(' ', bytes($bytes, $force_unit));
}

/**
 * Takes a human readable size and converts it to bytes.
 *
 * @see  Aidan Lister: http://aidanlister.com/repos/v/function.size_readable.php
 *
 * @param  string    size Accepts: 20M, 20MiB, 20MB as formats.
 * @param  boolean   $si  whether to use SI prefixes or IEC
 * @return integer
 */
function rbytes($size, $si = true)
{
    // IEC prefixes (binary)
    if ($si == false || strpos($size, 'i') !== false) {
        $units = ['B', 'Ki?B?', 'Mi?B?', 'Gi?B?', 'Ti?B?', 'Pi?B?'];
        $mod   = 1024;
    }
    // SI prefixes (decimal)
    else {
        $units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
        $mod   = 1000;
    }

    for ($i = count($units) - 1; $i >= 0; $i--) {
        $m = preg_match('/(' . $units[$i] . ')/i', $size, $match);
        if ($m) {
            $size_raw = trim(preg_replace('/' . $units[$i] . '/i', '', $size));

            return $size_raw * pow($mod, $i > 0 ? $i : 1);
        }
    }
    echo get_caller();
    die("Not well formed size: '$size'\n");
}

/**
 * Returns human readable sizes.
 * @see  Based on original functions written by:
 * @see  Aidan Lister: http://aidanlister.com/repos/v/function.size_readable.php
 * @see  Quentin Zervaas: http://www.phpriot.com/d/code/strings/filesize-format/
 *
 * @param  integer  size    in bytes
 * @param  string   a       definitive unit
 * @param  string   the     return string format
 * @param  boolean  whether to use SI prefixes or IEC
 * @return string
 */
function bytes($bytes, $force_unit = null, $format = '%01.2f %s', $si = true)
{
    $force_unit = (string) $force_unit;
    $units      = [
        'bin' => ['mod' => 1024, 'suffixes' => ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB']],
        'si'  => ['mod' => 1000, 'suffixes' => ['B', 'kB', 'MB', 'GB', 'TB', 'PB']],
    ];

    extract($si === false || strpos($force_unit, 'i') !== false ? $units['bin'] : $units['si']);

    // Determine unit to use
    if (($power = array_search($force_unit, $suffixes)) === false) {
        $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
    }

    return sprintf($format, $bytes / pow($mod, $power), $suffixes[$power]);
}
\



/**
 * Formats a numbers as bytes, based on size, and adds the appropriate suffix
 *
 * @param  mixed    $num         Will be cast as int
 * @param  int      $precision
 * @param  string   $locale
 * @return string
 */
function number_to_size($num, $precision = 1, $locale = null)
{
    // Strip any formatting
    $num = 0 + str_replace(',', '', $num);

    // Can't work with non-numbers...
    if (!is_numeric($num)) {
        return false;
    }

    if ($num >= 1000000000000) {
        $num  = round($num / 1099511627776, $precision);
        $unit = lang('Number.terabyteAbbr');
    } elseif ($num >= 1000000000) {
        $num  = round($num / 1073741824, $precision);
        $unit = lang('Number.gigabyteAbbr');
    } elseif ($num >= 1000000) {
        $num  = round($num / 1048576, $precision);
        $unit = lang('Number.megabyteAbbr');
    } elseif ($num >= 1000) {
        $num  = round($num / 1024, $precision);
        $unit = lang('Number.kilobyteAbbr');
    } else {
        $unit = lang('Number.bytes');
    }

    return format_number($num, $precision, $locale, ['after' => ' ' . $unit]);
}

//--------------------------------------------------------------------

/**
 * Converts numbers to a more readable representation
 * when dealing with very large numbers (in the thousands or above),
 * up to the quadrillions, because you won't often deal with numbers
 * larger than that.
 *
 * It uses the "short form" numbering system as this is most commonly
 * used within most English-speaking countries today.
 *
 * @see https://simple.wikipedia.org/wiki/Names_for_large_numbers
 *
 * @param  string         $num
 * @param  int            $precision
 * @param  string|null    $locale
 * @return bool|string
 */
function number_to_amount($num, $precision = 0, $locale = null)
{
    // Strip any formatting
    $num = 0 + str_replace(',', '', $num);

    // Can't work with non-numbers...
    if (!is_numeric($num)) {
        return false;
    }

    $suffix = '';

    if ($num > 1000000000000000) {
        $suffix = lang('Number.quadrillion');
        $num    = round(($num / 1000000000000000), $precision);
    } elseif ($num > 1000000000000) {
        $suffix = lang('Number.trillion');
        $num    = round(($num / 1000000000000), $precision);
    } else if ($num > 1000000000) {
        $suffix = lang('Number.billion');
        $num    = round(($num / 1000000000), $precision);
    } else if ($num > 1000000) {
        $suffix = lang('Number.million');
        $num    = round(($num / 1000000), $precision);
    } else if ($num > 1000) {
        $suffix = lang('Number.thousand');
        $num    = round(($num / 1000), $precision);
    }

    return format_number($num, $precision, $locale, ['after' => $suffix]);
}

//--------------------------------------------------------------------

/**
 * @param  float    $num
 * @param  string   $currency
 * @param  string   $locale
 * @return string
 */
function number_to_currency($num, $currency, $locale = null)
{
    return format_number($num, 1, $locale, [
        'type'     => NumberFormatter::CURRENCY,
        'currency' => $currency,
    ]);
}

//--------------------------------------------------------------------

if (!function_exists('format_number')) {

    /**
     * A general purpose, locale-aware, number_format method.
     * Used by all of the functions of the number_helper.
     *
     * @param  float       $num
     * @param  int         $precision
     * @param  string|null $locale
     * @param  array       $options
     * @return string
     */
    function format_number($num, $precision = 1, $locale = null, $options = [])
    {
        // Locale is either passed in here, negotiated with client, or grabbed from our config file.
        $locale = $locale ?  ? \CodeIgniter\Config\Services::request()->getLocale();

        // Type can be any of the NumberFormatter options, but provide a default.
        $type = isset($options['type']) ? (int) $options['type'] :
        NumberFormatter::DECIMAL;

        // In order to specify a precision, we'll have to modify
        // the pattern used by NumberFormatter.
        $pattern = '#,##0.' . str_repeat('#', $precision);

        $formatter = new NumberFormatter($locale, $type);

        // Try to format it per the locale
        if ($type == NumberFormatter::CURRENCY) {
            $output = $formatter->formatCurrency($num, $options['currency']);
        } else {
            $formatter->setPattern($pattern);
            $output = $formatter->format($num);
        }

        // This might lead a trailing period if $precision == 0
        $output = trim($output, '. ');

        if (intl_is_failure($formatter->getErrorCode())) {
            throw new BadFunctionCallException($formatter->getErrorMessage());
        }

        // Add on any before/after text.
        if (isset($options['before']) && is_string($options['before'])) {
            $output = $options['before'] . $output;
        }

        if (isset($options['after']) && is_string($options['after'])) {
            $output .= $options['after'];
        }

        return $output;
    }

}

//--------------------------------------------------------------------

/**
 * Convert a number to a roman numeral.
 *
 * @param  int      $num it will convert to int
 * @return string
 */
function number_to_roman($num)
{
    $num = (int) $num;
    if ($num < 1 or $num > 3999) {
        return;
    }

    $_number_to_roman = function ($num, $th) use (&$_number_to_roman) {
        $return = '';
        $key1   = null;
        $key2   = null;
        switch ($th) {
            case 1 :
                $key1  = 'I';
                $key2  = 'V';
                $key_f = 'X';
                break;
            case 2 :
                $key1  = 'X';
                $key2  = 'L';
                $key_f = 'C';
                break;
            case 3:
                $key1  = 'C';
                $key2  = 'D';
                $key_f = 'M';
                break;
            case 4:
                $key1 = 'M';
                break;
        }
        $n = $num % 10;
        switch ($n) {
            case 1:
            case 2:
            case 3:
                $return = str_repeat($key1, $n);
                break;
            case 4:
                $return = $key1 . $key2;
                break;
            case 5:
                $return = $key2;
                break;
            case 6:
            case 7:
            case 8:
                $return = $key2 . str_repeat($key1, $n - 5);
                break;
            case 9:
                $return = $key1 . $key_f;
                break;
        }
        switch ($num) {
            case 10:
                $return = $key_f;
                break;
        }
        if ($num > 10) {
            $return = $_number_to_roman($num / 10, ++$th) . $return;
        }

        return $return;
    };

    return $_number_to_roman($num, 1);
}
