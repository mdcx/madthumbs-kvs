<?php
if ( ! function_exists('now'))
{

    /**
     * Get "now" time
     *
     * Returns time() based on the timezone parameter or on the
     * app_timezone() setting
     *
     * @param    string $timezone
     *
     * @return    int
     */
    function now($timezone = null)
    {
        $timezone = empty($timezone) ? app_timezone() : $timezone;

        if ($timezone === 'local' || $timezone === date_default_timezone_get())
        {
            return time();
        }

        $datetime = new DateTime('now', new DateTimeZone($timezone));
        sscanf($datetime->format('j-n-Y G:i:s'), '%d-%d-%d %d:%d:%d', $day, $month, $year, $hour, $minute, $second);

        return mktime($hour, $minute, $second, $month, $day, $year);
    }

}


/**
 * Pretty Date Diff
 * Takes the result of DATE1 - DATE2 and turns it into a human readble string
 * or array. For example: $ts = 453534 would be 1 week, 3 days, 2 mins (or something)
 *
 * @param  int   $seconds The numnber of seconds to diff
 * @param  bool  $str     Return it as a pretty textual string?
 * @return array Array containing d/h/m/s
 */
function pretty_diff($seconds, $str = true, $precision = 'all')
{
    if (is_numeric($seconds)) {
        $seconds = (int) $seconds;
    } else {
        $seconds = strtotime($seconds);
        $seconds = time() - $seconds;
    }

    $rem = $seconds;
    $ret = [
        'y' => 0,
        'w' => 0,
        'd' => 0,
        'h' => 0,
        'm' => 0,
        's' => 0,
    ];

    $count = 0;
    $rstr  = '';

    $str_part = [];
    while ($rem > 0) {
        switch ($rem) {
            // seconds
            case $rem < 60:
                $ret['s']  = $rem;
                $ret['s']  = round($ret['s'], 2);
                $rem       = 0;
                $this_part = $ret['s'] . " second";
                if ($ret['s'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
                break;
            // minutes
            case $rem < 3600:
                $tmp       = $rem / 60; // 1.65
                $ret['m']  = floor($tmp);
                $rem       = $rem - ($ret['m'] * 60);
                $this_part = $ret['m'] . " minute";
                if ($ret['m'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
                break;
            // this means it's less than days etc.
            case $rem < 86400:
                $tmp       = $rem / 3600;
                $ret['h']  = floor($tmp);
                $rem       = $rem - ($ret['h'] * 3600);
                $this_part = $ret['h'] . " hour";
                if ($ret['h'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
                break;
            // days
            case $rem < 604800:
                $ret['d']  = floor($rem / 86400);
                $rem       = $rem - ($ret['d'] * 86400);
                $this_part = $ret['d'] . " day";
                if ($ret['d'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;

                break;
            // weeks
            case $rem < 31449600:
                $ret['w']  = floor($rem / 604800);
                $rem       = $rem - ($ret['w'] * 604800);
                $this_part = $ret['w'] . " week";
                if ($ret['w'] > 1) {
                    $this_part .= "s";
                }
                $str_part[] = $this_part;
            // no break
            default:
                $ret['y'] = floor($rem / (52 * 604800));
                $rem -= $ret['y'] * (52 * 604800);
                $this_part = $ret['y'] . ' year';
                if ($ret['y'] > 1) {
                    $this_part .= 's';
                }

                $str_part[] = $this_part;
        }
        $count++;
        if ($count > 10) {
            break;
        }
    }

    // 5 days 3 hours 10 mintes 5 sec
    if ($str != false) {
        if ($precision != 'all' && is_numeric($precision)) {
            $precision = max([$precision, count($str_part)]);
            $str_part  = array_slice($str_part, 0, $precision);
        }

        return implode(' ', $str_part);
    } else {
        return $ret;
    }
}

function hms_to_secs($time) {
    sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
    $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
    return $time_seconds;
}

/**
 * Convert seconds to a DD:HH:MM:SS timestamp (array format);
 * @param  int   $int
 * @return array Array of days, hours, mins, secs
 */
function secs_to_dhms($int)
{
    $format = '%h:%m:%s';

    if ($int <= 0) {
        return false;
    }

    $days   = (($int / 60) / 60) / 24;
    $days_r = $days - floor($days);
    $days   = $days - $days_r;

    $hours   = $days_r * 24;
    $hours_r = $hours - floor($hours);
    $hours   = $hours - $hours_r;

    $mins   = $hours_r * 60;
    $mins_r = $mins - floor($mins);
    $mins   = $mins - $mins_r;

    $secs = $mins_r * 60;

    $days  = sprintf('%02d', $days);
    $hours = sprintf('%02d', $hours);
    $mins  = sprintf('%02d', $mins);
    $secs  = sprintf('%02d', $secs);

    $p    = $r    = array();
    $p[0] = '/%d/';
    $p[1] = '/%h/';
    $p[2] = '/%m/';
    $p[3] = '/%s/';

    $r[0] = $days;
    $r[1] = $hours;
    $r[2] = $mins;
    $r[3] = $secs;

    $dhms = preg_replace($p, $r, $format);

    return compact('dhms', 'days', 'hours', 'mins', 'secs');
}


function hms_to_secs($time) {
    sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
    $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
    return $time_seconds;
}

