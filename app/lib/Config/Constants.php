<?php

defined('SEP[') OR define('SEP', str_repeat('-', 75) . "\n");
defined('NL') OR define('NL', empty($_SERVER['REMOTE_ADDR']) ? "\n" : "<br/>\n");

defined('SECOND') || define('SECOND', 1);
defined('MINUTE') || define('MINUTE', 60);
defined('HOUR') || define('HOUR', 3600);
defined('DAY') || define('DAY', 86400);
defined('WEEK') || define('WEEK', 604800);
defined('MONTH') || define('MONTH', 2592000);
defined('YEAR') || define('YEAR', 31536000);
defined('DECADE') || define('DECADE', 315360000);

defined('KB_FACTOR') || define('KB_FACTOR', 1024);
defined('MB_FACTOR') || define('MB_FACTOR', KB_FACTOR * 1024);
defined('GB_FACTOR') || define('GB_FACTOR', MB_FACTOR * 1024);
defined('TB_FACTOR') || define('TB_FACTOR', GB_FACTOR * 1024);
defined('ROUND_FACTOR') || define('ROUND_FACTOR', 3);

defined('EXIT_SUCCESS') || define('EXIT_SUCCESS', 0);               // no errors
defined('EXIT_ERROR') || define('EXIT_ERROR', 1);                   // generic error
defined('EXIT_CONFIG') || define('EXIT_CONFIG', 3);                 // configuration error
defined('EXIT_UNKNOWN_FILE') || define('EXIT_UNKNOWN_FILE', 4);     // file not found
defined('EXIT_UNKNOWN_CLASS') || define('EXIT_UNKNOWN_CLASS', 5);   // unknown class
defined('EXIT_UNKNOWN_METHOD') || define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') || define('EXIT_USER_INPUT', 7);         // invalid user input
defined('EXIT_DATABASE') || define('EXIT_DATABASE', 8);             // database error
defined('EXIT__AUTO_MIN') || define('EXIT__AUTO_MIN', 9);           // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') || define('EXIT__AUTO_MAX', 125);         // highest automatically-assigned error code
