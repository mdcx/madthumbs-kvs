<?php

    /**
     *  Use FFMpeg to grab some information about a movie.
     *  @package Movinfo
     *
     *  @author Mike Campbell <mike@madnm.com>
     *  @created Apr 1, 2017
     */
    namespace Mad\Video;

    define('FFPROBE_BIN', trim(`which ffprobe`));

    class FFProbe {

        private $movie_path = false;
        public $formats = array();
        public $streams = array();
        public $duration = false;
        public $safe_duration = false;
        public $height = false;
        public $width = false;
        public $dimensions = false;
        public $framerate = false;
        public $filesize = 0;
        public $aspect = false;
        public $dst_size = false;
        public $dst_mobile = false;
        public $keyframes = false;

        function __construct( $path = false )
        {
            $this->_ffprobe_bin = defined( 'FFPROBE_BIN' ) ? FFPROBE_BIN : `which ffprobe`;
            if ( $path )
            {
                $this->_init_movie( $path );
                $this->movie_data();
            }
        }

        private function _init_movie( $path )
        {
            $this->movie_path = !is_file( $path ) || !is_readable( $path ) ? false : $path;
            $this->formats = $this->streams = array();
            $fields = array( 'duration', 'safe_duration', 'height', 'width', 'dimensions',
                'framerate', 'filesize',
                'aspect', 'dst_size', 'dst_mobile', 'keyframes' );
            foreach ( $fields as $f ) $this->$f = false;
        }

        public function movie_data( $path = false )
        {
            if ( $path )
                $this->_init_movie( $path );

            if ( !$this->movie_path )
                return false;


            $this->filesize = filesize( $this->movie_path );
            $cmd = sprintf( '%s -sexagesimal -show_format -show_streams -pretty %s 2>&1', $this->_ffprobe_bin, escapeshellarg( $this->movie_path ) );
            $out = array();
            $ret = `$cmd`;
            $out = explode("\n", $ret);
            $in_format = FALSE;
            $in_stream = FALSE;
            $format_id = 0;
            $stream_id = 0;

            foreach ( $out as $line )
            {
                $line = trim( $line );
                if ( empty( $line ) )
                    continue;

                if ( preg_match( '#\[(\/?)(STREAM|FORMAT)\]#', $line, $m ) )
                {
                    $var = 'in_' . strtolower( $m[2] );
                    $$var = (empty( $m[1] ) ? TRUE : FALSE);

                    if ( $in_format )
                        $this->formats[$format_id] = new stdClass;
                    else if ( $var == 'in_format' )
                        $format_id += 1;

                    if ( $in_stream )
                        $this->streams[$stream_id] = new stdClass;
                    else if ( $var == 'in_stream' )
                        $stream_id += 1;
                    continue;
                }

                list($key, $val) = explode( '=', $line );

                if ( $in_format )
                    $this->formats[$format_id]->{$key} = $val;

                if ( $in_stream )
                    $this->streams[$stream_id]->{$key} = $val;
            }

            $streams = $this->streams;
            $this->streams = [];
            foreach($streams as $stream) {
                if ( isset($stream->codec_type) )
                    $this->streams[$stream->codec_type][] = $stream;
            }

            $this->duration = $this->get_video_duration();
            // $this->safe_duration = $this->get_safe_duration();
            $this->framerate = $this->get_video_framerate();
            $this->dimensions = $this->get_video_dimens();
            $this->width = $this->dimensions[0];
            $this->height = $this->dimensions[1];
            $this->keyframes = $this->get_keyframes();
            $this->set_video_aspect();
        }

        function set_video_aspect()
        {
            if ( $this->width == 0 || $this->height == 0 )
            {
                $this->aspect = $this->dst_mobile = $this->dst_size = false;
                return false;
            }

            $aspect = round( $this->width / $this->height, 4 );
            if ( substr( $aspect, 0, 3 ) == '1.7' )
            {
                $this->aspect = '16:9';
                $this->dst_size = '640x360';
                $this->dst_mobile = '320x180';
            }
            else
            {
                $this->aspect = '4:3';
                $this->dst_size = '640x480';
                $this->dst_mobile = '320x240';
            }
        }

        // report the shorter of the 2 durations if format and stream disagree.
        function get_video_duration()
        {
            $durations = [];
            foreach ( $this->formats as $fk=>$format )
            {
                if ( isset($format->duration) )
                    $durations['format_'.$fk] = hms_to_secs($format->duration);
            }

            foreach ( $this->streams['video'] as $sk=>$stream )
            {
                    if ( isSet( $stream->duration ) )
                        $durations['video_'.$sk] = hms_to_secs($stream->duration);
            }
            foreach ( $this->streams['audio'] as $sk=>$stream )
            {
                    if ( isSet( $stream->duration ) )
                        $durations['audio_'.$sk] = hms_to_secs($stream->duration);
            }
            return $durations;
        }

        function get_safe_duration()
        {
            $max_diff = 15;

            $try = $this->duration * .025;

            if ( $try > $max_diff )
                $try = $max_diff;

            return $this->duration - $try;
        }

        function hms_to_secs($time) {
            sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
            $time_seconds = isset($hours) ? $hours * 3600 + $minutes * 60 + $seconds : $minutes * 60 + $seconds;
            return $time_seconds;
        }

        function get_video_framerate()
        {
            $fps = FALSE;
            foreach ( $this->streams as $stream )
            {
                if ( $stream->codec_type == 'video' )
                {
                    list($_frms, $_secs) = explode( '/', $stream->r_frame_rate );
                    $fps = (int) $_frms / max( 1, (int) $_secs );
                }
            }
            if ( !$fps )
            {
                $fps = 0;
            }
            return $fps;
        }

        function get_video_dimens()
        {
            $dimens = FALSE;
            foreach ( $this->streams['video'] as $stream )
            {
                if ( $stream->codec_type == 'video' )
                {
                    $dimens = array( (int) $stream->width, (int) $stream->height );
                }
            }
            return $dimens;
        }

        function get_keyframes( $fps = false )
        {
            if ( !$fps )
                $fps = $this->framerate;

            $keys = floor( $fps );
            $keys = $keys < 3 ? 3 : $keys > 32 ? 32 : $keys;

            return $keys;
        }

    }
