<?php namespace Mad\App;

use League\Container\ContainerAwareTrait;
use League\Container\ContainerAwareInterface;

class MadApp implements ContainerAwareInterface
{
    /**
     * instantiate the class, and give it an instance of the php league container which will
     * be responsible for loading/instantiating our classes.
     *
     * This class will be responsible for providing access to the services/classes
     * it will allso load allof our config files
     *
     * I think we will build a 'command' or controller class that will run the actual scripts.
     *
     * I'm also not sure if this whole 'App' is redundant with the functions that the container itself
     * performs.
     */

    use ContainerAwareTrait;

    protected $_instances;

    /**
     * @var array
     */
    protected $serviceMap = [
        'logger' => [\Mad\Log\Writer::class, true],
        'loader' => [\Mad\Core\Loader::class, true],
        'args'   => [\Mad\Core\Args::class, true],
        // 'db' => \Mad\Core\DB::class,
        'locker' => [\Mad\Core\Locker::class, true],
        'config' => [\Mad\Core\Config::class, true],
        'dotenv' => 'dotenv_closure',
    ];

    /**
     * @param  $name
     * @param  $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $instance = strtolower($name);
        if (isset($this->_instances[$instance])) {
            return $this->_instances[$instance];
        } else {
            throw new \Exception("Failed to locate instance: $instance");
        }
    }

    /**
     * @param \League\Container\Container $container
     */
    public function __construct(\League\Container\Container $container)
    {
        $this->setContainer($container);
        foreach ($this->serviceMap as $alias => $class) {
            $shared = false;
            if (is_string($class)) {
                if (substr($class, -7) === 'closure') {
                    $class = $this->closure(substr($class, 0, -8));
                    if ($class === 'false') {
                        throw new \Exception("Failed to locate Closure for '$alias'");
                    }

                }
            } elseif (is_array($class)) {
                list($class, $shared) = $class;
                if ($shared) {
                    $this->getContainer()->add($alias, $class)->setShared();
                } else {
                    $this->getContainer()->add($alias, $class);
                }
                $this->_instances[$alias] = $this->getContainer()->get($class);
            }
        }
    }

    /**
     * @param $which
     */
    public function closure($which)
    {
        print "trying to find closure for: $which" . NL;
        if ($which === 'dotenv') {
            $this->getContainer()->add(\Mad\Config\DotEnv::class, function () {
                return (new \Mad\Config\DotEnv(APP_ROOT));
            });
            $this->_instances[$which] = $this->getContainer()->get(\Mad\Config\DotEnv::class);
            $this->_instances[$which]->load();
        }

        return false;
    }

    /**
     * @param $conn
     * @return mixed
     */
    public function database($conn = '')
    {
        if (isset($this->_instances['database'][$conn])) {
            return $this->_instances['database'][$conn];
        }

        $config  = $this->config()->get('database');
        $default = $config->get('default');

        $connections = $config->get('connections');
        if (isset($connections[$conn])) {
            $p                                   = $connections[$conn];
            return $this->_instances['database'][$conn] = new \Mad\Core\DB($p['username'], $p['password'], $p['database'], $p['hostname']);
        }

    }

    /**
     * @param  $alias
     * @return mixed
     */
    public function make($class, $alias = null)
    {
        if (null === $alias) {
            $alias = $class;
        }

        $this->_instances[$alias] = $this->getContainer()->get($class);
    }
}
