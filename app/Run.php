<?php
require_once 'Common.php';


$app = app();

$req = $app->args()->required(['c','m','v']);
extract($req);
if ( $c ) {
    $class = '\\KVS\Controllers\\'.ucfirst($c);
    $control = new $class(app());
    if ( $control  && $m ) {
        $control->$m();
    }
}
