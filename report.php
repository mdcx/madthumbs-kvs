<?php include 'include/common.php';
load_file('text');
$kvs = new KVSDB;
$req = $args->required(['cmd', 'server_id', 'date']);
extract($req);

$cmd = $cmd == false ? $args->method() : $cmd;

if (!$date) {
    $date = date('Y-m-d');
}

if ($date < 0) {
    $date = date('Y-m-d', time() + ($date * 86400));
}

if (!$cmd) {
    die("specify --cmd " . NL);
}

define('SAVE_QUERIES', true);
if (!is_cli()) {
    print "<pre>\n";
}

if ($cmd === 'server_hour') {
    msg($cmd, "looking up hourly total report for $server_id @ $date");
    $res = $kvs->fetch("SELECT date_format(end_date, '%H') as end_hour, count(task_id) as total "
        . "FROM  ktvs_background_tasks_history bth "
        . "LEFT JOIN ktvs_admin_conversion_servers bcs ON bth.server_id = bcs.server_id "
        . "WHERE date_format(end_date,'%Y-%m-%d') = ? AND bth.server_id = ? "
        . "GROUP BY end_hour ORDER BY end_hour", $date, $server_id, 'R_ASSOC');

    $sum   = array_sum(array_column($res, 'total'));
    $res[] = ['end_hour' => 'SUM', 'total' => $sum];
    text_table($res, true);
} elseif ($cmd === 'hour') {
    msg($cmd, "looking up all hourly total report for @ $date");
    $res = $kvs->fetch("select date_format(end_date, '%Y-%m-%d %H:00') as end_time, count(task_id) as total FROM ktvs_background_tasks_history GROUP BY end_time ORDER BY end_time DESC LIMIT 24", 'R_ASSOC');

    $sum   = array_sum(array_column($res, 'total'));
    $res[] = ['end_time' => 'SUM', 'total' => $sum];
    text_table($res, true);

} elseif ($cmd === 'week') {

} elseif ($cmd === 'server_day') {
    msg($cmd, "looking up server daily report for $date");
    $res = $kvs->fetch("SELECT bcs.server_id, bcs.title, count(task_id) as total "
        . "FROM  ktvs_background_tasks_history bth "
        . "LEFT JOIN ktvs_admin_conversion_servers bcs ON bth.server_id = bcs.server_id "
        . "WHERE date_format(end_date,'%Y-%m-%d') = ? "
        . "GROUP BY bth.server_id ORDER BY bcs.title", $date, 'R_ASSOC');
    $sum = array_sum(array_column($res, 'total'));
    foreach ($res as $k => $v) {
        $res[$k]['link'] = "<a href='report.php?cmd=server_hour&server_id=$v[server_id]'>link</a>";
    }

    $res[] = ['server_id' => '', 'title' => 'SUM', 'total' => $sum, 'link' => ''];
    text_table($res, true);
} elseif ($cmd == '24h') {
    // $res = $kvs->fetch("")
}

$kvs->query_report();

if (!is_cli()) {
    print "</pre>";
}
